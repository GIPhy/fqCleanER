[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/)
[![Bash](https://img.shields.io/badge/Bash-5.1-be0032)](https://www.gnu.org/software/bash/)


# fqCleanER

_fqCleanER_ (fastq Cleaning and Enhancing Routine) is a command line tool written in [Bash](https://www.gnu.org/software/bash/) to ease the different standard preprocessing steps of short high-throughput sequencing (HTS) reads.

Eight standard HTS read processing steps can be carried out:<br>
**&emsp; &#10102; &nbsp;** HTS read [D]eduplication, using _fqduplicate_ from the [fqtools](http://ftp.pasteur.fr/pub/gensoft/projects/fqtools/) package,<br>
**&emsp; &#10103; &nbsp;** HTS read [T]rimming and clipping, using [_AlienTrimmer_](https://research.pasteur.fr/en/software/alientrimmer/) (Criscuolo and Brisse 2013),<br>
**&emsp; &#10104; &nbsp;** paired-ends HTS read [M]erging, using [_FLASh_](https://ccb.jhu.edu/software/FLASH/) (Magoc and Salzberg 2011),<br>
**&emsp; &#10105; &nbsp;** [C]ontaminating HTS read removal, using [_AlienRemover_](https://gitlab.pasteur.fr/GIPhy/AlienRemover),<br>
**&emsp; &#10106; &nbsp;** sequencing [E]rror correction, using [_Musket_](http://musket.sourceforge.net/homepage.htm) (Liu et al. 2013),<br>
**&emsp; &#10107; &nbsp;** [L]ow-coverage HTS read removal, using [_ROCK_](https://gitlab.pasteur.fr/vlegrand/ROCK) (Legrand et al. 2022a, 2022b),<br>
**&emsp; &#10108; &nbsp;** digital [N]ormalization, using [_ROCK_](https://gitlab.pasteur.fr/vlegrand/ROCK) (Legrand et al. 2022a, 2022b),<br>
**&emsp; &#10109; &nbsp;** high-coverage (redundant) HTS read [R]eduction, using [_ROCK_](https://gitlab.pasteur.fr/vlegrand/ROCK) (Legrand et al. 2022a, 2022b).


All these steps can be performed in any order on up to three paired- and/or single-end FASTQ files (compressed or not). 

_fqCleanER_ runs on UNIX, Linux and most OS X operating systems.




## Dependencies

You will need to install the required programs listed in the following table, or to verify that they are already installed with the required version.

<div align="center">

| program                                                                                                                 | package                                                          | version    | sources                                                                                            |
|:----------------------------------------------------------------------------------------------------------------------- |:----------------------------------------------------------------:| ----------:|:-------------------------------------------------------------------------------------------------- |
| [_gawk_](https://www.gnu.org/software/gawk/)                                                                            | -                                                                | > 4.0.0    | [ftp.gnu.org/gnu/gawk](https://ftp.gnu.org/gnu/gawk/)                                              |
| [_xargs_](https://www.gnu.org/software/findutils/manual/html_node/find_html/xargs-options.html)<sup>&nbsp;&#9733;</sup> | [GNU findutils](https://www.gnu.org/software/findutils/)         | &ge; 4.6.0 | [ftp.gnu.org/gnu/findutils](https://ftp.gnu.org/gnu/findutils/)                                    |
| [_bzip2_](https://sourceware.org/bzip2/)                                                                                | -                                                                | > 1.0.0    | [sourceware.org/bzip2/downloads.html](https://sourceware.org/bzip2/downloads.html)                 |
| [_DSRC_](https://refresh-bio.github.io/software/)                                                                       | -                                                                | &ge; 2.0   | [github.com/refresh-bio/DSRC](https://github.com/refresh-bio/DSRC)                                 |
| [_gzip_](https://www.gnu.org/software/gzip/)                                                                            | -                                                                | > 1.5.0    | [ftp.gnu.org/gnu/gzip](https://ftp.gnu.org/gnu/gzip/)                                              |
| [_AlienDiscover_](https://gitlab.pasteur.fr/GIPhy/AlienDiscover)                                                        | -                                                                | &ge; 0.1   | [gitlab.pasteur.fr/GIPhy/AlienDiscover](https://gitlab.pasteur.fr/GIPhy/AlienDiscover)             |
| [_AlienRemover_](https://gitlab.pasteur.fr/GIPhy/AlienRemover)                                                          | -                                                                | &ge; 1.0   | [gitlab.pasteur.fr/GIPhy/AlienRemover](https://gitlab.pasteur.fr/GIPhy/AlienRemover)               |
| [_AlienTrimmer_](https://research.pasteur.fr/en/software/alientrimmer/)                                                 | -                                                                | &ge; 2.1      | [gitlab.pasteur.fr/GIPhy/AlienTrimmer](https://gitlab.pasteur.fr/GIPhy/AlienTrimmer)               |
| [_FLASh_](https://ccb.jhu.edu/software/FLASH/)                                                                          | -                                                                | > 1.2.10   | [sourceforge.net/projects/flashpage](https://sourceforge.net/projects/flashpage/)                  |
| _fqconvert_ <br> _fqduplicate_ <br> _fqextract_ <br> _fqstats_                                                          | [fqtools](http://ftp.pasteur.fr/pub/gensoft/projects/fqtools/)   | &ge; 1.1a  | [ftp.pasteur.fr/pub/gensoft/projects/fqtools](http://ftp.pasteur.fr/pub/gensoft/projects/fqtools/) |
| [_Musket_](http://musket.sourceforge.net/homepage.htm)<sup>&nbsp;&#x2726;</sup>                                         | -                                                                | &ge; 1.1   | [sourceforge.net/projects/musket](https://sourceforge.net/projects/musket/)                        |
| [_ntCard_](https://www.bcgsc.ca/resources/software/ntcard)                                                              | -                                                                | > 1.2      | [github.com/bcgsc/ntCard](https://github.com/bcgsc/ntCard)                                         |
| [_ROCK_](https://research.pasteur.fr/en/software/rock)                                                                  | -                                                                | &ge; 1.9.3 | [gitlab.pasteur.fr/vlegrand/ROCK](https://gitlab.pasteur.fr/vlegrand/ROCK)                         |

</div>


<sup>&nbsp;&#9733;</sup> <span style="font-size:0.9em;">For some Mac OS X, it is worth noting that the default [BSD _xargs_](https://www.freebsd.org/cgi/man.cgi?xargs) does not offer all the functionalities required by _fqCleanER_.
However, the expected [GNU _xargs_](https://www.gnu.org/software/findutils/manual/html_node/find_html/xargs-options.html) (here named `gxargs`) can be easily installed using [_homebrew_](https://brew.sh) (i.e. `brew install findutils`).
Of note, _fqCleanER_ first looks for the `gxargs` binary on the `$PATH`, and, if missing, for the `xargs` binary.</span><br>
<sup>&nbsp;&#x2726;</sup> <span style="font-size:0.9em;">When compiling the source code of [_Musket_](https://musket.sourceforge.net/homepage.htm#installation), it is recommended to edit its _Makefile_ to increase the value of the macro `MAX_SEQ_LENGTH` (e.g. 1000) in order to avoid any problem during the execution of _fqCleanER_.</span>





## Installation and execution

**A.** Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/fqCleanER.git
```

**B.** Give the execute permission to the file `fqCleanER.sh`:

```bash
chmod +x fqCleanER.sh
```

**C.** Execute _fqCleanER_ with the following command line model:

```bash
./fqCleanER.sh  [options]
```

**D.** If at least one of the required program (see [Dependencies](#dependencies)) is not available on your `$PATH` variable (or if one compiled binary has a different default name), _fqCleanER_ will exit with an error message.
When running _fqCleanER_ without option, a usage documentation should be displayed (see below); otherwise, the name of the missing program is displayed before exiting.
In such a case, edit the file `fqCleanER.sh` and indicate the local path to the corresponding binary(ies) within the code block `REQUIREMENTS` (approximately lines 80-220).
For each required program, the table below reports the corresponding variable assignment instruction to edit (if needed) within the code block `REQUIREMENTS`

<div align="center">
<sup>

| program          | variable assignment                |   | program          | variable assignment                               |
|:---------------- |:---------------------------------- | - |:---------------- |:------------------------------------------------- |
| _AlienDiscover_  | `ALIENDISCOVER_BIN=AlienDiscover;` |   | _fqconvert_      | `FQCONVERT_BIN=fqconvert;`                        |
| _AlienRemover_   | `ALIENREMOVER_BIN=AlienRemover;`   |   | _fqduplicate_    | `FQDUPLICATE_BIN=fqduplicate;`                    |
| _AlienTrimmer_   | `ALIENTRIMMER_BIN=AlienTrimmer;`   |   | _fqextract_      | `FQEXTRACT_BIN=fqextract;`                        |
| _bzip2_          | `BZIP2_BIN=bzip2;`                 |   | _fqstats_        | `FQSTATS_BIN=fqstats;`                            | 
| _DSRC_           | `DSRC2_BIN=dsrc;`                  |   | _Musket_         | `MUSKET_BIN=musket;`                              |
| _FLASh_          | `FLASH_BIN=flash;`                 |   | _ntCard_         | `NTCARD_BIN=ntcard;`                              |
| _gawk_           | `GAWK_BIN=gawk;`                   |   | _ROCK_           | `ROCK_BIN=rock;`                                  |
| _gzip_           | `GZIP_BIN=gzip;`                   |   | _xargs_          | `XARGS_BIN=xargs;`<br>`GXARGS_BIN=gxargs;` (OS X) |

</sup>
</div>

Note that depending on the installation of some required programs, the corresponding variable can be assigned with complex commands. 
For example, as _AlienTrimmer_ is a Java tool that can be run using a Java virtual machine, the executable jar file `AlienTrimmer.jar` can be used by _fqCleanER_ after editing the corresponding variable assignment instruction as follows: `ALIENTRIMMER_BIN="java -jar AlienTrimmer.jar"`.


## Usage

Run _fqCleanER_ without option to read the following documentation:

```
 USAGE:  fqCleanER.sh  [options] 

 OPTIONS:
  -1 <infile>   fwd (R1) FASTQ input file name from PE library 1         | input files can be |
  -2 <infile>   rev (R2) FASTQ input file name from PE library 1         | uncompressed (file |
  -3 <infile>   fwd (R1) FASTQ input file name from PE library 2         | extensions  .fastq |
  -4 <infile>   rev (R2) FASTQ input file name from PE library 2         | or    .fq),     or |
  -5 <infile>   fwd (R1) FASTQ input file name from PE library 3         | compressed   using |
  -6 <infile>   rev (R2) FASTQ input file name from PE library 3         | either gzip (.gz), |
  -7 <infile>   FASTQ input file name from SE library 4                  | bzip2   (.bz2   or |
  -8 <infile>   FASTQ input file name from SE library 5                  | .bz),   or    DSRC |
  -9 <infile>   FASTQ input file name from SE library 6                  | (.dsrc  or .dsrc2) |
  -o <outdir>   path and name of the output directory (mandatory option)
  -b <string>   base name for output files (mandatory option)
  -a <infile>   to set a file containing every alien oligonucleotide sequence (one per line) to
                be clipped during step 'T' (see below)
  -a <string>   one or several key words  (separated with commas),  each corresponding to a set 
                of alien oligonucleotide sequences to be clipped during step 'T' (see below):
                   POLY                nucleotide homopolymers
                   NEXTERA             Illumina Nextera index Kits
                   IUDI                Illumina Unique Dual index Kits
                   AMPLISEQ            AmpliSeq for Illumina Panels
                   TRUSIGHT_PANCANCER  Illumina TruSight RNA Pan-Cancer Kits
                   TRUSEQ_UD           Illumina TruSeq Unique Dual index Kits
                   TRUSEQ_CD           Illumina TruSeq Combinatorial Dual index Kits
                   TRUSEQ_SINGLE       Illumina TruSeq Single index Kits
                   TRUSEQ_SMALLRNA     Illumina TruSeq Small RNA Kits
                Note that  these sets  of alien  sequences are  not  exhaustive  and will never
                replace the exact oligos used for library preparation  (default: "POLY")
  -a AUTO       to perform  de novo  inference of  3' alien  oligonucleotide sequence(s)  of at 
                least 20 nucleotide length;  selected sequences  are completed  with those from 
                "POLY" (see above)                
  -A <infile>   to set sequence or k-mer  model file(s)  to carry out  contaminant read removal 
                during step 'C';  several comma-separated file names can be specified;  allowed 
                file extensions: .fa, .fasta, .fna, .kmr or .kmz (default: phiX174 genome)
  -d <string>   displays the alien oligonucleotide sequences corresponding to the specified key
                word(s); see option -a for the list of available key words
  -q <int>      quality score threshold;  all bases with Phred  score below  this threshold are 
                considered as non-confident (default: 15)
  -l <int>      minimum required length for a read (default: half the average read length)
  -p <int>      maximum allowed percentage  of non-confident bases  (as ruled by option -q) per 
                read (default: 50) 
  -c <int>      minimum allowed coverage depth for step 'L' or 'N' (default: 4)
  -C <int>      maximum allowed coverage depth for step 'R' or 'N' (default: 90)
  -s <string>   a sequence of tasks  to be iteratively performed,  each being defined by one of 
                the following uppercase characters:
                   C   discarding [C]ontaminating reads (as ruled by option -A)
                   E   correcting sequencing [E]rrors
                   D   [D]eduplicating reads
                   L   discarding [L]ow-coverage reads (as ruled by option -c)
                   N   digital [N]ormalization (i.e. same as consecutive steps "RL")
                   M   [M]erging overlapping PE reads
                   R   [R]educing redundancy (as ruled by option -C)
                   T   [T]rimming and clipping (as ruled by options -q, -l, -p, -a)
                (default: "T")
  -z <string>   compressed output  file(s) using  gzip ("gz"),  bzip2 ("bz2")  or DSRC ("dsrc")
                (default: not compressed)
  -t <int>      number of threads (default: 12)
  -w <dir>      tmp directory (default: $TMPDIR, otherwise /tmp)
  -h            prints this help and exit

 EXAMPLES:
   fqCleanER.sh  -4 se.fq -o out -b se.flt
   fqCleanER.sh  -1 r1.fq -2 r2.fq -o out -b pe.flt
   fqCleanER.sh  -1 r1.fq -2 r2.fq -a NEXTERA -q 20 -s DTENM -o out -b flt -z gz
```

## Notes

* _fqCleanER_ is able to consider up to three paired-ends libraries (PE; options `-1` to `-6`) and three single-end libraries (SE; options `-7` to `-9`). Input files should be in FASTQ format and can be compressed using [_gzip_](https://www.gnu.org/software/gzip/), [_bzip2_](https://sourceware.org/bzip2/) or [_DSRC_](http://sun.aei.polsl.pl/REFRESH/index.php?page=projects&project=dsrc&subpage=about) (Roguski and Deorowicz 2014). All input files are always converted into Phred+33 format using [_fqconvert_](http://ftp.pasteur.fr/pub/gensoft/projects/fqtools/). 

* Output files are defined by a specified prefix (mandatory option `-b`) and written in a specified output directory (mandatory option `-o`). Output files can be compressed using [_gzip_](https://www.gnu.org/software/gzip/), [_bzip2_](https://sourceware.org/bzip2/) or [_DSRC_](http://sun.aei.polsl.pl/REFRESH/index.php?page=projects&project=dsrc&subpage=about) (option `-z`).

* Temporary files are written into a dedicated directory created into the `$TMPDIR` directory (when defined, otherwise `/tmp`). When possible, it is highly recommended to set a temp directory with large capacity (option `-w`).

* The cleaning/enhancing steps can be specified using option `-s` in any order. The same step can be specified several times (e.g. `-s DTDNEN`).

  <span style="color:navy; font-size:1.1em;">**[C]**</span> &nbsp; Contaminating HTS read removal (`-s C`) is performed using [_AlienRemover_](https://gitlab.pasteur.fr/GIPhy/AlienRemover) with default options. Contaminating sequences/_k_-mers are specified using option `-A`. When contaminating sequences are quite short (e.g. virus genomes), they can be directly specified via a FASTA-formatted file without affecting the overall running time. However, to consider large contaminating sequences (e.g. human genomes), it is highly recommended to precompute the corresponding _k_-mer set using [_AlienRemover_](https://gitlab.pasteur.fr/GIPhy/AlienRemover) and specify the corresponding _k_-mer file (kmr/kmz file extension) to observe fast running times. If the option `-A` is not set, calling step C enables to remove [phi-X174](https://www.ncbi.nlm.nih.gov/nucleotide/NC_001422.1) HTS reads.

  <span style="color:navy; font-size:1.1em;">**[D]**</span> &nbsp; HTS read deduplication (`-s D`) is performed using [_fqduplicate_](http://ftp.pasteur.fr/pub/gensoft/projects/fqtools/). Note that a pair of PE reads (R1,R2) and (R1',R2') are considered as duplicated (i.e. identical) when R1 = R1' and R2 = R2'.

  <span style="color:navy; font-size:1.1em;">**[E]**</span> &nbsp; Sequencing error correction (`-s E`) is performed using [_Musket_](http://musket.sourceforge.net/homepage.htm) (Liu et al. 2013) with _k_-mer length _k_ = 21. This step generally requires quite important running times and will benefit from a large number of threads (option `-t`).

  <span style="color:navy; font-size:1.1em;">**[L][N][R]**</span> &nbsp; These three steps (`-s L`, `-s N`, `-s R`, respectively) are related to the digital normalization procedure (Brown et al. 2012), performed using [_ROCK_](https://research.pasteur.fr/en/software/rock) (Legrand et al. 2022a, 2022b) with _k_-mer length _k_ = 25. Given a lower-bound and a upper-bound coverage depth thresholds (options `-c` and `-C`, respectively), the digital normalization selects a subset of HTS reads such that every sequenced base has a coverage depth between these two bounds. When setting a moderate upper-bound (that is lower than the overall average coverage depth; default: `-C 90`), every sequenced base from the selected HTS read subset is expected to have a coverage depth close to this bound. When setting a small lower-bound (default: `-c 4`), all HTS reads corresponding to a sequenced region with coverage depth lower than this bound will be discarded (e.g. artefactual or erroneous HTS read, low-coverage contaminating HTS read). Step N (`-s N`) uses the two bounds (options `-C` and `-c`), whereas steps L/R (`-s L` and `-s R`, respectively) use only the lower-/upper-bound, respectively.

  <span style="color:navy; font-size:1.1em;">**[M]**</span> &nbsp; PE HTS read merging (`-s M`, only with PE input files) is performed using [_FLASh_](https://ccb.jhu.edu/software/FLASH/) (Magoc and Salzberg 2011) when the insert size is shorter than the sum of the two paired HTS read lengths. When using this step, dedicated output files are written ( _.M.fastq_ file extension).

  <span style="color:navy; font-size:1.1em;">**[T]**</span> &nbsp; Trimming and clipping (`-s T`) are performed using [_AlienTrimmer_](https://research.pasteur.fr/en/software/alientrimmer/) (Criscuolo and Brisse 2013). Clipping is carried out based on the specified alien oligonucleotides (option `-a`), where alien oligonucleotide sequences can be (i) set using precomputed standard library names, (ii) specified via user-defined FASTA-formatted file, or (iii) directly estimated from the input files using [_AlienDiscover_](https://gitlab.pasteur.fr/GIPhy/AlienDiscover) (option `-a AUTO`). When step T is run without setting option `-a`, clipping is carried out with the four homopolymers (`POLY`) as alien oligonucleotides. Trimming is carried out by deleting 5' and 3' regions containing many non-confident bases, where a base is considered as non-confident when its Phred score is lower than a Phred score threshold (set using option `-q`; default: 15). After trimming/clipping an HTS read, it can be discarded when the number of remaining bases is lower than a specified length threshold (option `-l`; default: half the average read length) or when the percentage of remaining non-confident bases is higher than another specified threshold (option `-p`; default: 50%). Note that when HTS read discarding breaks PE, singletons are written into dedicated output files ( _.S.fastq_ file extension).


* Each predefined set of alien oligonucleotide sequences can be displayed using option `-d`. Some sets of alien oligonucleotide sequences are derived from _'Illumina Adapter Sequences'_  [Document # 1000000002694 v16](https://emea.support.illumina.com/downloads/illumina-adapter-sequences-document-1000000002694.html), i.e. options `-a NEXTERA` (_Nextera DNA Indexes_), `-a  IUDI` (_IDT for Illumina UD Indexes_), `-a AMPLISEQ` (_AmpliSeq for Illumina Panels_), `-a TRUSIGHT_PANCANCER` (_TruSight RNA Pan-Cancer Panel_), `-a TRUSEQ_UD` (_IDT for Illumina-TruSeq DNA and RNA UD Indexes_), `-a TRUSEQ_CD` (_TruSeq DNA and RNA CD Indexes_), `-a TRUSEQ_SINGLE` (_TruSeq Single Indexes_), and `-a TRUSEQ_SMALLRNA` (_TruSeq Small RNA_). <br> <sup><sub>**[Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works created by Illumina customers are authorized for use with Illumina instruments and products only. All other uses are strictly prohibited.]**</sub></sup>

## References

Brown TC, Howe A, Zhang Q, Pyrkosz AB, Brom TH (2012) 
_A Reference-Free Algorithm for Computational Normalization of Shotgun Sequencing Data_. 
[arXiv:1203.4802](https://arxiv.org/abs/1203.4802).

Criscuolo A, Brisse S (2013) 
_AlienTrimmer: a tool to quickly and accurately trim off multiple short contaminant sequences from high-throughput sequencing reads_.
**Genomics**, 102(5-6):500-506. [doi:10.1016/j.ygeno.2013.07.011](https://doi.org/10.1016/j.ygeno.2013.07.011).

Legrand V, Kergrohen T, Joly N, Criscuolo A (2022a) 
_ROCK: digital normalization of whole genome sequencing data_. 
**Journal of Open Source Software**, 7(73):3790. [doi:10.21105/joss.03790](https://doi.org/10.21105/joss.03790).

Legrand V, Kergrohen T, Joly N, Criscuolo A (2022b) 
_ROCK: digital normalization of whole genome sequencing data_. 
_In_: Lemaitre C, Becker E, Derrien T (eds), **Proceedings of JOBIM 2022**, Rennes, France, p. 21. 
[doi:10.14293/S2199-1006.1.SOR-.PPNAZX5.v1](https://www.scienceopen.com/hosted-document?doi=10.14293/S2199-1006.1.SOR-.PPNAZX5.v1).

Liu Y, Schröder J, Schmidt B (2013) 
_Musket: a multistage k-mer spectrum-based error corrector for Illumina sequence data_. 
**Bioinformatics**, 29(3):308-315.
[doi:10.1093/bioinformatics/bts690](https://doi.org/10.1093/bioinformatics/bts690).

Magoc T, Salzberg S (2011) 
_FLASH: Fast length adjustment of short reads to improve genome assemblies_. 
**Bioinformatics**, 27:21:2957-2963.
[doi:10.1093/bioinformatics/btr507](https://doi.org/10.1093/bioinformatics/btr507).

Roguski L, Deorowicz S (2014)
_DSRC 2: Industry-oriented compression of FASTQ files_.
**Bioinformatics**, 30(15):2213-2215.
[doi:10.1093/bioinformatics/btu208](https://doi.org/10.1093/bioinformatics/btu208).


## Citations

Abou Fayad A, Rafei R, Njamkepo E, Ezzeddine J, Hussein H, Sinno S, Gerges J-R, Barada S, Sleiman A, Assi M, Baakliny M, Hamedeh L, Mahfouz R, Dabboussi F, Feghali R, Mohsen Z, Rady A, Ghosn N, Abiad F, Abubakar A, Barakat A, Wauquier N, Quilici M-L, Hamze M, Weill F-X, Matar GM (2024)
_An unusual two-strain cholera outbreak in Lebanon, 2022-2023: a genomic epidemiology study_.
**Nature Communications**, 15:6963.
[doi:10.1038/s41467-024-51428-0](https://doi.org/10.1038/s41467-024-51428-0)

Bouchier C, Touak G, Rei D, Clermont D (2024)
_Complete genome sequence of two Christensenella minuta strains CIP 112228 and CIP 112229, isolated from human fecal samples_.
**Microbiology Resource Announcements**, 13(12):e00766-24.
[doi:10.1128/mra.00766-24](https://doi.org/10.1128/mra.00766-24)

Charlier C, Noel C, Hafner L, Moura A, Mathiaud C, Pitsch A, Meziane C, Jolly-Sanchez L, de Pontfarcy A, Diamantis S, Bracq-Dieye H, Disson O, Thouvenot P, Valès G, Tessaud-Rita N, Tourdjman M, Leclercq A, Lecuit M (2023)
_Fatal neonatal listeriosis following L. monocytogenes horizontal transmission highlights neonatal susceptibility to orally acquired listeriosis_.
**Cell Reports Medicine**, 4(7):101094.
[doi:10.1016/j.xcrm.2023.101094](https://doi.org/10.1016/j.xcrm.2023.101094)

Chavarría-Pizarro L, Rivera-Méndez W, Núñez-Montero K, Pizarro-Cerdá J (2024)
_Novel strains of Actinobacteria associated with neotropical social wasps (Vespidae; Polistinae, Epiponini) with antimicrobial potential for natural product discovery_.
**FEMS Microbes**, 5:xtae005.
[doi:10.1093/femsmc/xtae005](https://doi.org/10.1093/femsmc/xtae005)

Gravey F, Sévin C, Castagnet S, Foucher N, Maillard K, Tapprest J, Léon A, Langlois B, Le Hello S, Petry S (2024)
_Antimicrobial resistance and genetic diversity of Klebsiella pneumoniae strains from different clinical sources in horses_.
**Frontiers in Microbiology**, 14:1334555.
[doi:10.3389/fmicb.2023.1334555](https://doi.org/10.3389/fmicb.2023.1334555)

Jalalizadeh F, Njamkepo E, Weill F-X, Goodarzi F, Rahnamaye-Farzami M, Sabourian R, Bakhshi B (2024)
-Genetic approach toward linkage of Iran 2012–2016 cholera outbreaks with 7th pandemic Vibrio cholerae_. 
**BMC Microbiology**, 24:33. 
[doi:10.1186/s12866-024-03185-9](https://doi.org/10.1186/s12866-024-03185-9)

Markovich Y, Palacios-Gorba C, Gomis J, Gómez-Martín A, Ortolá S, Quereda JJ (2024)
_Phenotypic and genotypic antimicrobial resistance of Listeria spp. in Spain_.
**Veterinary Microbiology**, 293:110086.
[doi:10.1016/j.vetmic.2024.110086](https://doi.org/10.1016/j.vetmic.2024.110086)

Mornico D, Hon C-C, Koutero M, Weber C, Coppée J-Y, Clark CG, Dillies M-A, Guillen N (2022) 
_RNA Sequencing Reveals Widespread Transcription of Natural Antisense RNAs in Entamoeba Species_. 
**Microorganisms**, 10(2):396. 
[doi:10.3390/microorganisms10020396](https://doi.org/10.3390/microorganisms10020396)

Moura A, Leclercq A, Vales G, Tessaud-Rita N, Bracq-Dieye H, Thouvenot P, Madec Y, Charlier C, Lecuit M (2023)
_Phenotypic and genotypic antimicrobial resistance of Listeria monocytogenes: an observational study in France_.
**The Lancet Regional Health Europe**, 37:100800.
[doi:10.1016/j.lanepe.2023.100800](https://doi.org/10.1016/j.lanepe.2023.100800)

Pottier M, Castagnet S, Gravey F, Leduc G, Sévin C, Petry S, Giard J-C, Le Hello S, Léon A (2023)
_Antimicrobial Resistance and Genetic Diversity of Pseudomonas aeruginosa Strains Isolated from Equine and Other Veterinary Samples_.
**Pathogens**, 12(1):64.
[doi:10.3390/pathogens12010064](https://doi.org/10.3390/pathogens12010064)

Rouard C, Greig DR, Tauhid T, Dupke S, Njamkepo E, Amato E, van der Putten B, Naseer U, Blaschitz M, Mandilara GD, Cohen SJ, Indra A, Noël H, Sideroglou T, Heger F, van den Beld M, Wester AL, Quilici M-L, Scholz HC, Fröding I, Jenkins C, Weill F-X (2022) 
_Genomic analysis of Vibrio cholerae O1 isolates from cholera cases, Europe, 2022_. 
**Euro Surveillance**, 29(36):pii=2400069.
[doi:10.2807/1560-7917.ES.2024.29.36.2400069](https://doi.org/10.2807/1560-7917.ES.2024.29.36.2400069)

Rouard C, Njamkepo E, Quilici M-L, Nguyen S, Knight-Connoni V, Šafránková R, Weill F-X (2024)
_Vibrio cholerae serogroup O5 was responsible for the outbreak of gastroenteritis in Czechoslovakia in 1965_.
**Microbial Genomics**, 10:9.
[doi:10.1099/mgen.0.001282](https://doi.org/10.1099/mgen.0.001282)

Vautrin N, Alexandre K, Pestel-Caron M, Bernard E, Fabre R, Leoz M, Dahyot S, Caron F (2023)
_Contribution of Antibiotic Susceptibility Testing and CH Typing Compared to Next-Generation Sequencing for the Diagnosis of Recurrent Urinary Tract Infections Due to Genetically Identical Escherichia coli Isolates: a Prospective Cohort Study of Cystitis in Women_.
**Microbiology Spectrum**, 11(4):e02785-22. 
[doi:10.1128/spectrum.02785-22](https://doi.org/10.1128/spectrum.02785-22)

Yassine I​, Hansen EE​, Lefèvre S​, Ruckly C​, Carle I​, Lejay-Collin M​, Fabre L​, Rafei R​, Pardos de la Gandara M, Daboussi F​, Shahin A, Weill F-X (2023)
_ShigaPass: an in silico tool predicting Shigella serotypes from whole-genome sequencing assemblies_.
**Microbial Genomics**, 9:3.
[doi:10.1099/mgen.0.000961](https://doi.org/10.1099/mgen.0.000961)

