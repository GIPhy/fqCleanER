#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  fqCleanER: fastq file Cleaning and Enhancement Routine                                                    #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2015-2023 Institut Pasteur"                                                      #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=23.12                                                                                              #
# + fixed bug in fqinit() when dealing with Phred lines containing only '?' (e.g. SRR26898806)               #
#                                                                                                            #
# VERSION=23.07                                                                                              #
# + replacing MINION with ALIENDISCOVER (-a AUTO)                                                            #
# + able to deal with GXARGS on OS X                                                                         #
# + mvf() now considers empty infiles                                                                        #
# + by default, automated estimate for option -l                                                             #
# + new static options (-q 10 -m 5) for ROCK (instead of -q 0 -m 5)                                          #
# + adding read length in the stdout outputs                                                                 #
#                                                                                                            #
# VERSION=21.10ac                                                                                            #
# + updating fq2oligo() to better deal with MINION output when setting -a AUTO                               #
#                                                                                                            #
# VERSION=21.06ac                                                                                            #
# + complete updating of the code                                                                            #
#                                                                                                            #
# VERSION=6.03.181008ac                                                                                      #
# + ready to be distributed: just edit PWD (line 59)                                                         #
#                                                                                                            #
# VERSION=6.02.170728ac                                                                                      #
# + using ROCK 1.8 with options -p 1 -m 10 for steps L, R and N                                              #
#                                                                                                            #
# VERSION=6.01.170515ac                                                                                      #
# + option -a AUTO is available for inferring alien oligos with the program minion from the package kraken   #
#   (ftp://ftp.ebi.ac.uk/pub/contrib/enrightlab/kraken/reaper/binaries/reaper-15-065/linux/)                 #
# + using ROCK 1.6 for steps L, R and N                                                                      #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- input file extensions --------------------------------------------------------------------------------  #
#                                                                                                            #
  declare -a FILE_EXTENSIONS=("fq" "fastq" "gz" "bz" "bz2" "dsrc" "dsrc2");
#                                                                                                            #
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
  NA2="$NA,$NA";
  NA3="$NA,$NA,$NA";
  NA4="$NA,$NA,$NA,$NA";
  NA15="$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA,$NA";
  WAITIME=1;
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# - SUMMARY -
# AlienDiscover/0.1 AlienRemover/1.0 AlienTrimmer/2.1 bzip2/1.0.6 dsrc/2.0.2 fqtools/1.2 FLASH/1.2.11 gawk/5.0.1 musket/1.1 ntCard/1.2.2 ROCK/2.0.0
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  if [ ! $(command -v $GAWK_BIN) ]; then echo "$GAWK_BIN not found"                       >&2 ; exit 1 ; fi
  BAWK="$GAWK_BIN";
  CAWK="$GAWK_BIN -F,";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- xargs ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
# + GNU xargs                                                                                                #
  XARGS_BIN=xargs;
# + gxargs (for mac OS X)                                                                                    #
  GXARGS_BIN=gxargs;
  XARGS_STATIC_OPTIONS="";     
    if [ $(command -v $GXARGS_BIN) ]; then XARGS="$GXARGS_BIN $XARGS_STATIC_OPTIONS";
  elif [ $(command -v $XARGS_BIN)  ]; then XARGS="$XARGS_BIN  $XARGS_STATIC_OPTIONS";
  else                                echo "neither $GXARGS_BIN nor $XARGS_BIN detected"  >&2 ; exit 1 ; fi
#                                                                                                            #
# -- bzip2: file compresser -------------------------------------------------------------------------------  #
#                                                                                                            #
  BZIP2_BIN=bzip2;
  if [ ! $(command -v $BZIP2_BIN) ]; then echo "$BZIP2_BIN not found"                     >&2 ; exit 1 ; fi
  BZIP2_STATIC_OPTIONS="-q -z ";
  BZIP2="$BZIP2_BIN $BZIP2_STATIC_OPTIONS";
  BUNZIP2_STATIC_OPTIONS="-q -d ";
  BUNZIP2="$BZIP2_BIN $BUNZIP2_STATIC_OPTIONS";
#                                                                                                            #
# -- dsrc: FASTQ file compresser --------------------------------------------------------------------------  #
#                                                                                                            #
  DSRC2_BIN=dsrc;
  if [ ! $(command -v $DSRC2_BIN) ]; then echo "$DSRC2_BIN not found"                     >&2 ; exit 1 ; fi
  DSRC2_STATIC_OPTIONS="";
  DSRC2="$DSRC2_BIN $DSRC2_STATIC_OPTIONS";
#                                                                                                            #
# -- gzip: file compresser --------------------------------------------------------------------------------  #
#                                                                                                            #
  GZIP_BIN=gzip;
  if [ ! $(command -v $GZIP_BIN) ]; then echo "$GZIP_BIN not found"                       >&2 ; exit 1 ; fi
  GZIP_STATIC_OPTIONS="";
  GZIP="$GZIP_BIN $GZIP_STATIC_OPTIONS";
  GUNZIP_STATIC_OPTIONS="-d ";
  GUNZIP="$GZIP_BIN $GUNZIP_STATIC_OPTIONS";
#                                                                                                            #
# -- AlienDiscover: inferring alien oligos ----------------------------------------------------------------  #
#                                                                                                            #
  ALIENDISCOVER_BIN=AlienDiscover;
  if [ ! $(command -v $ALIENDISCOVER_BIN) ]; then echo "$ALIENDISCOVER_BIN not found"     >&2 ; exit 1 ; fi
  ALIENDISCOVER_STATIC_OPTIONS="";
  ALIENDISCOVER="$ALIENDISCOVER_BIN $ALIENDISCOVER_STATIC_OPTIONS";
#                                                                                                            #
# -- AlienRemover: discarding alien reads -----------------------------------------------------------------  #
#                                                                                                            #
  ALIENREMOVER_BIN=AlienRemover;
  if [ ! $(command -v $ALIENREMOVER_BIN) ]; then echo "$ALIENREMOVER_BIN not found"       >&2 ; exit 1 ; fi
  ALIENREMOVER_STATIC_OPTIONS="-c 0.15 ";
  ALIENREMOVER="$ALIENREMOVER_BIN $ALIENREMOVER_STATIC_OPTIONS";
  K_ALIENREMOVER=25; # k-mer length for AlienRemover
#                                                                                                            #
# -- AlienTrimmer: trimming/clipping FASTQ files ----------------------------------------------------------  #
#                                                                                                            #
  ALIENTRIMMER_BIN=AlienTrimmer;
  if [ ! $(command -v $ALIENTRIMMER_BIN) ]; then echo "$ALIENTRIMMER_BIN not found"       >&2 ; exit 1 ; fi
  ALIENTRIMMER_STATIC_OPTIONS="";
  ALIENTRIMMER="$ALIENTRIMMER_BIN $ALIENTRIMMER_STATIC_OPTIONS";
#                                                                                                            #
# -- FLASH: overlapping PE read merging -------------------------------------------------------------------  #
#                                                                                                            #
  FLASH_BIN=flash;
  if [ ! $(command -v $FLASH_BIN) ]; then echo "$FLASH_BIN not found"                     >&2 ; exit 1 ; fi
  FLASH_STATIC_OPTIONS="-p 33 -q ";
  FLASH="$FLASH_BIN $FLASH_STATIC_OPTIONS";
  M_SCALE_FLASH="0.9"; # scale factor for Flash option -M, i.e. -M ($M_SCALE_FLASH*$arl)
#                        where arl is the average read length
#                                                                                                            #
# -- fqtools: FASTQ file utilities (fqconvert, fqduplicate, fqextract & fqstats) --------------------------  #
#                                                                                                            #
  FQCONVERT_BIN=fqconvert;
  if [ ! $(command -v $FQCONVERT_BIN) ]; then echo "$FQCONVERT_BIN not found"             >&2 ; exit 1 ; fi
  FQCONVERT_STATIC_OPTIONS="-n 100000 ";
  FQCONVERT="$FQCONVERT_BIN $FQCONVERT_STATIC_OPTIONS";

  FQDUPLICATE_BIN=fqduplicate;
  if [ ! $(command -v $FQDUPLICATE_BIN) ]; then echo "$FQDUPLICATE_BIN not found"         >&2 ; exit 1 ; fi
  FQDUPLICATE_STATIC_OPTIONS="";
  FQDUPLICATE="$FQDUPLICATE_BIN $FQDUPLICATE_STATIC_OPTIONS";

  FQEXTRACT_BIN=fqextract;
  if [ ! $(command -v $FQEXTRACT_BIN) ]; then echo "$FQEXTRACT_BIN not found"             >&2 ; exit 1 ; fi
  FQEXTRACT_STATIC_OPTIONS="";
  FQEXTRACT="$FQEXTRACT_BIN $FQEXTRACT_STATIC_OPTIONS";

  FQSTATS_BIN=fqstats;
  if [ ! $(command -v $FQSTATS_BIN) ]; then echo "$FQSTATS_BIN not found"                 >&2 ; exit 1 ; fi
  FQSTATS_STATIC_OPTIONS="";
  FQSTATS="$FQSTATS_BIN $FQSTATS_STATIC_OPTIONS";
#                                                                                                            #
# -- Musket: multistage k-mer spectrum-based corrector ----------------------------------------------------  #
#                                                                                                            #
  MUSKET_BIN=musket;
  if [ ! $(command -v $MUSKET_BIN) ]; then echo "$MUSKET_BIN not found"                   >&2 ; exit 1 ; fi
  MUSKET_STATIC_OPTIONS="-inorder ";
  MUSKET="$MUSKET_BIN $MUSKET_STATIC_OPTIONS";
  K_MUSKET=21; # k-mer length for Musket
#                                                                                                            #
# -- ntCard: estimating occurrences of distinct canonical k-mers ------------------------------------------  #
#                                                                                                            #
  NTCARD_BIN=ntcard;
  if [ ! $(command -v $NTCARD_BIN) ]; then echo "$NTCARD_BIN not found"                   >&2 ; exit 1 ; fi
  NTCARD_STATIC_OPTIONS="";
  NTCARD="$NTCARD_BIN $NTCARD_STATIC_OPTIONS";
#                                                                                                            #
# -- ROCK: Reducing Over-Covering K-mers ------------------------------------------------------------------  #
#                                                                                                            #
  ROCK_BIN=rock;
  if [ ! $(command -v $ROCK_BIN) ]; then echo "$ROCK_BIN not found"                       >&2 ; exit 1 ; fi
  ROCK_STATIC_OPTIONS="-k 25 -q 10 -m 5 ";
  ROCK="$ROCK_BIN $ROCK_STATIC_OPTIONS";
  ROCK_MAXCOV=65500;
  K_ROCK=25; # k-mer length for ROCK
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ====================                                                                                       #
# = TECHNICAL OLIGOS =                                                                                       #
# ====================                                                                                       #
#                                                                                                            #
# -- POLY: poly-A and poly-C ------------------------------------------------------------------------------  #
#                                                                                                            #
read -r -d '' POLY <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
EOF
#                                                                                                            #
# -- NEXTERA ----------------------------------------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 2-3, 28-30)                   #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' NEXTERA <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Nextera Adapter  
CTGTCTCTTATACACATCT
>Nextera Mate Pair Adapter 2
AGATGTGTATAAGAGACAG
>Nextera Transposase Adapter R1
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Nextera Transposase Adapter R2
GTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter [H/N]701
CAAGCAGAAGACGGCATACGAGATtcgccttaGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]702
CAAGCAGAAGACGGCATACGAGATctagtacgGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]703
CAAGCAGAAGACGGCATACGAGATttctgcctGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]704
CAAGCAGAAGACGGCATACGAGATgctcaggaGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]705
CAAGCAGAAGACGGCATACGAGATaggagtccGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]706
CAAGCAGAAGACGGCATACGAGATcatgcctaGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]707
CAAGCAGAAGACGGCATACGAGATgtagagagGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]708
CAAGCAGAAGACGGCATACGAGATcctctctgGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]709
CAAGCAGAAGACGGCATACGAGATagcgtagcGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]710
CAAGCAGAAGACGGCATACGAGATcagcctcgGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]711
CAAGCAGAAGACGGCATACGAGATtgcctcttGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]712
CAAGCAGAAGACGGCATACGAGATtcctctacGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]714
CAAGCAGAAGACGGCATACGAGATtcatgagcGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]715
CAAGCAGAAGACGGCATACGAGATcctgagatGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]716
CAAGCAGAAGACGGCATACGAGATtagcgagtGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]718
CAAGCAGAAGACGGCATACGAGATgtagctccGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]719
CAAGCAGAAGACGGCATACGAGATtactacgcGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]720
CAAGCAGAAGACGGCATACGAGATaggctccgGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]721
CAAGCAGAAGACGGCATACGAGATgcagcgtaGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]722
CAAGCAGAAGACGGCATACGAGATctgcgcatGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]723
CAAGCAGAAGACGGCATACGAGATgagcgctaGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]724
CAAGCAGAAGACGGCATACGAGATcgctcagtGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]726
CAAGCAGAAGACGGCATACGAGATgtcttaggGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]727
CAAGCAGAAGACGGCATACGAGATactgatcgGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]728
CAAGCAGAAGACGGCATACGAGATtagctgcaGTCTCGTGGGCTCGG
>Index 1 (i7) Adapter [H/N]729
CAAGCAGAAGACGGCATACGAGATgacgtcgaGTCTCGTGGGCTCGG
>Index 2 (i5) Adapter [E/H/N/S]501
AATGATACGGCGACCACCGAGATCTACACtagatcgcTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]502
AATGATACGGCGACCACCGAGATCTACACctctctatTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]503
AATGATACGGCGACCACCGAGATCTACACtatcctctTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]504
AATGATACGGCGACCACCGAGATCTACACagagtagaTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]505
AATGATACGGCGACCACCGAGATCTACACgtaaggagTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]506
AATGATACGGCGACCACCGAGATCTACACactgcataTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]507
AATGATACGGCGACCACCGAGATCTACACaaggagtaTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]508
AATGATACGGCGACCACCGAGATCTACACctaagcctTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]510
AATGATACGGCGACCACCGAGATCTACACcgtctaatTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]511
AATGATACGGCGACCACCGAGATCTACACtctctccgTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]513
AATGATACGGCGACCACCGAGATCTACACtcgactagTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]515
AATGATACGGCGACCACCGAGATCTACACttctagctTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]516
AATGATACGGCGACCACCGAGATCTACACcctagagtTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]517
AATGATACGGCGACCACCGAGATCTACACgcgtaagaTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]518
AATGATACGGCGACCACCGAGATCTACACctattaagTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]520
AATGATACGGCGACCACCGAGATCTACACaaggctatTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]521
AATGATACGGCGACCACCGAGATCTACACgagccttaTCGTCGGCAGCGTC
>Index 2 (i5) Adapter [E/H/N/S]522
AATGATACGGCGACCACCGAGATCTACACttatgcgaTCGTCGGCAGCGTC
EOF
#                                                                                                            #
# -- IUDI: Illumina Unique Dual Indexes -------------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 2-27)                         #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' IUDI <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Nextera Adapter  
CTGTCTCTTATACACATCT
>Nextera Mate Pair Adapter 2
AGATGTGTATAAGAGACAG
>Nextera Transposase Adapter R1
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Nextera Transposase Adapter R2
GTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Illumina UD IDT Index 1 (i7) Adapter UDP0001
CAAGCAGAAGACGGCATACGAGATcgctcagttcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0001
AATGATACGGCGACCACCGAGATCTACACtcgtggagcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0002
CAAGCAGAAGACGGCATACGAGATtatctgacctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0002
AATGATACGGCGACCACCGAGATCTACACctacaagataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0003
CAAGCAGAAGACGGCATACGAGATatatgagacgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0003
AATGATACGGCGACCACCGAGATCTACACtatagtagctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0004
CAAGCAGAAGACGGCATACGAGATcttatggaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0004
AATGATACGGCGACCACCGAGATCTACACtgcctggtggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0005
CAAGCAGAAGACGGCATACGAGATtaatctcgtcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0005
AATGATACGGCGACCACCGAGATCTACACacattatcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0006
CAAGCAGAAGACGGCATACGAGATgcgcgatgttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0006
AATGATACGGCGACCACCGAGATCTACACgtccacttgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0007
CAAGCAGAAGACGGCATACGAGATagagcactagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0007
AATGATACGGCGACCACCGAGATCTACACtggaacagtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0008
CAAGCAGAAGACGGCATACGAGATtgccttgatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0008
AATGATACGGCGACCACCGAGATCTACACccttgttaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0009
CAAGCAGAAGACGGCATACGAGATctactcagtcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0009
AATGATACGGCGACCACCGAGATCTACACgttgatagtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0010
CAAGCAGAAGACGGCATACGAGATtcgtctgactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0010
AATGATACGGCGACCACCGAGATCTACACaccagcgacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0011
CAAGCAGAAGACGGCATACGAGATgaacatacggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0011
AATGATACGGCGACCACCGAGATCTACACcatacactgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0012
CAAGCAGAAGACGGCATACGAGATcctatgactcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0012
AATGATACGGCGACCACCGAGATCTACACgtgtggcgctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0013
CAAGCAGAAGACGGCATACGAGATtaatggcaagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0013
AATGATACGGCGACCACCGAGATCTACACatcacgaaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0014
CAAGCAGAAGACGGCATACGAGATgtgccgcttcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0014
AATGATACGGCGACCACCGAGATCTACACcggctctactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0015
CAAGCAGAAGACGGCATACGAGATcggcaatggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0015
AATGATACGGCGACCACCGAGATCTACACgaatgcacgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0016
CAAGCAGAAGACGGCATACGAGATgccgtaaccgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0016
AATGATACGGCGACCACCGAGATCTACACaagactatagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0017
CAAGCAGAAGACGGCATACGAGATaaccattctcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0017
AATGATACGGCGACCACCGAGATCTACACtcggcagcaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0018
CAAGCAGAAGACGGCATACGAGATggttgcctctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0018
AATGATACGGCGACCACCGAGATCTACACctaatgatggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0019
CAAGCAGAAGACGGCATACGAGATctaatgatggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0019
AATGATACGGCGACCACCGAGATCTACACggttgcctctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0020
CAAGCAGAAGACGGCATACGAGATtcggcctatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0020
AATGATACGGCGACCACCGAGATCTACACcgcacatggcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0021
CAAGCAGAAGACGGCATACGAGATagtcaaccatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0021
AATGATACGGCGACCACCGAGATCTACACggcctgtcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0022
CAAGCAGAAGACGGCATACGAGATgagcgcaataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0022
AATGATACGGCGACCACCGAGATCTACACctgtgttaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0023
CAAGCAGAAGACGGCATACGAGATaacaaggcgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0023
AATGATACGGCGACCACCGAGATCTACACtaaggaacgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0024
CAAGCAGAAGACGGCATACGAGATgtatgtagaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0024
AATGATACGGCGACCACCGAGATCTACACctaactgtaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0025
CAAGCAGAAGACGGCATACGAGATttctatggttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0025
AATGATACGGCGACCACCGAGATCTACACggcgagatggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0026
CAAGCAGAAGACGGCATACGAGATcctcgcaaccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0026
AATGATACGGCGACCACCGAGATCTACACaatagagcaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0027
CAAGCAGAAGACGGCATACGAGATtggatgcttaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0027
AATGATACGGCGACCACCGAGATCTACACtcaatccattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0028
CAAGCAGAAGACGGCATACGAGATatgtcgtggtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0028
AATGATACGGCGACCACCGAGATCTACACtcgtatgcggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0029
CAAGCAGAAGACGGCATACGAGATagagtgcggcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0029
AATGATACGGCGACCACCGAGATCTACACtccgacctcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0030
CAAGCAGAAGACGGCATACGAGATtgcctggtggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0030
AATGATACGGCGACCACCGAGATCTACACcttatggaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0031
CAAGCAGAAGACGGCATACGAGATtgcgtgtcacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0031
AATGATACGGCGACCACCGAGATCTACACgcttacggacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0032
CAAGCAGAAGACGGCATACGAGATcatacactgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0032
AATGATACGGCGACCACCGAGATCTACACgaacatacggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0033
CAAGCAGAAGACGGCATACGAGATcgtataatcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0033
AATGATACGGCGACCACCGAGATCTACACgtcgattacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0034
CAAGCAGAAGACGGCATACGAGATtacgcggctgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0034
AATGATACGGCGACCACCGAGATCTACACactagccgtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0035
CAAGCAGAAGACGGCATACGAGATgcgagttaccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0035
AATGATACGGCGACCACCGAGATCTACACaagttggtgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0036
CAAGCAGAAGACGGCATACGAGATtacggccggtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0036
AATGATACGGCGACCACCGAGATCTACACtggcaatattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0037
CAAGCAGAAGACGGCATACGAGATgtcgattacaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0037
AATGATACGGCGACCACCGAGATCTACACgatcaccgcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0038
CAAGCAGAAGACGGCATACGAGATctgtctgcacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0038
AATGATACGGCGACCACCGAGATCTACACtaccatccgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0039
CAAGCAGAAGACGGCATACGAGATcagccgattgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0039
AATGATACGGCGACCACCGAGATCTACACgctgtaggaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0040
CAAGCAGAAGACGGCATACGAGATtgactacataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0040
AATGATACGGCGACCACCGAGATCTACACcgcactaatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0041
CAAGCAGAAGACGGCATACGAGATattgccgagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0041
AATGATACGGCGACCACCGAGATCTACACgacaactgaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0042
CAAGCAGAAGACGGCATACGAGATgccattagacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0042
AATGATACGGCGACCACCGAGATCTACACagtggtcaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0043
CAAGCAGAAGACGGCATACGAGATggcgagatggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0043
AATGATACGGCGACCACCGAGATCTACACttctatggttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0044
CAAGCAGAAGACGGCATACGAGATtggctcgcagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0044
AATGATACGGCGACCACCGAGATCTACACaatccggccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0045
CAAGCAGAAGACGGCATACGAGATtagaataacgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0045
AATGATACGGCGACCACCGAGATCTACACccataaggttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0046
CAAGCAGAAGACGGCATACGAGATtaatggatctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0046
AATGATACGGCGACCACCGAGATCTACACatctctaccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0047
CAAGCAGAAGACGGCATACGAGATtatccaggacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0047
AATGATACGGCGACCACCGAGATCTACACcggtggcgaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0048
CAAGCAGAAGACGGCATACGAGATagtgccactgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0048
AATGATACGGCGACCACCGAGATCTACACtaacaataggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0049
CAAGCAGAAGACGGCATACGAGATgtgcaacactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0049
AATGATACGGCGACCACCGAGATCTACACctggtacacgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0050
CAAGCAGAAGACGGCATACGAGATacatggtgtcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0050
AATGATACGGCGACCACCGAGATCTACACtcaacgtgtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0051
CAAGCAGAAGACGGCATACGAGATgacagacaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0051
AATGATACGGCGACCACCGAGATCTACACactgttgtgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0052
CAAGCAGAAGACGGCATACGAGATtcttacatcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0052
AATGATACGGCGACCACCGAGATCTACACgtgcgtccttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0053
CAAGCAGAAGACGGCATACGAGATttacaattccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0053
AATGATACGGCGACCACCGAGATCTACACagcacatcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0054
CAAGCAGAAGACGGCATACGAGATaagcttatgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0054
AATGATACGGCGACCACCGAGATCTACACttccgtcgcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0055
CAAGCAGAAGACGGCATACGAGATtattcctcagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0055
AATGATACGGCGACCACCGAGATCTACACcttaaccactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0056
CAAGCAGAAGACGGCATACGAGATctcgtgcgttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0056
AATGATACGGCGACCACCGAGATCTACACgcctcggataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0057
CAAGCAGAAGACGGCATACGAGATttaggatagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0057
AATGATACGGCGACCACCGAGATCTACACcgtcgactggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0058
CAAGCAGAAGACGGCATACGAGATccgaagcgagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0058
AATGATACGGCGACCACCGAGATCTACACtactagtcaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0059
CAAGCAGAAGACGGCATACGAGATggaccaacagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0059
AATGATACGGCGACCACCGAGATCTACACatagaccgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0060
CAAGCAGAAGACGGCATACGAGATttccaggtaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0060
AATGATACGGCGACCACCGAGATCTACACacagttccagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0061
CAAGCAGAAGACGGCATACGAGATtgattagccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0061
AATGATACGGCGACCACCGAGATCTACACaggcatgtagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0062
CAAGCAGAAGACGGCATACGAGATtaacagtgttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0062
AATGATACGGCGACCACCGAGATCTACACgcaagtctcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0063
CAAGCAGAAGACGGCATACGAGATaccgcgcaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0063
AATGATACGGCGACCACCGAGATCTACACttggctccgcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0064
CAAGCAGAAGACGGCATACGAGATgttcgcgccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0064
AATGATACGGCGACCACCGAGATCTACACaactgatactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0065
CAAGCAGAAGACGGCATACGAGATagacacattaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0065
AATGATACGGCGACCACCGAGATCTACACgtaaggcataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0066
CAAGCAGAAGACGGCATACGAGATgcgttggtatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0066
AATGATACGGCGACCACCGAGATCTACACaattgctgcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0067
CAAGCAGAAGACGGCATACGAGATagcacatcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0067
AATGATACGGCGACCACCGAGATCTACACttacaattccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0068
CAAGCAGAAGACGGCATACGAGATttgttccgtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0068
AATGATACGGCGACCACCGAGATCTACACaacctagcacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0069
CAAGCAGAAGACGGCATACGAGATaagtactccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0069
AATGATACGGCGACCACCGAGATCTACACtctgtgtggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0070
CAAGCAGAAGACGGCATACGAGATacgtcaatacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0070
AATGATACGGCGACCACCGAGATCTACACggaattccaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0071
CAAGCAGAAGACGGCATACGAGATggtgtacaagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0071
AATGATACGGCGACCACCGAGATCTACACaagcgcgcttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0072
CAAGCAGAAGACGGCATACGAGATccacctgtgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0072
AATGATACGGCGACCACCGAGATCTACACtgagcgttgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0073
CAAGCAGAAGACGGCATACGAGATgttccgcaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0073
AATGATACGGCGACCACCGAGATCTACACatcataggctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0074
CAAGCAGAAGACGGCATACGAGATaccttatgaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0074
AATGATACGGCGACCACCGAGATCTACACtgttagaaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0075
CAAGCAGAAGACGGCATACGAGATcgctgcagagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0075
AATGATACGGCGACCACCGAGATCTACACgatggatgtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0076
CAAGCAGAAGACGGCATACGAGATgtagagtcagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0076
AATGATACGGCGACCACCGAGATCTACACacggccgtcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0077
CAAGCAGAAGACGGCATACGAGATggataccagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0077
AATGATACGGCGACCACCGAGATCTACACcgttgcttacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0078
CAAGCAGAAGACGGCATACGAGATcgcactaatgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0078
AATGATACGGCGACCACCGAGATCTACACtgactacataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0079
CAAGCAGAAGACGGCATACGAGATtcctgaccgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0079
AATGATACGGCGACCACCGAGATCTACACcggcctcgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0080
CAAGCAGAAGACGGCATACGAGATctggcttgccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0080
AATGATACGGCGACCACCGAGATCTACACcaagcatccgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0081
CAAGCAGAAGACGGCATACGAGATaccagcgacaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0081
AATGATACGGCGACCACCGAGATCTACACtcgtctgactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0082
CAAGCAGAAGACGGCATACGAGATttgtaacggtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0082
AATGATACGGCGACCACCGAGATCTACACctcatagcgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0083
CAAGCAGAAGACGGCATACGAGATgtaaggcataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0083
AATGATACGGCGACCACCGAGATCTACACagacacattaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0084
CAAGCAGAAGACGGCATACGAGATgtccacttgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0084
AATGATACGGCGACCACCGAGATCTACACgcgcgatgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0085
CAAGCAGAAGACGGCATACGAGATttaggtaccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0085
AATGATACGGCGACCACCGAGATCTACACcatgagtactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0086
CAAGCAGAAGACGGCATACGAGATggaattccaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0086
AATGATACGGCGACCACCGAGATCTACACacgtcaatacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0087
CAAGCAGAAGACGGCATACGAGATcatgtagaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0087
AATGATACGGCGACCACCGAGATCTACACgatacctcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0088
CAAGCAGAAGACGGCATACGAGATtacacgctccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0088
AATGATACGGCGACCACCGAGATCTACACatccgtaagtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0089
CAAGCAGAAGACGGCATACGAGATgcttacggacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0089
AATGATACGGCGACCACCGAGATCTACACcgtgtatcttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0090
CAAGCAGAAGACGGCATACGAGATcgcttgaagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0090
AATGATACGGCGACCACCGAGATCTACACgaaccatgaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0091
CAAGCAGAAGACGGCATACGAGATcgccttctgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0091
AATGATACGGCGACCACCGAGATCTACACggccatcataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0092
CAAGCAGAAGACGGCATACGAGATataccaacgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0092
AATGATACGGCGACCACCGAGATCTACACacatacttccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0093
CAAGCAGAAGACGGCATACGAGATctggatatgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0093
AATGATACGGCGACCACCGAGATCTACACtatgtgcaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0094
CAAGCAGAAGACGGCATACGAGATcaatctatgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0094
AATGATACGGCGACCACCGAGATCTACACgattaaggtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0095
CAAGCAGAAGACGGCATACGAGATggtggaatacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0095
AATGATACGGCGACCACCGAGATCTACACatgtagacaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0096
CAAGCAGAAGACGGCATACGAGATtggacggaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0096
AATGATACGGCGACCACCGAGATCTACACcacatcggtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0097
CAAGCAGAAGACGGCATACGAGATctgaccggcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0097
AATGATACGGCGACCACCGAGATCTACACcctgatacaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0098
CAAGCAGAAGACGGCATACGAGATgaattgagtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0098
AATGATACGGCGACCACCGAGATCTACACttaagttgtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0099
CAAGCAGAAGACGGCATACGAGATgcgtgtgagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0099
AATGATACGGCGACCACCGAGATCTACACcggacagtgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0100
CAAGCAGAAGACGGCATACGAGATtctccattgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0100
AATGATACGGCGACCACCGAGATCTACACgcactacaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0101
CAAGCAGAAGACGGCATACGAGATacatgcatatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0101
AATGATACGGCGACCACCGAGATCTACACtggtgcctggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0102
CAAGCAGAAGACGGCATACGAGATcaggcgccatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0102
AATGATACGGCGACCACCGAGATCTACACtccacggcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0103
CAAGCAGAAGACGGCATACGAGATacataacggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0103
AATGATACGGCGACCACCGAGATCTACACttgtagtgtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0104
CAAGCAGAAGACGGCATACGAGATttaatagaccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0104
AATGATACGGCGACCACCGAGATCTACACccacgacacgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0105
CAAGCAGAAGACGGCATACGAGATacgattgctgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0105
AATGATACGGCGACCACCGAGATCTACACtgtgatgtatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0106
CAAGCAGAAGACGGCATACGAGATttctacagaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0106
AATGATACGGCGACCACCGAGATCTACACgagcgcaataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0107
CAAGCAGAAGACGGCATACGAGATtattgcgttcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0107
AATGATACGGCGACCACCGAGATCTACACatcttactgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0108
CAAGCAGAAGACGGCATACGAGATcatgagtactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0108
AATGATACGGCGACCACCGAGATCTACACatgtcgtggtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0109
CAAGCAGAAGACGGCATACGAGATtaattctaccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0109
AATGATACGGCGACCACCGAGATCTACACgtagccatcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0110
CAAGCAGAAGACGGCATACGAGATacgctaattaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0110
AATGATACGGCGACCACCGAGATCTACACtggttaagaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0111
CAAGCAGAAGACGGCATACGAGATccttgttaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0111
AATGATACGGCGACCACCGAGATCTACACtgttgttcgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0112
CAAGCAGAAGACGGCATACGAGATgtagccatcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0112
AATGATACGGCGACCACCGAGATCTACACccaacaacatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0113
CAAGCAGAAGACGGCATACGAGATcttgtaattcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0113
AATGATACGGCGACCACCGAGATCTACACaccggctcagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0114
CAAGCAGAAGACGGCATACGAGATtccaattctaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0114
AATGATACGGCGACCACCGAGATCTACACgttaatctgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0115
CAAGCAGAAGACGGCATACGAGATagagctgcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0115
AATGATACGGCGACCACCGAGATCTACACcggctaacgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0116
CAAGCAGAAGACGGCATACGAGATcttcgccgatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0116
AATGATACGGCGACCACCGAGATCTACACtccaagaattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0117
CAAGCAGAAGACGGCATACGAGATtcggtcacggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0117
AATGATACGGCGACCACCGAGATCTACACccgaacgttgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0118
CAAGCAGAAGACGGCATACGAGATgaacaagtatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0118
AATGATACGGCGACCACCGAGATCTACACtaaccgccgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0119
CAAGCAGAAGACGGCATACGAGATaattggcggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0119
AATGATACGGCGACCACCGAGATCTACACctccgtgctgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0120
CAAGCAGAAGACGGCATACGAGATggcctgtcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0120
AATGATACGGCGACCACCGAGATCTACACcattccagctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0121
CAAGCAGAAGACGGCATACGAGATtaggttctctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0121
AATGATACGGCGACCACCGAGATCTACACggttatgctaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0122
CAAGCAGAAGACGGCATACGAGATacacaatatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0122
AATGATACGGCGACCACCGAGATCTACACaccacacggtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0123
CAAGCAGAAGACGGCATACGAGATttcctgtacgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0123
AATGATACGGCGACCACCGAGATCTACACtaggttctctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0124
CAAGCAGAAGACGGCATACGAGATggtaacgcagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0124
AATGATACGGCGACCACCGAGATCTACACtatggctcgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0125
CAAGCAGAAGACGGCATACGAGATtccacggcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0125
AATGATACGGCGACCACCGAGATCTACACctcgtgcgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0126
CAAGCAGAAGACGGCATACGAGATgatacctcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0126
AATGATACGGCGACCACCGAGATCTACACccagttggcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0127
CAAGCAGAAGACGGCATACGAGATcaacgtcagcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0127
AATGATACGGCGACCACCGAGATCTACACtgttcgcattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0128
CAAGCAGAAGACGGCATACGAGATcggttattagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0128
AATGATACGGCGACCACCGAGATCTACACaaccgcatcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0129
CAAGCAGAAGACGGCATACGAGATcgcgcctagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0129
AATGATACGGCGACCACCGAGATCTACACcgaaggttaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0130
CAAGCAGAAGACGGCATACGAGATtcttggctatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0130
AATGATACGGCGACCACCGAGATCTACACagtgccactgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0131
CAAGCAGAAGACGGCATACGAGATtcacaccgaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0131
AATGATACGGCGACCACCGAGATCTACACgaacaagtatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0132
CAAGCAGAAGACGGCATACGAGATaacgttacatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0132
AATGATACGGCGACCACCGAGATCTACACacgattgctgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0133
CAAGCAGAAGACGGCATACGAGATcggcctcgttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0133
AATGATACGGCGACCACCGAGATCTACACatacctggatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0134
CAAGCAGAAGACGGCATACGAGATcataacaccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0134
AATGATACGGCGACCACCGAGATCTACACtccaattctaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0135
CAAGCAGAAGACGGCATACGAGATacagaggccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0135
AATGATACGGCGACCACCGAGATCTACACtgagacagcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0136
CAAGCAGAAGACGGCATACGAGATtggtgcctggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0136
AATGATACGGCGACCACCGAGATCTACACacgctaattaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0137
CAAGCAGAAGACGGCATACGAGATtaggaaccggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0137
AATGATACGGCGACCACCGAGATCTACACtatattcgagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0138
CAAGCAGAAGACGGCATACGAGATaatattggccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0138
AATGATACGGCGACCACCGAGATCTACACcggtccgataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0139
CAAGCAGAAGACGGCATACGAGATataggtattcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0139
AATGATACGGCGACCACCGAGATCTACACacaatagagtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0140
CAAGCAGAAGACGGCATACGAGATccttcacgtaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0140
AATGATACGGCGACCACCGAGATCTACACcggttattagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0141
CAAGCAGAAGACGGCATACGAGATggccaataagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0141
AATGATACGGCGACCACCGAGATCTACACgataacaagtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0142
CAAGCAGAAGACGGCATACGAGATcagtagttgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0142
AATGATACGGCGACCACCGAGATCTACACagttatcacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0143
CAAGCAGAAGACGGCATACGAGATttcatccaacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0143
AATGATACGGCGACCACCGAGATCTACACttccaggtaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0144
CAAGCAGAAGACGGCATACGAGATcaattggattGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0144
AATGATACGGCGACCACCGAGATCTACACcatgtagaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0145
CAAGCAGAAGACGGCATACGAGATggccatcataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0145
AATGATACGGCGACCACCGAGATCTACACgattgtcataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0146
CAAGCAGAAGACGGCATACGAGATaattgctgcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0146
AATGATACGGCGACCACCGAGATCTACACattccgctatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0147
CAAGCAGAAGACGGCATACGAGATtaaggaacgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0147
AATGATACGGCGACCACCGAGATCTACACgaccgctgtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0148
CAAGCAGAAGACGGCATACGAGATctatacgcggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0148
AATGATACGGCGACCACCGAGATCTACACtaggaaccggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0149
CAAGCAGAAGACGGCATACGAGATattcagaatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0149
AATGATACGGCGACCACCGAGATCTACACagcggtggacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0150
CAAGCAGAAGACGGCATACGAGATgtattctctaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0150
AATGATACGGCGACCACCGAGATCTACACtatagattcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0151
CAAGCAGAAGACGGCATACGAGATcctgatacaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0151
AATGATACGGCGACCACCGAGATCTACACacagaggccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0152
CAAGCAGAAGACGGCATACGAGATgaccgctgtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0152
AATGATACGGCGACCACCGAGATCTACACattcctattgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0153
CAAGCAGAAGACGGCATACGAGATttcagcgtggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0153
AATGATACGGCGACCACCGAGATCTACACtattcctcagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0154
CAAGCAGAAGACGGCATACGAGATaactccgaacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0154
AATGATACGGCGACCACCGAGATCTACACcgccttctgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0155
CAAGCAGAAGACGGCATACGAGATattccgctatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0155
AATGATACGGCGACCACCGAGATCTACACgcgcagagtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0156
CAAGCAGAAGACGGCATACGAGATtgaatattgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0156
AATGATACGGCGACCACCGAGATCTACACggcgccaattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0157
CAAGCAGAAGACGGCATACGAGATcgcaatctagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0157
AATGATACGGCGACCACCGAGATCTACACagatatggcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0158
CAAGCAGAAGACGGCATACGAGATaaccgcatcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0158
AATGATACGGCGACCACCGAGATCTACACcctgcttggtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0159
CAAGCAGAAGACGGCATACGAGATctagtccggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0159
AATGATACGGCGACCACCGAGATCTACACgacgaacaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0160
CAAGCAGAAGACGGCATACGAGATgctccgtcacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0160
AATGATACGGCGACCACCGAGATCTACACtggcggtccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0161
CAAGCAGAAGACGGCATACGAGATagatggaattGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0161
AATGATACGGCGACCACCGAGATCTACACcttcagttacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0162
CAAGCAGAAGACGGCATACGAGATacaccgttaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0162
AATGATACGGCGACCACCGAGATCTACACtcctgaccgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0163
CAAGCAGAAGACGGCATACGAGATgataacaagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0163
AATGATACGGCGACCACCGAGATCTACACcgcgcctagaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0164
CAAGCAGAAGACGGCATACGAGATctggtacacgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0164
AATGATACGGCGACCACCGAGATCTACACaggataagttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0165
CAAGCAGAAGACGGCATACGAGATcgaaggttaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0165
AATGATACGGCGACCACCGAGATCTACACaggccagacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0166
CAAGCAGAAGACGGCATACGAGATatcgcatatgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0166
AATGATACGGCGACCACCGAGATCTACACccttgaacggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0167
CAAGCAGAAGACGGCATACGAGATatcataggctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0167
AATGATACGGCGACCACCGAGATCTACACcaccacctacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0168
CAAGCAGAAGACGGCATACGAGATgattgtcataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0168
AATGATACGGCGACCACCGAGATCTACACttgcttgtatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0169
CAAGCAGAAGACGGCATACGAGATccaacaacatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0169
AATGATACGGCGACCACCGAGATCTACACcaatctatgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0170
CAAGCAGAAGACGGCATACGAGATttggtggtgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0170
AATGATACGGCGACCACCGAGATCTACACtggtactgatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0171
CAAGCAGAAGACGGCATACGAGATgcgaacgcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0171
AATGATACGGCGACCACCGAGATCTACACttcatccaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0172
CAAGCAGAAGACGGCATACGAGATcaaccggaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0172
AATGATACGGCGACCACCGAGATCTACACcataacaccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0173
CAAGCAGAAGACGGCATACGAGATagcggtggacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0173
AATGATACGGCGACCACCGAGATCTACACtcctattagcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0174
CAAGCAGAAGACGGCATACGAGATgacgaacaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0174
AATGATACGGCGACCACCGAGATCTACACtctctagattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0175
CAAGCAGAAGACGGCATACGAGATccactggtccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0175
AATGATACGGCGACCACCGAGATCTACACcgcgagcctaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0176
CAAGCAGAAGACGGCATACGAGATtgttagaaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0176
AATGATACGGCGACCACCGAGATCTACACgataagctctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0177
CAAGCAGAAGACGGCATACGAGATtatattcgagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0177
AATGATACGGCGACCACCGAGATCTACACgagatgtcgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0178
CAAGCAGAAGACGGCATACGAGATcgcgacgatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0178
AATGATACGGCGACCACCGAGATCTACACctggatatgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0179
CAAGCAGAAGACGGCATACGAGATgcctcggataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0179
AATGATACGGCGACCACCGAGATCTACACggccaataagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0180
CAAGCAGAAGACGGCATACGAGATtgagacagcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0180
AATGATACGGCGACCACCGAGATCTACACattactcaccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0181
CAAGCAGAAGACGGCATACGAGATtgttcgcattGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0181
AATGATACGGCGACCACCGAGATCTACACaattggcggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0182
CAAGCAGAAGACGGCATACGAGATtccaagaattGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0182
AATGATACGGCGACCACCGAGATCTACACttgtcaacttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0183
CAAGCAGAAGACGGCATACGAGATgctgtaggaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0183
AATGATACGGCGACCACCGAGATCTACACggcgaattctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0184
CAAGCAGAAGACGGCATACGAGATatacctggatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0184
AATGATACGGCGACCACCGAGATCTACACcaacgtcagcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0185
CAAGCAGAAGACGGCATACGAGATgttggaccgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0185
AATGATACGGCGACCACCGAGATCTACACtcttacatcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0186
CAAGCAGAAGACGGCATACGAGATaccaagttacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0186
AATGATACGGCGACCACCGAGATCTACACcgccatacctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0187
CAAGCAGAAGACGGCATACGAGATgtgtggcgctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0187
AATGATACGGCGACCACCGAGATCTACACctaatgtcttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0188
CAAGCAGAAGACGGCATACGAGATggcagtagcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0188
AATGATACGGCGACCACCGAGATCTACACcaaccggaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0189
CAAGCAGAAGACGGCATACGAGATtgcggtgttgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0189
AATGATACGGCGACCACCGAGATCTACACggcagtagcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0190
CAAGCAGAAGACGGCATACGAGATgattaaggtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0190
AATGATACGGCGACCACCGAGATCTACACttaggatagaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0191
CAAGCAGAAGACGGCATACGAGATcaacattcaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0191
AATGATACGGCGACCACCGAGATCTACACcgcaatctagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0192
CAAGCAGAAGACGGCATACGAGATgtgttaccggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0192
AATGATACGGCGACCACCGAGATCTACACgagttgtactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0193
CAAGCAGAAGACGGCATACGAGATtatcatgagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0193
AATGATACGGCGACCACCGAGATCTACACaacacgtggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0194
CAAGCAGAAGACGGCATACGAGATcttggcctcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0194
AATGATACGGCGACCACCGAGATCTACACgtgttaccggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0195
CAAGCAGAAGACGGCATACGAGATgtctcgtgaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0195
AATGATACGGCGACCACCGAGATCTACACagattgttacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0196
CAAGCAGAAGACGGCATACGAGATccatccacgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0196
AATGATACGGCGACCACCGAGATCTACACttgaccaatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0197
CAAGCAGAAGACGGCATACGAGATacaaccaggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0197
AATGATACGGCGACCACCGAGATCTACACctgaccggcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0198
CAAGCAGAAGACGGCATACGAGATagcagaattaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0198
AATGATACGGCGACCACCGAGATCTACACtctcatcaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0199
CAAGCAGAAGACGGCATACGAGATcagtcgtgcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0199
AATGATACGGCGACCACCGAGATCTACACggaccaacagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0200
CAAGCAGAAGACGGCATACGAGATgtctaacctcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0200
AATGATACGGCGACCACCGAGATCTACACaatgtattgcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0201
CAAGCAGAAGACGGCATACGAGATgaactcggttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0201
AATGATACGGCGACCACCGAGATCTACACgatctctggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0202
CAAGCAGAAGACGGCATACGAGATagttatcacaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0202
AATGATACGGCGACCACCGAGATCTACACcaggcgccatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0203
CAAGCAGAAGACGGCATACGAGATgtagcatactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0203
AATGATACGGCGACCACCGAGATCTACACttaatagaccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0204
CAAGCAGAAGACGGCATACGAGATcttcagttacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0204
AATGATACGGCGACCACCGAGATCTACACggagtcgcgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0205
CAAGCAGAAGACGGCATACGAGATagtccgaggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0205
AATGATACGGCGACCACCGAGATCTACACaacgccagagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0206
CAAGCAGAAGACGGCATACGAGATacagttccagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0206
AATGATACGGCGACCACCGAGATCTACACcgtaattaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0207
CAAGCAGAAGACGGCATACGAGATccgcatattcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0207
AATGATACGGCGACCACCGAGATCTACACacgagactgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0208
CAAGCAGAAGACGGCATACGAGATttatccgatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0208
AATGATACGGCGACCACCGAGATCTACACgtatcggccgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0209
CAAGCAGAAGACGGCATACGAGATatagtctagcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0209
AATGATACGGCGACCACCGAGATCTACACaatacgacatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0210
CAAGCAGAAGACGGCATACGAGATtatagtagctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0210
AATGATACGGCGACCACCGAGATCTACACgttatatggcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0211
CAAGCAGAAGACGGCATACGAGATactccggtggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0211
AATGATACGGCGACCACCGAGATCTACACgcctgccatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0212
CAAGCAGAAGACGGCATACGAGATgtgcggtaagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0212
AATGATACGGCGACCACCGAGATCTACACtaagacctatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0213
CAAGCAGAAGACGGCATACGAGATgatatcctaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0213
AATGATACGGCGACCACCGAGATCTACACtataccatggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0214
CAAGCAGAAGACGGCATACGAGATtcgcgtataaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0214
AATGATACGGCGACCACCGAGATCTACACgccgtctgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0215
CAAGCAGAAGACGGCATACGAGATattctaagcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0215
AATGATACGGCGACCACCGAGATCTACACcagagtgataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0216
CAAGCAGAAGACGGCATACGAGATagcgcttcggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0216
AATGATACGGCGACCACCGAGATCTACACtgctaactatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0217
CAAGCAGAAGACGGCATACGAGATgttgatagtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0217
AATGATACGGCGACCACCGAGATCTACACtcagttaatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0218
CAAGCAGAAGACGGCATACGAGATaatagagcaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0218
AATGATACGGCGACCACCGAGATCTACACgtgaccttgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0219
CAAGCAGAAGACGGCATACGAGATctaactgtaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0219
AATGATACGGCGACCACCGAGATCTACACacatgcatatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0220
CAAGCAGAAGACGGCATACGAGATgcgtacttagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0220
AATGATACGGCGACCACCGAGATCTACACaacatacctaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0221
CAAGCAGAAGACGGCATACGAGATtaccgaactaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0221
AATGATACGGCGACCACCGAGATCTACACccatgtgtagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0222
CAAGCAGAAGACGGCATACGAGATgtagtaatagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0222
AATGATACGGCGACCACCGAGATCTACACgagtctctccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0223
CAAGCAGAAGACGGCATACGAGATggttatgctaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0223
AATGATACGGCGACCACCGAGATCTACACgctatgcgcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0224
CAAGCAGAAGACGGCATACGAGATacaatagagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0224
AATGATACGGCGACCACCGAGATCTACACatcgcatatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0225
CAAGCAGAAGACGGCATACGAGATgcttccactaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0225
AATGATACGGCGACCACCGAGATCTACACagtacctataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0226
CAAGCAGAAGACGGCATACGAGATagatatggcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0226
AATGATACGGCGACCACCGAGATCTACACgaccggagatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0227
CAAGCAGAAGACGGCATACGAGATaatatgaagcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0227
AATGATACGGCGACCACCGAGATCTACACcgttcagcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0228
CAAGCAGAAGACGGCATACGAGATtagcgctagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0228
AATGATACGGCGACCACCGAGATCTACACttacttcctcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0229
CAAGCAGAAGACGGCATACGAGATagttaagagcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0229
AATGATACGGCGACCACCGAGATCTACACcacgtccaccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0230
CAAGCAGAAGACGGCATACGAGATcagataccacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0230
AATGATACGGCGACCACCGAGATCTACACgctactatctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0231
CAAGCAGAAGACGGCATACGAGATacggccgtcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0231
AATGATACGGCGACCACCGAGATCTACACagtcaaccatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0232
CAAGCAGAAGACGGCATACGAGATgtaattactgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0232
AATGATACGGCGACCACCGAGATCTACACcgaggcggtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0233
CAAGCAGAAGACGGCATACGAGATaagtcttgtaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0233
AATGATACGGCGACCACCGAGATCTACACcaggtgttcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0234
CAAGCAGAAGACGGCATACGAGATgtcaccacagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0234
AATGATACGGCGACCACCGAGATCTACACgacagacaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0235
CAAGCAGAAGACGGCATACGAGATattagtggagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0235
AATGATACGGCGACCACCGAGATCTACACtgtacttgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0236
CAAGCAGAAGACGGCATACGAGATtgctaactatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0236
AATGATACGGCGACCACCGAGATCTACACctctaagtagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0237
CAAGCAGAAGACGGCATACGAGATtaagacctatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0237
AATGATACGGCGACCACCGAGATCTACACgtcaccacagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0238
CAAGCAGAAGACGGCATACGAGATtggttaagaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0238
AATGATACGGCGACCACCGAGATCTACACtctacataccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0239
CAAGCAGAAGACGGCATACGAGATactcttccttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0239
AATGATACGGCGACCACCGAGATCTACACcacgttaggcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0240
CAAGCAGAAGACGGCATACGAGATgtctccttccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0240
AATGATACGGCGACCACCGAGATCTACACtggtgagtctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0241
CAAGCAGAAGACGGCATACGAGATtccgcgttcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0241
AATGATACGGCGACCACCGAGATCTACACcttcgaaggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0242
CAAGCAGAAGACGGCATACGAGATaggttgcaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0242
AATGATACGGCGACCACCGAGATCTACACgtagagtcagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0243
CAAGCAGAAGACGGCATACGAGATgaaccatgaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0243
AATGATACGGCGACCACCGAGATCTACACgacattgtcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0244
CAAGCAGAAGACGGCATACGAGATttgagaggatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0244
AATGATACGGCGACCACCGAGATCTACACtccgcaaggcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0245
CAAGCAGAAGACGGCATACGAGATtggtctagtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0245
AATGATACGGCGACCACCGAGATCTACACactgccttatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0246
CAAGCAGAAGACGGCATACGAGATagtggataatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0246
AATGATACGGCGACCACCGAGATCTACACtacgcacgtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0247
CAAGCAGAAGACGGCATACGAGATggcacgccatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0247
AATGATACGGCGACCACCGAGATCTACACcgcttgaagtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0248
CAAGCAGAAGACGGCATACGAGATgatctctggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0248
AATGATACGGCGACCACCGAGATCTACACctgcacttcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0249
CAAGCAGAAGACGGCATACGAGATtgctggacatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0249
AATGATACGGCGACCACCGAGATCTACACcagcggacaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0250
CAAGCAGAAGACGGCATACGAGATccgaacgttgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0250
AATGATACGGCGACCACCGAGATCTACACggatccgcatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0251
CAAGCAGAAGACGGCATACGAGATattaatacgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0251
AATGATACGGCGACCACCGAGATCTACACtgcggtgttgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0252V2
CAAGCAGAAGACGGCATACGAGATccagattcggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0252V2
AATGATACGGCGACCACCGAGATCTACACatgaatcaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0253
CAAGCAGAAGACGGCATACGAGATggtattgagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0253
AATGATACGGCGACCACCGAGATCTACACgacgttcgcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0254
CAAGCAGAAGACGGCATACGAGATcaagatgcttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0254
AATGATACGGCGACCACCGAGATCTACACcattcaacaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0255
CAAGCAGAAGACGGCATACGAGATacgagactgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0255
AATGATACGGCGACCACCGAGATCTACACcacggattatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0256
CAAGCAGAAGACGGCATACGAGATttatcttgcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0256
AATGATACGGCGACCACCGAGATCTACACttgaggacggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0257
CAAGCAGAAGACGGCATACGAGATagattgttacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0257
AATGATACGGCGACCACCGAGATCTACACctctgtatacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0258V2
CAAGCAGAAGACGGCATACGAGATtataccatggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0258V2
AATGATACGGCGACCACCGAGATCTACACtctcgcggagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0259
CAAGCAGAAGACGGCATACGAGATaacggtatgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0259
AATGATACGGCGACCACCGAGATCTACACggtaacgcagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0260
CAAGCAGAAGACGGCATACGAGATcaatggcgccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0260
AATGATACGGCGACCACCGAGATCTACACaccgcgcaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0261
CAAGCAGAAGACGGCATACGAGATctaattcgctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0261
AATGATACGGCGACCACCGAGATCTACACagccggaacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0262
CAAGCAGAAGACGGCATACGAGATcatggtctaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0262
AATGATACGGCGACCACCGAGATCTACACtcctaggaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0263
CAAGCAGAAGACGGCATACGAGATatactgtgtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0263
AATGATACGGCGACCACCGAGATCTACACttgagcctaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0264
CAAGCAGAAGACGGCATACGAGATgccgacaagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0264
AATGATACGGCGACCACCGAGATCTACACccacctgtgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0265
CAAGCAGAAGACGGCATACGAGATcgaggcggtaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0265
AATGATACGGCGACCACCGAGATCTACACcctcgcaaccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0266
CAAGCAGAAGACGGCATACGAGATgatataacagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0266
AATGATACGGCGACCACCGAGATCTACACgtatagctgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0267
CAAGCAGAAGACGGCATACGAGATtcgccggttaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0267
AATGATACGGCGACCACCGAGATCTACACgctacattagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0268
CAAGCAGAAGACGGCATACGAGATagactctcttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0268
AATGATACGGCGACCACCGAGATCTACACtacgaatcttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0269
CAAGCAGAAGACGGCATACGAGATgctcgcctacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0269
AATGATACGGCGACCACCGAGATCTACACtaggagcgcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0270
CAAGCAGAAGACGGCATACGAGATaggataagttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0270
AATGATACGGCGACCACCGAGATCTACACgtactggcgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0271
CAAGCAGAAGACGGCATACGAGATgagacataatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0271
AATGATACGGCGACCACCGAGATCTACACagttaagagcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0272
CAAGCAGAAGACGGCATACGAGATagctgttataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0272
AATGATACGGCGACCACCGAGATCTACACtcgcgtataaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0273
CAAGCAGAAGACGGCATACGAGATgtatcattggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0273
AATGATACGGCGACCACCGAGATCTACACgagtgtgccgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0274
CAAGCAGAAGACGGCATACGAGATaataggcctcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0274
AATGATACGGCGACCACCGAGATCTACACctagtccggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0275
CAAGCAGAAGACGGCATACGAGATccgcttagctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0275
AATGATACGGCGACCACCGAGATCTACACattaatacgcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0276
CAAGCAGAAGACGGCATACGAGATtcctaggaagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0276
AATGATACGGCGACCACCGAGATCTACACcctagagtatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0277
CAAGCAGAAGACGGCATACGAGATtcacagatcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0277
AATGATACGGCGACCACCGAGATCTACACtaggaagactTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0278
CAAGCAGAAGACGGCATACGAGATacttgtccacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0278
AATGATACGGCGACCACCGAGATCTACACccgtggccttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0279
CAAGCAGAAGACGGCATACGAGATtgtacttgttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0279
AATGATACGGCGACCACCGAGATCTACACggatatatccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0280
CAAGCAGAAGACGGCATACGAGATcacttaatctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0280
AATGATACGGCGACCACCGAGATCTACACcacctcttggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0281
CAAGCAGAAGACGGCATACGAGATcagagtgataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0281
AATGATACGGCGACCACCGAGATCTACACaacgttacatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0282
CAAGCAGAAGACGGCATACGAGATggcgaattctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0282
AATGATACGGCGACCACCGAGATCTACACcggcaagctcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0283
CAAGCAGAAGACGGCATACGAGATagtggtcaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0283
AATGATACGGCGACCACCGAGATCTACACtcttggctatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0284
CAAGCAGAAGACGGCATACGAGATcattccagctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0284
AATGATACGGCGACCACCGAGATCTACACacggaatgcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0285
CAAGCAGAAGACGGCATACGAGATctcgttatcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0285
AATGATACGGCGACCACCGAGATCTACACgttccgcaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0286
CAAGCAGAAGACGGCATACGAGATccttactatgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0286
AATGATACGGCGACCACCGAGATCTACACaccaagttacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0287
CAAGCAGAAGACGGCATACGAGATagaagccaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0287
AATGATACGGCGACCACCGAGATCTACACtggctcgcagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0288
CAAGCAGAAGACGGCATACGAGATtaatcggtacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0288
AATGATACGGCGACCACCGAGATCTACACaactaacgttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0289V2
CAAGCAGAAGACGGCATACGAGATgctactatctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0289V2
AATGATACGGCGACCACCGAGATCTACACggcacgccatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0290V2
CAAGCAGAAGACGGCATACGAGATgtcttctaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0290V2
AATGATACGGCGACCACCGAGATCTACACgcaggctggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0291V2
CAAGCAGAAGACGGCATACGAGATatgtgcgagcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0291V2
AATGATACGGCGACCACCGAGATCTACACatggcttaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0292
CAAGCAGAAGACGGCATACGAGATtggcaatattGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0292
AATGATACGGCGACCACCGAGATCTACACcggtgacaccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0293
CAAGCAGAAGACGGCATACGAGATgaatgcacgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0293
AATGATACGGCGACCACCGAGATCTACACgcgttggtatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0294
CAAGCAGAAGACGGCATACGAGATcgtgtatcttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0294
AATGATACGGCGACCACCGAGATCTACACtgtgctaacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0295
CAAGCAGAAGACGGCATACGAGATattcattgcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0295
AATGATACGGCGACCACCGAGATCTACACccagaagtaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0296
CAAGCAGAAGACGGCATACGAGATtccttcatagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0296
AATGATACGGCGACCACCGAGATCTACACcttatacctgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0297
CAAGCAGAAGACGGCATACGAGATtctagtcttcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0297
AATGATACGGCGACCACCGAGATCTACACactagaacttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0298
CAAGCAGAAGACGGCATACGAGATctcgactcctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0298
AATGATACGGCGACCACCGAGATCTACACttaggcttacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0299
CAAGCAGAAGACGGCATACGAGATagtgagtgaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0299
AATGATACGGCGACCACCGAGATCTACACtatcatgagaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0300
CAAGCAGAAGACGGCATACGAGATgaagcggaccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0300
AATGATACGGCGACCACCGAGATCTACACctcacacaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0301V2
CAAGCAGAAGACGGCATACGAGATcaagccactaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0301V2
AATGATACGGCGACCACCGAGATCTACACagttacttggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0302
CAAGCAGAAGACGGCATACGAGATggacctcaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0302
AATGATACGGCGACCACCGAGATCTACACcggattatatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0303
CAAGCAGAAGACGGCATACGAGATgagtctctccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0303
AATGATACGGCGACCACCGAGATCTACACttgaagcagaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0304
CAAGCAGAAGACGGCATACGAGATaacggagcggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0304
AATGATACGGCGACCACCGAGATCTACACtacggcgaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0305
CAAGCAGAAGACGGCATACGAGATtgtgatgtatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0305
AATGATACGGCGACCACCGAGATCTACACtctccattgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0306
CAAGCAGAAGACGGCATACGAGATaacatacctaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0306
AATGATACGGCGACCACCGAGATCTACACcgagaccaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0307
CAAGCAGAAGACGGCATACGAGATgtgctaggtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0307
AATGATACGGCGACCACCGAGATCTACACtgctggacatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0308
CAAGCAGAAGACGGCATACGAGATcatacttgaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0308
AATGATACGGCGACCACCGAGATCTACACgatggtatcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0309
CAAGCAGAAGACGGCATACGAGATcttgtcttaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0309
AATGATACGGCGACCACCGAGATCTACACggcttaattgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0310
CAAGCAGAAGACGGCATACGAGATaagagaggtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0310
AATGATACGGCGACCACCGAGATCTACACctcgactcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0311
CAAGCAGAAGACGGCATACGAGATtgcacgagaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0311
AATGATACGGCGACCACCGAGATCTACACatacacagagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0312
CAAGCAGAAGACGGCATACGAGATacttcctagcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0312
AATGATACGGCGACCACCGAGATCTACACtctcggacgaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0313
CAAGCAGAAGACGGCATACGAGATgtgctattaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0313
AATGATACGGCGACCACCGAGATCTACACaccacgtctgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0314
CAAGCAGAAGACGGCATACGAGATagcgtgaatgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0314
AATGATACGGCGACCACCGAGATCTACACgttgtactcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0315
CAAGCAGAAGACGGCATACGAGATccttagtgccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0315
AATGATACGGCGACCACCGAGATCTACACtcaggtcaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0316
CAAGCAGAAGACGGCATACGAGATtgtaccgaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0316
AATGATACGGCGACCACCGAGATCTACACagtccgaggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0317
CAAGCAGAAGACGGCATACGAGATggagattagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0317
AATGATACGGCGACCACCGAGATCTACACcacttaatctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0318
CAAGCAGAAGACGGCATACGAGATtactaacacaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0318
AATGATACGGCGACCACCGAGATCTACACtactctgttaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0319
CAAGCAGAAGACGGCATACGAGATtaggtcgttgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0319
AATGATACGGCGACCACCGAGATCTACACgcgactcgatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0320
CAAGCAGAAGACGGCATACGAGATatgccgaccgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0320
AATGATACGGCGACCACCGAGATCTACACctaggcaaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0321
CAAGCAGAAGACGGCATACGAGATctagcgtcgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0321
AATGATACGGCGACCACCGAGATCTACACcctcttcgaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0322
CAAGCAGAAGACGGCATACGAGATtgcctacgagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0322
AATGATACGGCGACCACCGAGATCTACACtcatcctcttTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0323
CAAGCAGAAGACGGCATACGAGATactagaacttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0323
AATGATACGGCGACCACCGAGATCTACACggtaagataaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0324
CAAGCAGAAGACGGCATACGAGATcacctcttggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0324
AATGATACGGCGACCACCGAGATCTACACaacgagccagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0325
CAAGCAGAAGACGGCATACGAGATaagcagatatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0325
AATGATACGGCGACCACCGAGATCTACACtagacaatctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0326
CAAGCAGAAGACGGCATACGAGATgccagatccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0326
AATGATACGGCGACCACCGAGATCTACACcaatgctgaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0327
CAAGCAGAAGACGGCATACGAGATttggattcaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0327
AATGATACGGCGACCACCGAGATCTACACgtcacggtgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0328
CAAGCAGAAGACGGCATACGAGATactagccgtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0328
AATGATACGGCGACCACCGAGATCTACACggtgtacaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0329
CAAGCAGAAGACGGCATACGAGATcggcaagctcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0329
AATGATACGGCGACCACCGAGATCTACACaggttgcaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0330
CAAGCAGAAGACGGCATACGAGATgaagctagctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0330
AATGATACGGCGACCACCGAGATCTACACtaatacggagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0331
CAAGCAGAAGACGGCATACGAGATacaaggattgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0331
AATGATACGGCGACCACCGAGATCTACACcgaagacgcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0332
CAAGCAGAAGACGGCATACGAGATgcaacaggtgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0332
AATGATACGGCGACCACCGAGATCTACACattgacacatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0333
CAAGCAGAAGACGGCATACGAGATcaaggtgacgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0333
AATGATACGGCGACCACCGAGATCTACACcagccgattgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0334
CAAGCAGAAGACGGCATACGAGATaccagtcattGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0334
AATGATACGGCGACCACCGAGATCTACACtctcacgcgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0335
CAAGCAGAAGACGGCATACGAGATccggaatcatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0335
AATGATACGGCGACCACCGAGATCTACACctctgacgtgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0336
CAAGCAGAAGACGGCATACGAGATttgagcctaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0336
AATGATACGGCGACCACCGAGATCTACACtcgaatggaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0337
CAAGCAGAAGACGGCATACGAGATccaccttacaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0337
AATGATACGGCGACCACCGAGATCTACACaaggccttggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0338
CAAGCAGAAGACGGCATACGAGATgttgcagttgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0338
AATGATACGGCGACCACCGAGATCTACACtgaacgcaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0339
CAAGCAGAAGACGGCATACGAGATtcactcatgtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0339
AATGATACGGCGACCACCGAGATCTACACccgcttagctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0340
CAAGCAGAAGACGGCATACGAGATgactggttgcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0340
AATGATACGGCGACCACCGAGATCTACACcaccgaggaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0341
CAAGCAGAAGACGGCATACGAGATatcgtcgctcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0341
AATGATACGGCGACCACCGAGATCTACACcgtataatcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0342
CAAGCAGAAGACGGCATACGAGATggtgcgttcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0342
AATGATACGGCGACCACCGAGATCTACACatgacagaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0343
CAAGCAGAAGACGGCATACGAGATcggcgtaagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0343
AATGATACGGCGACCACCGAGATCTACACattcattgcaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0344
CAAGCAGAAGACGGCATACGAGATgacatcagctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0344
AATGATACGGCGACCACCGAGATCTACACtcatgtcctgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0345
CAAGCAGAAGACGGCATACGAGATactaattcagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0345
AATGATACGGCGACCACCGAGATCTACACaattcgatcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0346
CAAGCAGAAGACGGCATACGAGATttcctccttaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0346
AATGATACGGCGACCACCGAGATCTACACttccgacattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0347
CAAGCAGAAGACGGCATACGAGATtgtgtaagctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0347
AATGATACGGCGACCACCGAGATCTACACtggcacgaccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0348
CAAGCAGAAGACGGCATACGAGATgtggctggttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0348
AATGATACGGCGACCACCGAGATCTACACgccacagcacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0349
CAAGCAGAAGACGGCATACGAGATtcgacttaagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0349
AATGATACGGCGACCACCGAGATCTACACcagtagttgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0350
CAAGCAGAAGACGGCATACGAGATcacgttaggcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0350
AATGATACGGCGACCACCGAGATCTACACagctctcaagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0351
CAAGCAGAAGACGGCATACGAGATtgaagtaagtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0351
AATGATACGGCGACCACCGAGATCTACACtctggaattaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0352
CAAGCAGAAGACGGCATACGAGATacggaatgcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0352
AATGATACGGCGACCACCGAGATCTACACattagtggagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0353
CAAGCAGAAGACGGCATACGAGATgtgtgatatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0353
AATGATACGGCGACCACCGAGATCTACACgactatatgtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0354
CAAGCAGAAGACGGCATACGAGATacacagcgctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0354
AATGATACGGCGACCACCGAGATCTACACcgttcggaacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0355
CAAGCAGAAGACGGCATACGAGATagcgcggtgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0355
AATGATACGGCGACCACCGAGATCTACACtcgatactagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0356
CAAGCAGAAGACGGCATACGAGATcaaggctatcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0356
AATGATACGGCGACCACCGAGATCTACACtaccacaatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0357
CAAGCAGAAGACGGCATACGAGATtgcgtccaggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0357
AATGATACGGCGACCACCGAGATCTACACtggtataccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0358
CAAGCAGAAGACGGCATACGAGATaggtgcgtaaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0358
AATGATACGGCGACCACCGAGATCTACACgctctcgttgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0359
CAAGCAGAAGACGGCATACGAGATgcagcaacgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0359
AATGATACGGCGACCACCGAGATCTACACgtctcgtgaaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0360
CAAGCAGAAGACGGCATACGAGATatccttgtcgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0360
AATGATACGGCGACCACCGAGATCTACACaaggccacctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0361
CAAGCAGAAGACGGCATACGAGATgaaggtacacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0361
AATGATACGGCGACCACCGAGATCTACACctgtgagctaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0362
CAAGCAGAAGACGGCATACGAGATttggccaggtGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0362
AATGATACGGCGACCACCGAGATCTACACtcacagatcgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0363
CAAGCAGAAGACGGCATACGAGATaggccagacaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0363
AATGATACGGCGACCACCGAGATCTACACagaagccaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0364
CAAGCAGAAGACGGCATACGAGATagcattaactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0364
AATGATACGGCGACCACCGAGATCTACACactgcagccgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0365
CAAGCAGAAGACGGCATACGAGATattactcaccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0365
AATGATACGGCGACCACCGAGATCTACACaacatctagtTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0366
CAAGCAGAAGACGGCATACGAGATgcgcagagtaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0366
AATGATACGGCGACCACCGAGATCTACACccttactatgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0367
CAAGCAGAAGACGGCATACGAGATcgccatacctGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0367
AATGATACGGCGACCACCGAGATCTACACgtggcgagacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0368
CAAGCAGAAGACGGCATACGAGATgcaggctggaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0368
AATGATACGGCGACCACCGAGATCTACACgccagatccaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0369
CAAGCAGAAGACGGCATACGAGATgttatatggcGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0369
AATGATACGGCGACCACCGAGATCTACACacacaatatcTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0370
CAAGCAGAAGACGGCATACGAGATcactcgcactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0370
AATGATACGGCGACCACCGAGATCTACACtggaggtaatTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0371
CAAGCAGAAGACGGCATACGAGATaccggctcagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0371
AATGATACGGCGACCACCGAGATCTACACccttcacgtaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0372
CAAGCAGAAGACGGCATACGAGATatagaccgttGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0372
AATGATACGGCGACCACCGAGATCTACACctatacgcggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0373
CAAGCAGAAGACGGCATACGAGATtgaacgcaacGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0373
AATGATACGGCGACCACCGAGATCTACACgttgcagttgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0374
CAAGCAGAAGACGGCATACGAGATgtggttgaagGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0374
AATGATACGGCGACCACCGAGATCTACACttatgcgcctTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0375
CAAGCAGAAGACGGCATACGAGATactgaatagaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0375
AATGATACGGCGACCACCGAGATCTACACtctcagtacaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0376
CAAGCAGAAGACGGCATACGAGATggacgtcttgGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0376
AATGATACGGCGACCACCGAGATCTACACagtatacggaTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0377
CAAGCAGAAGACGGCATACGAGATgttgtactcaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0377
AATGATACGGCGACCACCGAGATCTACACacgcttggacTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0378
CAAGCAGAAGACGGCATACGAGATagaaccgcggGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0378
AATGATACGGCGACCACCGAGATCTACACggagtagattTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0379
CAAGCAGAAGACGGCATACGAGATcagtatcaatGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0379
AATGATACGGCGACCACCGAGATCTACACtacacgctccTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0380
CAAGCAGAAGACGGCATACGAGATtccataatccGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0380
AATGATACGGCGACCACCGAGATCTACACtccgatagagTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0381
CAAGCAGAAGACGGCATACGAGATatgagaaccaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0381
AATGATACGGCGACCACCGAGATCTACACctcaaggccgTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0382
CAAGCAGAAGACGGCATACGAGATtcgtggttgaGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0382
AATGATACGGCGACCACCGAGATCTACACcaagttcataTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0383
CAAGCAGAAGACGGCATACGAGATcaagttcataGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0383
AATGATACGGCGACCACCGAGATCTACACaatccttaggTCGTCGGCAGCGTC
>Illumina UD IDT Index 1 (i7) Adapter UDP0384
CAAGCAGAAGACGGCATACGAGATcttaaccactGTCTCGTGGGCTCGG
>Illumina UD IDT Index 2 (i5) Adapter UDP0384
AATGATACGGCGACCACCGAGATCTACACggtggaatacTCGTCGGCAGCGTC
EOF
#                                                                                                            #
# -- AMPLISEQ ---------------------------------------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 31-33)                        #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' AMPLISEQ <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Adapter
CTGTCTCTTATACACATCT
>Index 1 (i7) Adapter Q7005
CAAGCAGAAGACGGCATACGAGATgtgaatatGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7006
CAAGCAGAAGACGGCATACGAGATacaggcgcGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7007
CAAGCAGAAGACGGCATACGAGATcatagagtGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7008
CAAGCAGAAGACGGCATACGAGATtgcgagacGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7015
CAAGCAGAAGACGGCATACGAGATtctctactGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7016
CAAGCAGAAGACGGCATACGAGATctctcgtcGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7017
CAAGCAGAAGACGGCATACGAGATccaagtctGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7018
CAAGCAGAAGACGGCATACGAGATttggactcGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7023
CAAGCAGAAGACGGCATACGAGATgcagaattGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7024
CAAGCAGAAGACGGCATACGAGATatgaggccGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7025
CAAGCAGAAGACGGCATACGAGATactaagatGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7026
CAAGCAGAAGACGGCATACGAGATgtcggagcGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7027
CAAGCAGAAGACGGCATACGAGATagcctcatGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7028
CAAGCAGAAGACGGCATACGAGATgattctgcGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7029
CAAGCAGAAGACGGCATACGAGATtcgtagtgGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7030
CAAGCAGAAGACGGCATACGAGATctacgacaGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7035
CAAGCAGAAGACGGCATACGAGATatggcatgGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7036
CAAGCAGAAGACGGCATACGAGATgcaatgcaGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7039
CAAGCAGAAGACGGCATACGAGATcttatcggGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7040
CAAGCAGAAGACGGCATACGAGATtccgctaaGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7041
CAAGCAGAAGACGGCATACGAGATgatctatcGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7042
CAAGCAGAAGACGGCATACGAGATagctcgctGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7047
CAAGCAGAAGACGGCATACGAGATacactaagGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 1 (i7) Adapter Q7048
CAAGCAGAAGACGGCATACGAGATgtgtcggaGTCTCGTGGGCTCGGAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5001
AATGATACGGCGACCACCGAGATCTACACagcgctagTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5002
AATGATACGGCGACCACCGAGATCTACACgatatcgaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5003
AATGATACGGCGACCACCGAGATCTACACcgcagacgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5004
AATGATACGGCGACCACCGAGATCTACACtatgagtaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5007
AATGATACGGCGACCACCGAGATCTACACacatagcgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5008
AATGATACGGCGACCACCGAGATCTACACgtgcgataTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5009
AATGATACGGCGACCACCGAGATCTACACccaacagaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5010
AATGATACGGCGACCACCGAGATCTACACttggtgagTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5013
AATGATACGGCGACCACCGAGATCTACACaaccgcggTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5014
AATGATACGGCGACCACCGAGATCTACACggttataaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5017
AATGATACGGCGACCACCGAGATCTACACctagcttgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5018
AATGATACGGCGACCACCGAGATCTACACtcgatccaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5025
AATGATACGGCGACCACCGAGATCTACACataccaagTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5026
AATGATACGGCGACCACCGAGATCTACACgcgttggaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5027
AATGATACGGCGACCACCGAGATCTACACcttcacggTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5028
AATGATACGGCGACCACCGAGATCTACACtcctgtaaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5029
AATGATACGGCGACCACCGAGATCTACACcctcggtaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5030
AATGATACGGCGACCACCGAGATCTACACttctaacgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5031
AATGATACGGCGACCACCGAGATCTACACcgctcgtgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5032
AATGATACGGCGACCACCGAGATCTACACtatctacaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5035
AATGATACGGCGACCACCGAGATCTACACcattgttgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5036
AATGATACGGCGACCACCGAGATCTACACtgccaccaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5039
AATGATACGGCGACCACCGAGATCTACACacgccgcaTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
>Index 2 (i5) Adapter Q5040
AATGATACGGCGACCACCGAGATCTACACgtattatgTCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
EOF
#                                                                                                            #
# -- TRUSIGHT_PANCANCER: TruSight RNA Pan-Cancer Panel ----------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 42-44)                        #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' TRUSIGHT_PANCANCER <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Universal Adapter
AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index Adapter 1
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatcacgATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 2
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgatgtATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 3
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttaggcATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 4
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgaccaATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 5
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacagtgATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 6
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgccaatATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 7
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcagatcATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 8
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacttgaATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 9
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgatcagATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 10
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtagcttATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 11
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggctacATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 12
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcttgtaATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 13
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagtcaaCAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 14
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagttccGTATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 15
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatgtcaGAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 16
GATCGGAAGAGCACACGTCTGAACTCCAGTCACccgtccCGATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 18
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtccgcACATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 19
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtgaaaCGATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 20
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtggccTTATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 21
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtttcgGAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 22
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgtacgTAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 23
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgagtggATATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 25
GATCGGAAGAGCACACGTCTGAACTCCAGTCACactgatATATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 27
GATCGGAAGAGCACACGTCTGAACTCCAGTCACattcctTTATCTCGTATGCCGTCTTCTGCTTG
EOF
#                                                                                                            #
# -- TRUSEQ_UD: TruSeq DNA/RNA UD indexes -----------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 45-50)                        #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' TRUSEQ_UD <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Adapter R1
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
>Adapter R2
AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0001
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaaccgcggATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0001
AATGATACGGCGACCACCGAGATCTACACagcgctagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0002
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggttataaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0002
AATGATACGGCGACCACCGAGATCTACACgatatcgaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0003
GATCGGAAGAGCACACGTCTGAACTCCAGTCACccaagtccATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0003
AATGATACGGCGACCACCGAGATCTACACcgcagacgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0004
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttggacttATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0004
AATGATACGGCGACCACCGAGATCTACACtatgagtaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0005
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcagtggatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0005
AATGATACGGCGACCACCGAGATCTACACaggtgcgtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0006
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgacaagcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0006
AATGATACGGCGACCACCGAGATCTACACgaacatacACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0007
GATCGGAAGAGCACACGTCTGAACTCCAGTCACctagcttgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0007
AATGATACGGCGACCACCGAGATCTACACacatagcgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0008
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtcgatccaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0008
AATGATACGGCGACCACCGAGATCTACACgtgcgataACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0009
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcctgaactATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0009
AATGATACGGCGACCACCGAGATCTACACccaacagaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0010
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttcaggtcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0010
AATGATACGGCGACCACCGAGATCTACACttggtgagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0011
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagtagagaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0011
AATGATACGGCGACCACCGAGATCTACACcgcggttcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0012
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgacgagagATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0012
AATGATACGGCGACCACCGAGATCTACACtataacctACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0013
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagacttggATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0013
AATGATACGGCGACCACCGAGATCTACACaaggatgaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0014
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgagtccaaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0014
AATGATACGGCGACCACCGAGATCTACACggaagcagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0015V2
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcagtaggcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0015V2
AATGATACGGCGACCACCGAGATCTACACtgacgaatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0016V2
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgacgaatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0016V2
AATGATACGGCGACCACCGAGATCTACACcagtaggcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0017
GATCGGAAGAGCACACGTCTGAACTCCAGTCACctgtattaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0017
AATGATACGGCGACCACCGAGATCTACACatattcacACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0018
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtcacgccgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0018
AATGATACGGCGACCACCGAGATCTACACgcgcctgtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0019
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacttacatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0019
AATGATACGGCGACCACCGAGATCTACACactctatgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0020
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtccgtgcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0020
AATGATACGGCGACCACCGAGATCTACACgtctcgcaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0021
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaaggtaccATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0021
AATGATACGGCGACCACCGAGATCTACACaagacgtcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0022
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggaacgttATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0022
AATGATACGGCGACCACCGAGATCTACACggagtactACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0023
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaattctgcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0023
AATGATACGGCGACCACCGAGATCTACACaccggccaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0024
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggcctcatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0024
AATGATACGGCGACCACCGAGATCTACACgttaattgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0025
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatcttagtATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0025
AATGATACGGCGACCACCGAGATCTACACaaccgcggACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0026
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgctccgacATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0026
AATGATACGGCGACCACCGAGATCTACACggttataaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0027
GATCGGAAGAGCACACGTCTGAACTCCAGTCACataccaagATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0027
AATGATACGGCGACCACCGAGATCTACACccaagtccACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0028
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgcgttggaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0028
AATGATACGGCGACCACCGAGATCTACACttggacttACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0029
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcttcacggATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0029
AATGATACGGCGACCACCGAGATCTACACcagtggatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0030
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtcctgtaaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0030
AATGATACGGCGACCACCGAGATCTACACtgacaagcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0031
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagaatgccATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0031
AATGATACGGCGACCACCGAGATCTACACctagcttgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0032
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgaggcattATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0032
AATGATACGGCGACCACCGAGATCTACACtcgatccaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0033
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcctcggtaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0033
AATGATACGGCGACCACCGAGATCTACACcctgaactACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0034
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttctaacgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0034
AATGATACGGCGACCACCGAGATCTACACttcaggtcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0035
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatgaggctATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0035
AATGATACGGCGACCACCGAGATCTACACagtagagaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0036
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgcagaatcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0036
AATGATACGGCGACCACCGAGATCTACACgacgagagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0037
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcactacgaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0037
AATGATACGGCGACCACCGAGATCTACACagacttggACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0038
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgtcgtagATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0038
AATGATACGGCGACCACCGAGATCTACACgagtccaaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0039
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaccacttaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0039
AATGATACGGCGACCACCGAGATCTACACcttaagccACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0040
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgttgtccgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0040
AATGATACGGCGACCACCGAGATCTACACtccggattACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0041
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatccatatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0041
AATGATACGGCGACCACCGAGATCTACACctgtattaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0042
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgcttgcgcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0042
AATGATACGGCGACCACCGAGATCTACACtcacgccgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0043
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagtatcttATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0043
AATGATACGGCGACCACCGAGATCTACACacttacatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0044
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgacgctccATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0044
AATGATACGGCGACCACCGAGATCTACACgtccgtgcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0045
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcatgccatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0045
AATGATACGGCGACCACCGAGATCTACACaaggtaccACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0046
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgcattgcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0046
AATGATACGGCGACCACCGAGATCTACACggaacgttACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0047
GATCGGAAGAGCACACGTCTGAACTCCAGTCACattggaacATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0047
AATGATACGGCGACCACCGAGATCTACACaattctgcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0048
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgccaaggtATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0048
AATGATACGGCGACCACCGAGATCTACACggcctcatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0049
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgagatatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0049
AATGATACGGCGACCACCGAGATCTACACatcttagtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0050
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtagagcgcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0050
AATGATACGGCGACCACCGAGATCTACACgctccgacACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0051
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaacctgttATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0051
AATGATACGGCGACCACCGAGATCTACACataccaagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0052
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggttcaccATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0052
AATGATACGGCGACCACCGAGATCTACACgcgttggaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0053
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcattgttgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0053
AATGATACGGCGACCACCGAGATCTACACcttcacggACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0054
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgccaccaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0054
AATGATACGGCGACCACCGAGATCTACACtcctgtaaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0055V2
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgttcgccgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0055V2
AATGATACGGCGACCACCGAGATCTACACgctcattgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0056V2
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcacgagcgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0056V2
AATGATACGGCGACCACCGAGATCTACACatctgccaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0057
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacgccgcaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0057
AATGATACGGCGACCACCGAGATCTACACcctcggtaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0058
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtattatgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0058
AATGATACGGCGACCACCGAGATCTACACttctaacgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0059
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgatagatcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0059
AATGATACGGCGACCACCGAGATCTACACatgaggctACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0060
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagcgagctATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0060
AATGATACGGCGACCACCGAGATCTACACgcagaatcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0061
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcagttccgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0061
AATGATACGGCGACCACCGAGATCTACACcactacgaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0062
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgaccttaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0062
AATGATACGGCGACCACCGAGATCTACACtgtcgtagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0063
GATCGGAAGAGCACACGTCTGAACTCCAGTCACctaggcaaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0063
AATGATACGGCGACCACCGAGATCTACACaccacttaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0064
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtcgaatggATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0064
AATGATACGGCGACCACCGAGATCTACACgttgtccgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0065
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcttagtgtATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0065
AATGATACGGCGACCACCGAGATCTACACatccatatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0066
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtccgacacATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0066
AATGATACGGCGACCACCGAGATCTACACgcttgcgcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0067
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaacaggaaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0067
AATGATACGGCGACCACCGAGATCTACACagtatcttACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0068
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggtgaaggATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0068
AATGATACGGCGACCACCGAGATCTACACgacgctccACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0069
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcctgtggcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0069
AATGATACGGCGACCACCGAGATCTACACcatgccatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0070
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttcacaatATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0070
AATGATACGGCGACCACCGAGATCTACACtgcattgcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0071
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacacgagtATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0071
AATGATACGGCGACCACCGAGATCTACACattggaacACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0072
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtgtagacATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0072
AATGATACGGCGACCACCGAGATCTACACgccaaggtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0073
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgttaattgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0073
AATGATACGGCGACCACCGAGATCTACACcgagatatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0074
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaccggccaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0074
AATGATACGGCGACCACCGAGATCTACACtagagcgcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0075
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggagtactATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0075
AATGATACGGCGACCACCGAGATCTACACaacctgttACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0076
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaagacgtcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0076
AATGATACGGCGACCACCGAGATCTACACggttcaccACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0077
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtctcgcaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0077
AATGATACGGCGACCACCGAGATCTACACcattgttgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0078
GATCGGAAGAGCACACGTCTGAACTCCAGTCACactctatgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0078
AATGATACGGCGACCACCGAGATCTACACtgccaccaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0079
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgcgcctgtATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0079
AATGATACGGCGACCACCGAGATCTACACctctgcctACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0080
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatattcacATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0080
AATGATACGGCGACCACCGAGATCTACACtctcattcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0081
GATCGGAAGAGCACACGTCTGAACTCCAGTCACctacagttATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0081
AATGATACGGCGACCACCGAGATCTACACacgccgcaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0082
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtcgtgaccATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0082
AATGATACGGCGACCACCGAGATCTACACgtattatgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0083
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggaagcagATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0083
AATGATACGGCGACCACCGAGATCTACACgatagatcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0084
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaaggatgaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0084
AATGATACGGCGACCACCGAGATCTACACagcgagctACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0085
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtataacctATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0085
AATGATACGGCGACCACCGAGATCTACACcagttccgACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0086
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgcggttcATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0086
AATGATACGGCGACCACCGAGATCTACACtgaccttaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0087
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttggtgagATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0087
AATGATACGGCGACCACCGAGATCTACACctaggcaaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0088
GATCGGAAGAGCACACGTCTGAACTCCAGTCACccaacagaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0088
AATGATACGGCGACCACCGAGATCTACACtcgaatggACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0089
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtgcgataATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0089
AATGATACGGCGACCACCGAGATCTACACcttagtgtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0090
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacatagcgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0090
AATGATACGGCGACCACCGAGATCTACACtccgacacACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0091
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgaacatacATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0091
AATGATACGGCGACCACCGAGATCTACACaacaggaaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0092
GATCGGAAGAGCACACGTCTGAACTCCAGTCACaggtgcgtATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0092
AATGATACGGCGACCACCGAGATCTACACggtgaaggACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0093
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtatgagtaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0093
AATGATACGGCGACCACCGAGATCTACACcctgtggcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0094
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgcagacgATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0094
AATGATACGGCGACCACCGAGATCTACACttcacaatACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0095
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgatatcgaATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0095
AATGATACGGCGACCACCGAGATCTACACacacgagtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>TruSeq UD IDT Index 1 (i7) Adapter UDI0096
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagcgctagATCTCGTATGCCGTCTTCTGCTTG
>TruSeq UD IDT Index 2 (i5) Adapter UDI0096
AATGATACGGCGACCACCGAGATCTACACgtgtagacACACTCTTTCCCTACACGACGCTCTTCCGATCT
EOF
#                                                                                                            #
# -- TRUSEQ_CD: TruSeq DNA/RNA CD Indexes -----------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 50-51)                        #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' TRUSEQ_CD <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Adapter R1
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
>Adapter R2
AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
>Index 1 (i7) Adapter D701
GATCGGAAGAGCACACGTCTGAACTCCAGTCACattactcgATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D702
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtccggagaATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D703
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgctcattATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D704
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgagattccATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D705
GATCGGAAGAGCACACGTCTGAACTCCAGTCACattcagaaATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D706
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgaattcgtATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D707
GATCGGAAGAGCACACGTCTGAACTCCAGTCACctgaagctATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D708
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtaatgcgcATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D709
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcggctatgATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D710
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtccgcgaaATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D711
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtctcgcgcATCTCGTATGCCGTCTTCTGCTTG
>Index 1 (i7) Adapter D712
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagcgatagATCTCGTATGCCGTCTTCTGCTTG
>Index 2 (i5) Adapter D501
AATGATACGGCGACCACCGAGATCTACACtatagcctACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D502
AATGATACGGCGACCACCGAGATCTACACatagaggcACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D503
AATGATACGGCGACCACCGAGATCTACACcctatcctACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D504
AATGATACGGCGACCACCGAGATCTACACggctctgaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D505
AATGATACGGCGACCACCGAGATCTACACaggcgaagACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D506
AATGATACGGCGACCACCGAGATCTACACtaatcttaACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D507
AATGATACGGCGACCACCGAGATCTACACcaggacgtACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Index 2 (i5) Adapter D508
AATGATACGGCGACCACCGAGATCTACACgtactgacACACTCTTTCCCTACACGACGCTCTTCCGATCT
EOF
#                                                                                                            #
# -- TRUSEQ_SINGLE: TruSeq Single Indexes -----------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v14; pp. 51-53)                        #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' TRUSEQ_SINGLE <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>Universal Adapter
AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Adapter R1
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
>Adapter R2
AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
>Index Adapter 1
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatcacgATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 2
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgatgtATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 3
GATCGGAAGAGCACACGTCTGAACTCCAGTCACttaggcATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 4
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtgaccaATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 5
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacagtgATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 6
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgccaatATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 7
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcagatcATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 8
GATCGGAAGAGCACACGTCTGAACTCCAGTCACacttgaATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 9
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgatcagATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 10
GATCGGAAGAGCACACGTCTGAACTCCAGTCACtagcttATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 11
GATCGGAAGAGCACACGTCTGAACTCCAGTCACggctacATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 12
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcttgtaATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 13
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagtcaaCAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 14
GATCGGAAGAGCACACGTCTGAACTCCAGTCACagttccGTATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 15
GATCGGAAGAGCACACGTCTGAACTCCAGTCACatgtcaGAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 16
GATCGGAAGAGCACACGTCTGAACTCCAGTCACccgtccCGATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 17
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtccgcACATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 18
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtgaaaCGATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 19
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtggccTTATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 20
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgtttcgGAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 21
GATCGGAAGAGCACACGTCTGAACTCCAGTCACcgtacgTAATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 22
GATCGGAAGAGCACACGTCTGAACTCCAGTCACgagtggATATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 23
GATCGGAAGAGCACACGTCTGAACTCCAGTCACactgatATATCTCGTATGCCGTCTTCTGCTTG
>Index Adapter 24
GATCGGAAGAGCACACGTCTGAACTCCAGTCACattcctTTATCTCGTATGCCGTCTTCTGCTTG
EOF
#                                                                                                            #
# -- TRUSEQ_SMALLRNA: TruSeq Small RNA --------------------------------------------------------------------  #
#  derived from: Illumina Adapter Sequences (Document # 1000000002694 v16; pp. 54-58)                        #
#  > Oligonucleotide sequences © 2021 Illumina, Inc. All rights reserved. Derivative works                   #
#    created by Illumina  customers are authorized  for use with  Illumina instruments and                   #
#    products only. All other uses are strictly prohibited.                                                  #
#                                                                                                            #
read -r -d '' TRUSEQ_SMALLRNA <<-'EOF'
>poly-A	  
AAAAAAAAAAAAAAAAAA
>poly-C
CCCCCCCCCCCCCCCCCC
>RNA Adapter RA5
GTTCAGAGTTCTACAGTCCGACGATC
>RNA Adapter RA3
TGGAATTCTCGGGTGCCAAGG
>Stop Oligo
GAATTCCACCACGTTCCCGTGG
>RNA RT Primer
GCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer RP1
AATGATACGGCGACCACCGAGATCTACACGTTCAGAGTTCTACAGTCCGA
>RNA PCR Primer Index RPI1
CAAGCAGAAGACGGCATACGAGATcgtgatGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI2
CAAGCAGAAGACGGCATACGAGATacatcgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI3
CAAGCAGAAGACGGCATACGAGATgcctaaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI4
CAAGCAGAAGACGGCATACGAGATtggtcaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI5
CAAGCAGAAGACGGCATACGAGATcactgtGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI6
CAAGCAGAAGACGGCATACGAGATattggcGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI7
CAAGCAGAAGACGGCATACGAGATgatctgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI8
CAAGCAGAAGACGGCATACGAGATtcaagtGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI9
CAAGCAGAAGACGGCATACGAGATctgatcGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI10
CAAGCAGAAGACGGCATACGAGATaagctaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI11
CAAGCAGAAGACGGCATACGAGATgtagccGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI12
CAAGCAGAAGACGGCATACGAGATtacaagGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI13
CAAGCAGAAGACGGCATACGAGATttgactGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI14
CAAGCAGAAGACGGCATACGAGATggaactGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI15
CAAGCAGAAGACGGCATACGAGATtgacatGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI16
CAAGCAGAAGACGGCATACGAGATggacggGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI17
CAAGCAGAAGACGGCATACGAGATctctacGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI18
CAAGCAGAAGACGGCATACGAGATgcggacGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI19
CAAGCAGAAGACGGCATACGAGATtttcacGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI20
CAAGCAGAAGACGGCATACGAGATggccacGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI21
CAAGCAGAAGACGGCATACGAGATcgaaacGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI22
CAAGCAGAAGACGGCATACGAGATcgtacgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI23
CAAGCAGAAGACGGCATACGAGATccactcGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI24
CAAGCAGAAGACGGCATACGAGATgctaccGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI25
CAAGCAGAAGACGGCATACGAGATatcagtGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI26
CAAGCAGAAGACGGCATACGAGATgctcatGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI27
CAAGCAGAAGACGGCATACGAGATaggaatGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI28
CAAGCAGAAGACGGCATACGAGATcttttgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI29
CAAGCAGAAGACGGCATACGAGATtagttgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI30
CAAGCAGAAGACGGCATACGAGATccggtgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI31
CAAGCAGAAGACGGCATACGAGATatcgtgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI32
CAAGCAGAAGACGGCATACGAGATtgagtgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI33
CAAGCAGAAGACGGCATACGAGATcgcctgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI34
CAAGCAGAAGACGGCATACGAGATgccatgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI35
CAAGCAGAAGACGGCATACGAGATaaaatgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI36
CAAGCAGAAGACGGCATACGAGATtgttggGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI37
CAAGCAGAAGACGGCATACGAGATattccgGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI38
CAAGCAGAAGACGGCATACGAGATagctagGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI39
CAAGCAGAAGACGGCATACGAGATgtatagGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI40
CAAGCAGAAGACGGCATACGAGATtctgagGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI41
CAAGCAGAAGACGGCATACGAGATgtcgtcGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI42
CAAGCAGAAGACGGCATACGAGATcgattaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI43
CAAGCAGAAGACGGCATACGAGATgctgtaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI44
CAAGCAGAAGACGGCATACGAGATattataGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI45
CAAGCAGAAGACGGCATACGAGATgaatgaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI46
CAAGCAGAAGACGGCATACGAGATtcgggaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI47
CAAGCAGAAGACGGCATACGAGATcttcgaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
>RNA PCR Primer Index RPI48
CAAGCAGAAGACGGCATACGAGATtgccgaGTGACTGGAGTTCCTTGGCACCCGAGAATTCCA
EOF
#                                                                                                            #
# -- Technical oligo list ---------------------------------------------------------------------------------  #
#                                                                                                            #
OLIGO_NAME=("POLY"  "NEXTERA"  "IUDI"  "AMPLISEQ"  "TRUSIGHT_PANCANCER"  "TRUSEQ_UD"  "TRUSEQ_CD"  "TRUSEQ_SINGLE"  "TRUSEQ_SMALLRNA");
OLIGO_SEQS=("$POLY" "$NEXTERA" "$IUDI" "$AMPLISEQ" "$TRUSIGHT_PANCANCER" "$TRUSEQ_UD" "$TRUSEQ_CD" "$TRUSEQ_SINGLE" "$TRUSEQ_SMALLRNA");
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ====================                                                                                       #
# = DNA SEQUENCES    =                                                                                       #
# ====================                                                                                       #
#                                                                                                            #
# -- phiX174 ----------------------------------------------------------------------------------------------  #
#   RefSeq accession: NC_001422.1                                                                            #
#                                                                                                            #
read -r -d '' PHIX174 <<-'EOF'
>phiX174
GAGTTTTATCGCTTCCATGACGCAGAAGTTAACACTTTCGGATATTTCTGATGAGTCGAAAAATTATCTTGATAAAGCAGGAATTACTACTGCTTGTTTACGAATTA
AATCGAAGTGGACTGCTGGCGGAAAATGAGAAAATTCGACCTATCCTTGCGCAGCTCGAGAAGCTCTTACTTTGCGACCTTTCGCCATCAACTAACGATTCTGTCAA
AAACTGACGCGTTGGATGAGGAGAAGTGGCTTAATATGCTTGGCACGTTCGTCAAGGACTGGTTTAGATATGAGTCACATTTTGTTCATGGTAGAGATTCTCTTGTT
GACATTTTAAAAGAGCGTGGATTACTATCTGAGTCCGATGCTGTTCAACCACTAATAGGTAAGAAATCATGAGTCAAGTTACTGAACAATCCGTACGTTTCCAGACC
GCTTTGGCCTCTATTAAGCTCATTCAGGCTTCTGCCGTTTTGGATTTAACCGAAGATGATTTCGATTTTCTGACGAGTAACAAAGTTTGGATTGCTACTGACCGCTC
TCGTGCTCGTCGCTGCGTTGAGGCTTGCGTTTATGGTACGCTGGACTTTGTGGGATACCCTCGCTTTCCTGCTCCTGTTGAGTTTATTGCTGCCGTCATTGCTTATT
ATGTTCATCCCGTCAACATTCAAACGGCCTGTCTCATCATGGAAGGCGCTGAATTTACGGAAAACATTATTAATGGCGTCGAGCGTCCGGTTAAAGCCGCTGAATTG
TTCGCGTTTACCTTGCGTGTACGCGCAGGAAACACTGACGTTCTTACTGACGCAGAAGAAAACGTGCGTCAAAAATTACGTGCGGAAGGAGTGATGTAATGTCTAAA
GGTAAAAAACGTTCTGGCGCTCGCCCTGGTCGTCCGCAGCCGTTGCGAGGTACTAAAGGCAAGCGTAAAGGCGCTCGTCTTTGGTATGTAGGTGGTCAACAATTTTA
ATTGCAGGGGCTTCGGCCCCTTACTTGAGGATAAATTATGTCTAATATTCAAACTGGCGCCGAGCGTATGCCGCATGACCTTTCCCATCTTGGCTTCCTTGCTGGTC
AGATTGGTCGTCTTATTACCATTTCAACTACTCCGGTTATCGCTGGCGACTCCTTCGAGATGGACGCCGTTGGCGCTCTCCGTCTTTCTCCATTGCGTCGTGGCCTT
GCTATTGACTCTACTGTAGACATTTTTACTTTTTATGTCCCTCATCGTCACGTTTATGGTGAACAGTGGATTAAGTTCATGAAGGATGGTGTTAATGCCACTCCTCT
CCCGACTGTTAACACTACTGGTTATATTGACCATGCCGCTTTTCTTGGCACGATTAACCCTGATACCAATAAAATCCCTAAGCATTTGTTTCAGGGTTATTTGAATA
TCTATAACAACTATTTTAAAGCGCCGTGGATGCCTGACCGTACCGAGGCTAACCCTAATGAGCTTAATCAAGATGATGCTCGTTATGGTTTCCGTTGCTGCCATCTC
AAAAACATTTGGACTGCTCCGCTTCCTCCTGAGACTGAGCTTTCTCGCCAAATGACGACTTCTACCACATCTATTGACATTATGGGTCTGCAAGCTGCTTATGCTAA
TTTGCATACTGACCAAGAACGTGATTACTTCATGCAGCGTTACCATGATGTTATTTCTTCATTTGGAGGTAAAACCTCTTATGACGCTGACAACCGTCCTTTACTTG
TCATGCGCTCTAATCTCTGGGCATCTGGCTATGATGTTGATGGAACTGACCAAACGTCGTTAGGCCAGTTTTCTGGTCGTGTTCAACAGACCTATAAACATTCTGTG
CCGCGTTTCTTTGTTCCTGAGCATGGCACTATGTTTACTCTTGCGCTTGTTCGTTTTCCGCCTACTGCGACTAAAGAGATTCAGTACCTTAACGCTAAAGGTGCTTT
GACTTATACCGATATTGCTGGCGACCCTGTTTTGTATGGCAACTTGCCGCCGCGTGAAATTTCTATGAAGGATGTTTTCCGTTCTGGTGATTCGTCTAAGAAGTTTA
AGATTGCTGAGGGTCAGTGGTATCGTTATGCGCCTTCGTATGTTTCTCCTGCTTATCACCTTCTTGAAGGCTTCCCATTCATTCAGGAACCGCCTTCTGGTGATTTG
CAAGAACGCGTACTTATTCGCCACCATGATTATGACCAGTGTTTCCAGTCCGTTCAGTTGTTGCAGTGGAATAGTCAGGTTAAATTTAATGTGACCGTTTATCGCAA
TCTGCCGACCACTCGCGATTCAATCATGACTTCGTGATAAAAGATTGAGTGTGAGGTTATAACGCCGAAGCGGTAAAAATTTTAATTTTTGCCGCTGAGGGGTTGAC
CAAGCGAAGCGCGGTAGGTTTTCTGCTTAGGAGTTTAATCATGTTTCAGACTTTTATTTCTCGCCATAATTCAAACTTTTTTTCTGATAAGCTGGTTCTCACTTCTG
TTACTCCAGCTTCTTCGGCACCTGTTTTACAGACACCTAAAGCTACATCGTCAACGTTATATTTTGATAGTTTGACGGTTAATGCTGGTAATGGTGGTTTTCTTCAT
TGCATTCAGATGGATACATCTGTCAACGCCGCTAATCAGGTTGTTTCTGTTGGTGCTGATATTGCTTTTGATGCCGACCCTAAATTTTTTGCCTGTTTGGTTCGCTT
TGAGTCTTCTTCGGTTCCGACTACCCTCCCGACTGCCTATGATGTTTATCCTTTGAATGGTCGCCATGATGGTGGTTATTATACCGTCAAGGACTGTGTGACTATTG
ACGTCCTTCCCCGTACGCCGGGCAATAACGTTTATGTTGGTTTCATGGTTTGGTCTAACTTTACCGCTACTAAATGCCGCGGATTGGTTTCGCTGAATCAGGTTATT
AAAGAGATTATTTGTCTCCAGCCACTTAAGTGAGGTGATTTATGTTTGGTGCTATTGCTGGCGGTATTGCTTCTGCTCTTGCTGGTGGCGCCATGTCTAAATTGTTT
GGAGGCGGTCAAAAAGCCGCCTCCGGTGGCATTCAAGGTGATGTGCTTGCTACCGATAACAATACTGTAGGCATGGGTGATGCTGGTATTAAATCTGCCATTCAAGG
CTCTAATGTTCCTAACCCTGATGAGGCCGCCCCTAGTTTTGTTTCTGGTGCTATGGCTAAAGCTGGTAAAGGACTTCTTGAAGGTACGTTGCAGGCTGGCACTTCTG
CCGTTTCTGATAAGTTGCTTGATTTGGTTGGACTTGGTGGCAAGTCTGCCGCTGATAAAGGAAAGGATACTCGTGATTATCTTGCTGCTGCATTTCCTGAGCTTAAT
GCTTGGGAGCGTGCTGGTGCTGATGCTTCCTCTGCTGGTATGGTTGACGCCGGATTTGAGAATCAAAAAGAGCTTACTAAAATGCAACTGGACAATCAGAAAGAGAT
TGCCGAGATGCAAAATGAGACTCAAAAAGAGATTGCTGGCATTCAGTCGGCGACTTCACGCCAGAATACGAAAGACCAGGTATATGCACAAAATGAGATGCTTGCTT
ATCAACAGAAGGAGTCTACTGCTCGCGTTGCGTCTATTATGGAAAACACCAATCTTTCCAAGCAACAGCAGGTTTCCGAGATTATGCGCCAAATGCTTACTCAAGCT
CAAACGGCTGGTCAGTATTTTACCAATGACCAAATCAAAGAAATGACTCGCAAGGTTAGTGCTGAGGTTGACTTAGTTCATCAGCAAACGCAGAATCAGCGGTATGG
CTCTTCTCATATTGGCGCTACTGCAAAGGATATTTCTAATGTCGTCACTGATGCTGCTTCTGGTGTGGTTGATATTTTTCATGGTATTGATAAAGCTGTTGCCGATA
CTTGGAACAATTTCTGGAAAGACGGTAAAGCTGATGGTATTGGCTCTAATTTGTCTAGGAAATAACCGTCAGGATTGACACCCTCCCAATTGTATGTTTTCATGCCT
CCAAATCTTGGAGGCTTTTTTATGGTTCGTTCTTATTACCCTTCTGAATGTCACGCTGATTATTTTGACTTTGAGCGTATCGAGGCTCTTAAACCTGCTATTGAGGC
TTGTGGCATTTCTACTCTTTCTCAATCCCCAATGCTTGGCTTCCATAAGCAGATGGATAACCGCATCAAGCTCTTGGAAGAGATTCTGTCTTTTCGTATGCAGGGCG
TTGAGTTCGATAATGGTGATATGTATGTTGACGGCCATAAGGCTGCTTCTGACGTTCGTGATGAGTTTGTATCTGTTACTGAGAAGTTAATGGATGAATTGGCACAA
TGCTACAATGTGCTCCCCCAACTTGATATTAATAACACTATAGACCACCGCCCCGAAGGGGACGAAAAATGGTTTTTAGAGAACGAGAAGACGGTTACGCAGTTTTG
CCGCAAGCTGGCTGCTGAACGCCCTCTTAAGGATATTCGCGATGAGTATAATTACCCCAAAAAGAAAGGTATTAAGGATGAGTGTTCAAGATTGCTGGAGGCCTCCA
CTATGAAATCGCGTAGAGGCTTTGCTATTCAGCGTTTGATGAATGCAATGCGACAGGCTCATGCTGATGGTTGGTTTATCGTTTTTGACACTCTCACGTTGGCTGAC
GACCGATTAGAGGCGTTTTATGATAATCCCAATGCTTTGCGTGACTATTTTCGTGATATTGGTCGTATGGTTCTTGCTGCCGAGGGTCGCAAGGCTAATGATTCACA
CGCCGACTGCTATCAGTATTTTTGTGTGCCTGAGTATGGTACAGCTAATGGCCGTCTTCATTTCCATGCGGTGCACTTTATGCGGACACTTCCTACAGGTAGCGTTG
ACCCTAATTTTGGTCGTCGGGTACGCAATCGCCGCCAGTTAAATAGCTTGCAAAATACGTGGCCTTATGGTTACAGTATGCCCATCGCAGTTCGCTACACGCAGGAC
GCTTTTTCACGTTCTGGTTGGTTGTGGCCTGTTGATGCTAAAGGTGAGCCGCTTAAAGCTACCAGTTATATGGCTGTTGGTTTCTATGTGGCTAAATACGTTAACAA
AAAGTCAGATATGGACCTTGCTGCTAAAGGTCTAGGAGCTAAAGAATGGAACAACTCACTAAAAACCAAGCTGTCGCTACTTCCCAAGAAGCTGTTCAGAATCAGAA
TGAGCCGCAACTTCGGGATGAAAATGCTCACAATGACAAATCTGTCCACGGAGTGCTTAATCCAACTTACCAAGCTGGGTTACGACGCGACGCCGTTCAACCAGATA
TTGAAGCAGAACGCAAAAAGAGAGATGAGATTGAGGCTGGGAAAAGTTACTGTAGCCGACGTTTTGGCGGCGCAACCTGTGACGACAAATCTGCTCAAATTTATGCG
CGCTTCGATAAAAATGATTGGCGTATCCAACCTGCA
EOF
#                                                                                                            #
#                                                                                                            #
##############################################################################################################




##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m fqCleanER v$VERSION                                    $COPYRIGHT\033[0m";
  cat <<EOF

 https://gitlab.pasteur.fr/GIPhy/fqCleanER

 USAGE:  fqCleanER.sh  [options] 

 OPTIONS:
  -1 <infile>   fwd (R1) FASTQ input file name from PE library 1         | input files can be |
  -2 <infile>   rev (R2) FASTQ input file name from PE library 1         | uncompressed (file |
  -3 <infile>   fwd (R1) FASTQ input file name from PE library 2         | extensions  .fastq |
  -4 <infile>   rev (R2) FASTQ input file name from PE library 2         | or    .fq),     or |
  -5 <infile>   fwd (R1) FASTQ input file name from PE library 3         | compressed   using |
  -6 <infile>   rev (R2) FASTQ input file name from PE library 3         | either gzip (.gz), |
  -7 <infile>   FASTQ input file name from SE library 4                  | bzip2   (.bz2   or |
  -8 <infile>   FASTQ input file name from SE library 5                  | .bz),   or    DSRC |
  -9 <infile>   FASTQ input file name from SE library 6                  | (.dsrc  or .dsrc2) |
  -o <outdir>   path and name of the output directory (mandatory option)
  -b <string>   base name for output files (mandatory option)
  -a <infile>   to set a file containing every alien oligonucleotide sequence (one per line) to
                be clipped during step 'T' (see below)
  -a <string>   one or several key words  (separated with commas),  each corresponding to a set 
                of alien oligonucleotide sequences to be clipped during step 'T' (see below):
                   POLY                nucleotide homopolymers
                   NEXTERA             Illumina Nextera index Kits
                   IUDI                Illumina Unique Dual index Kits
                   AMPLISEQ            AmpliSeq for Illumina Panels
                   TRUSIGHT_PANCANCER  Illumina TruSight RNA Pan-Cancer Kits
                   TRUSEQ_UD           Illumina TruSeq Unique Dual index Kits
                   TRUSEQ_CD           Illumina TruSeq Combinatorial Dual index Kits
                   TRUSEQ_SINGLE       Illumina TruSeq Single index Kits
                   TRUSEQ_SMALLRNA     Illumina TruSeq Small RNA Kits
                Note that  these sets  of alien  sequences are  not  exhaustive  and will never
                replace the exact oligos used for library preparation  (default: "POLY")
  -a AUTO       to perform  de novo  inference of  3' alien  oligonucleotide sequence(s)  of at 
                least 20 nucleotide length;  selected sequences  are completed  with those from 
                "POLY" (see above)                
  -A <infile>   to set sequence or k-mer  model file(s)  to carry out  contaminant read removal 
                during step 'C';  several comma-separated file names can be specified;  allowed 
                file extensions: .fa, .fasta, .fna, .kmr or .kmz (default: phiX174 genome)
  -d <string>   displays the alien oligonucleotide sequences corresponding to the specified key
                word(s); see option -a for the list of available key words
  -q <int>      quality score threshold;  all bases with Phred  score below  this threshold are 
                considered as non-confident (default: 15)
  -l <int>      minimum required length for a read (default: half the average read length)
  -p <int>      maximum allowed percentage  of non-confident bases  (as ruled by option -q) per 
                read (default: 50) 
  -c <int>      minimum allowed coverage depth for step 'L' or 'N' (default: 4)
  -C <int>      maximum allowed coverage depth for step 'R' or 'N' (default: 90)
  -s <string>   a sequence of tasks  to be iteratively performed,  each being defined by one of 
                the following uppercase characters:
                   C   discarding [C]ontaminating reads (as ruled by option -A)
                   E   correcting sequencing [E]rrors
                   D   [D]eduplicating reads
                   L   discarding [L]ow-coverage reads (as ruled by option -c)
                   N   digital [N]ormalization (i.e. same as consecutive steps "RL")
                   M   [M]erging overlapping PE reads
                   R   [R]educing redundancy (as ruled by option -C)
                   T   [T]rimming and clipping (as ruled by options -q, -l, -p, -a)
                (default: "T")
  -z <string>   compressed output  file(s) using  gzip ("gz"),  bzip2 ("bz2")  or DSRC ("dsrc")
                (default: not compressed)
  -t <int>      number of threads (default: 12)
  -w <dir>      tmp directory (default: \$TMPDIR, otherwise /tmp)
  -h            prints this help and exit

 EXAMPLES:
   fqCleanER.sh  -4 se.fq -o out -b se.flt
   fqCleanER.sh  -1 r1.fq -2 r2.fq -o out -b pe.flt
   fqCleanER.sh  -1 r1.fq -2 r2.fq -a NEXTERA -q 20 -s DTENM -o out -b flt -z gz
   
EOF
} 
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- fb ---------------------------------------------------------------------------------------------------  #
# >> returns the specified byte size $1 in rounded format                                                    #
#                                                                                                            #
fb() {
  if   [ $1 -gt 1073741824 ]; then echo "$(bc -l <<<"scale=1;$1/1073741824" | sed 's/^\./0\./') Gb" ;
  elif [ $1 -gt 1048576 ];    then echo "$(bc -l <<<"scale=1;$1/1048576"    | sed 's/^\./0\./') Mb" ;
  elif [ $1 -gt 1024 ];       then echo "$(bc -l <<<"scale=1;$1/1024"       | sed 's/^\./0\./') kb" ;
  else                             echo "$1 b" ; fi
}
#                                                                                                            #
# -- disp -------------------------------------------------------------------------------------------------  #
# >> displays info for the specified file $1                                                                 #
#                                                                                                            #
disp() {
  echo -e "[$(fb $(stat -c %s $1))]\t$1" ;
}  
#                                                                                                            #
# -- randstring -------------------------------------------------------------------------------------------  #
# >> returns a random string of specified length $1 digits                                                   #
#                                                                                                            #
randstring() {
  if [ -e /dev/urandom ]
  then local rstr=$(tr -dc A-Za-z0-9 </dev/urandom | head -c $1);
  else local rstr=$(head -c $1 <<<f"$RANDOM"q"$RANDOM"C"$RANDOM"l"$RANDOM"N"$RANDOM"r);
  fi
  echo "$rstr" ;
}
#                                                                                                            #
# -- randfile ---------------------------------------------------------------------------------------------  #
# >> creates and returns a random file name that does not exist from the specified basename $1               #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
# -- randfilesfx ------------------------------------------------------------------------------------------  #
# >> creates random files from specified basename $1 and specified extensions in $2                          #
#    next returns the basename of the created files                                                          #
# >> example:               randfileext /tmp/foo fastq,fq,1.fq                                               #
#    + this will perform:   touch /tmp/foo.$r.fastq ; touch /tmp/foo.$r.fq ; touch /tmp/foo.$r.1.fq ;        #
#    + and next returns:    /tmp/foo.$r  where $r is a random string                                         # 
#                                                                                                            #
randfilesfx() {
  local sfx=( $(tr ',' ' ' <<<"$2") );
  local nsfx=${#sfx[@]}; let nsfx--;
  local ok=false;
  while ! $ok
  do
    local ok=true;
    local rstr=$(randstring 9);
    for i in $(seq 0 $nsfx)
    do
      if [ ! -e $1.$rstr.${sfx[$i]} ]
      then
	touch $1.$rstr.${sfx[$i]} ;
      else
	for j in $(seq 0 $(( $i - 1 )))
	do
	  rm -f $1.$rstr.${sfx[$j]} ;
	done
	local ok=false;
	break ;
      fi
    done
  done  
  echo $1.$rstr ;
}
#                                                                                                            #
# -- get --------------------------------------------------------------------------------------------------  #
# >> returns the entry $2 in the comma-separated entry set $1                                                #
# >> returns $NA when $2 is out of bounds                                                                    #
#                                                                                                            #
get () {
  echo $($CAWK -v i=$2 -v not=$NA '(1<=i&&i<=NF){print$i;exit}{print not}' <<<"$1") ;
}
#                                                                                                            #
# -- chrono -----------------------------------------------------------------------------------------------  #
# >> returns the elapsed time                                                                                #
#                                                                                                            #
chrono() {
  local s=$SECONDS; printf "[%02d:%02d]" $(( $s / 60 )) $(( $s % 60 )) ;
}
#                                                                                                            #
# -- chext () ---------------------------------------------------------------------------------------------  #
# >> returns "valid" when the specified file $1 extension is valid; "invalid" otherwise                      #
#    note: based on the array FILE_EXTENSIONS                                                                #
#                                                                                                            #
chext() {
  local f=$1; local fe="${f##*.}"; local out="invalid";
  for e in "${FILE_EXTENSIONS[@]}"
  do
    if [ "$e" == "$fe" ]; then out="valid"; break ; fi
  done
  echo "$out" ;
}
#                                                                                                            #
# -- display_aliens ---------------------------------------------------------------------------------------  #
# >> returns the alien oligos corresponding to the names in "$1" (different names should be comma-separated) #
#                                                                                                            #
display_aliens() {
  for name in $(tr ',' ' ' <<<"$1")
  do
    ok=false;
    for i in ${!OLIGO_NAME[@]}
    do
      if [ "${OLIGO_NAME[$i]}" == "$name" ]
      then
        echo ;
	echo "## $name" ;
        echo "${OLIGO_SEQS[$i]}" ;
        ok=true;
        break ;
      fi
    done
    ! $ok && echo "incorrect name (option -d): $name" >&2 && exit 1 ; 
  done
}
#                                                                                                            #
# -- fqsize -----------------------------------------------------------------------------------------------  #
# >> returns the number of reads and bases (comma-separated) in the specified FASTQ file(s)                  #
#                                                                                                            #
fqsize() {
  echo $($FQSTATS $* | $BAWK '{print$2","$4}') ;
}
#                                                                                                            #
# -- fqkmers ----------------------------------------------------------------------------------------------  #
# >> returns 11 comma-separated estimated values from the specified FASTQ file(s):                           #
#     (i)    f1: no. canonical k-mers occuring only once                                                     #
#     (ii)   f2: no. canonical k-mers occuring twice                                                         #
#     (iii)  f3: no. canonical k-mers occuring thrice                                                        #
#     (iv)   f4: no. canonical k-mers occuring 4 times                                                       #
#     (v)    f5: no. canonical k-mers occuring 5 times                                                       #
#     (vi)   f6: no. canonical k-mers occuring 6 times                                                       #
#     (vii)  f7: no. canonical k-mers occuring 7 times                                                       #
#     (viii) f8: no. canonical k-mers occuring 8 times                                                       #
#     (ix)   f9: no. canonical k-mers occuring 9 times                                                       #
#     (x)    F0: no. distinct canonical k-mers                                                               #
#     (xi)   F1: total no. canonical k-mers                                                                  #
# >> no file pattern                                                                                         #
# + $1  (up to) 15 input file names separated by commas (use $NA for no file)                                #
# + $2  tmp directory                                                                                        #
# + $3  no. threads                                                                                          #
# + $4  k-mer length                                                                                         #
#                                                                                                            #
fqkmers() {
  local allfqs="";
  for i in {1..15}
  do
    local infile=$(get "$1"  $i);
    [ "$infile" != "$NA" ] && allfqs="$allfqs $infile";
  done
  local histf=$(randfile $2/fqkmr);
  local f0f1f=$(randfile $2/fqkmr);
  $NTCARD -k $4 -t $3 -o $histf $allfqs 2> $f0f1f ;
  local fi="$($TAWK '($2==10){exit}(NR==2){f=$3;next}(NR>2){f=f","$3;next}END{print f}' $histf)" ;
  local F0F1="$($TAWK '(NR==1){F1=$3;next}(NR==2){F0=$3;print F0","F1;exit}' $f0f1f)" ;
  rm -f $histf $f0f1f ;
  echo $fi,$F0F1 ;
}
#                                                                                                            #
# -- fqinit: copying/decompressing/UNIXing/rewriting/Phred+33ing FASTQ infiles into FASTQ outfiles --------  #
# >> 9 file pattern: 1.1,1.2,2.1,2.2,3.1,3.2,4.S,5.S,6.S                                                     #
# + $1  (up to) 9 initial input file names separated by commas (use $NA for no file)                         #
# + $2  (up to) 9 output file names separated by commas (use $NA for no file)                                #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
#                                                                                                            #
fqinit() {
  ## infiles
  local I11=$(get "$1"  1) I12=$(get "$1"  2); 
  local I21=$(get "$1"  3) I22=$(get "$1"  4); 
  local I31=$(get "$1"  5) I32=$(get "$1"  6); 
  local I4S=$(get "$1"  7) I5S=$(get "$1"  8) I6S=$(get "$1" 9);
  ## outfiles
  local O11=$(get "$2"  1) O12=$(get "$2"  2); 
  local O21=$(get "$2"  3) O22=$(get "$2"  4); 
  local O31=$(get "$2"  5) O32=$(get "$2"  6); 
  local O4S=$(get "$2"  7) O5S=$(get "$2"  8) O6S=$(get "$2" 9);
  ## tmp files
  local tf11=$(randfile $3/fin11) tf12=$(randfile $3/fin12);
  local tf21=$(randfile $3/fin21) tf22=$(randfile $3/fin22);
  local tf31=$(randfile $3/fin31) tf32=$(randfile $3/fin32);
  local tf4s=$(randfile $3/fin4s);
  local tf5s=$(randfile $3/fin5s);
  local tf6s=$(randfile $3/fin6s);

  ## PE lib 1
  if [ "$I11,$I12" != "$NA2" ]
  then
    case ${I11##*.} in                                          # [--------- simplified header ---------] [------------ rewriting "^\\?*$" Phred lines ------------]
    fastq|fq)   { tr -d '\r'     < $I11 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf11 & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I11 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf11 & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I11 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf11 & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I11 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf11 ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
    case ${I12##*.} in
    fastq|fq)   { tr -d '\r'     < $I12 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf12 & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I12 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf12 & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I12 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf12 & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I12 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf12 ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
  else
    rm -f $tf11 $tf12 ;
  fi
  ## PE lib 2
  if [ "$I21,$I22" != "$NA2" ]
  then
    case ${I21##*.} in
    fastq|fq)   { tr -d '\r'     < $I21 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf21 & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I21 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf21 & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I21 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf21 & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I21 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf21 ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
    case ${I22##*.} in
    fastq|fq)   { tr -d '\r'     < $I22 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf22 & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I22 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf22 & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I22 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf22 & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I22 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf22 ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
  else
    rm -f $tf21 $tf22 ;
  fi
  ## PE lib 3
  if [ "$I31,$I32" != "$NA2" ]
  then
    case ${I31##*.} in
    fastq|fq)   { tr -d '\r'     < $I31 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf31 & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I31 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf31 & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I31 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf31 & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I31 | paste - - - - | $TAWK '{sub(" .*","/1",$1);sub("/1/1$","/1",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf31 ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
    case ${I32##*.} in
    fastq|fq)   { tr -d '\r'     < $I32 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf32 & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I32 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf32 & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I32 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf32 & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I32 | paste - - - - | $TAWK '{sub(" .*","/2",$1);sub("/2/2$","/2",$1);gsub("\\?{37}","789:;<=>?@ABCDEFGHIHGFEDCBA@?>=<;:987",$4);print $1"\n"$2"\n+\n"$4}' > $tf32 ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
  else
    rm -f $tf31 $tf32 ;
  fi
  ## SE lib 4
  if [ "$I4S" != "$NA" ]
  then
    case ${I4S##*.} in
    fastq|fq)   { tr -d '\r'     < $I4S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf4s & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I4S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf4s & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I4S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf4s & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I4S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf4s ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
  else
    rm -f $tf4s ;
  fi
  ## SE lib 5
  if [ "$I5S" != "$NA" ]
  then
    case ${I5S##*.} in
    fastq|fq)   { tr -d '\r'     < $I5S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf5s & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I5S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf5s & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I5S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf5s & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I5S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf5s ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
  else
    rm -f $tf5s ;
  fi
  ## SE lib 6
  if [ "$I6S" != "$NA" ]
  then
    case ${I6S##*.} in
    fastq|fq)   { tr -d '\r'     < $I6S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf6s & } 2>/dev/null ;;
    gz)         { $GUNZIP       -c $I6S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf6s & } 2>/dev/null ;;
    bz|bz2)     { $BUNZIP2      -c $I6S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf6s & } 2>/dev/null ;;
    dsrc|dsrc2) { $DSRC2 d -t$4 -s $I6S | paste - - - - | $TAWK '{sub(" .*","",$1);print $1"\n"$2"\n+\n"$4}' > $tf6s ; } 2>/dev/null ;;
    esac
    while [ $(jobs -r | wc -l) -ge $4 ]; do sleep $WAITIME ; done
  else
    rm -f $tf6s ;
  fi

  wait 2>/dev/null ;

  local cmd=$(randfile $3/ficmd);
  [ -e $tf11 ] && echo "$FQCONVERT -o $O11 $tf11 &>/dev/null ;" >> $cmd ;
  [ -e $tf12 ] && echo "$FQCONVERT -o $O12 $tf12 &>/dev/null ;" >> $cmd ;
  [ -e $tf21 ] && echo "$FQCONVERT -o $O21 $tf21 &>/dev/null ;" >> $cmd ;
  [ -e $tf22 ] && echo "$FQCONVERT -o $O22 $tf22 &>/dev/null ;" >> $cmd ;
  [ -e $tf31 ] && echo "$FQCONVERT -o $O31 $tf31 &>/dev/null ;" >> $cmd ;
  [ -e $tf32 ] && echo "$FQCONVERT -o $O32 $tf32 &>/dev/null ;" >> $cmd ;
  [ -e $tf4s ] && echo "$FQCONVERT -o $O4S $tf4s &>/dev/null ;" >> $cmd ;
  [ -e $tf5s ] && echo "$FQCONVERT -o $O5S $tf5s &>/dev/null ;" >> $cmd ;
  [ -e $tf6s ] && echo "$FQCONVERT -o $O6S $tf6s &>/dev/null ;" >> $cmd ;

  $XARGS -a $cmd -P $4 -I CMD bash -c CMD ;

  rm -f $cmd $tf11 $tf12 $tf21 $tf22 $tf31 $tf32 $tf4s $tf5s $tf6s ;

  return 0 ;
}
#                                                                                                            #
# -- fq2oligo ---------------------------------------------------------------------------------------------  #
# >> writes into $2 the alien oligos inferred from the specified FASTQ infiles                               #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  one output FASTA file names                                                                          #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
#                                                                                                            #
fq2oligo() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## tmp files
  local o11=$(randfile $3/fo11) o12=$(randfile $3/fo12) o1s=$(randfile $3/fo1s) o1m=$(randfile $3/f21m);
  local o21=$(randfile $3/fo21) o22=$(randfile $3/fo22) o2s=$(randfile $3/fo2s) o2m=$(randfile $3/fo2m);
  local o31=$(randfile $3/fo31) o32=$(randfile $3/fo32) o3s=$(randfile $3/fo3s) o3m=$(randfile $3/fo3m);
  local o4s=$(randfile $3/fo4s) o5s=$(randfile $3/fo5s) o6s=$(randfile $3/fo6s);

  local cmd=$(randfile $3/focmd);
  [ "$I11" != "$NA" ] && echo "$ALIENDISCOVER -i $I11 > $o11 ;" >> $cmd || rm -f $o11 ;
  [ "$I12" != "$NA" ] && echo "$ALIENDISCOVER -i $I12 > $o12 ;" >> $cmd || rm -f $o12 ;
  [ "$I1S" != "$NA" ] && echo "$ALIENDISCOVER -i $I1S > $o1s ;" >> $cmd || rm -f $o1s ;
  [ "$I1M" != "$NA" ] && echo "$ALIENDISCOVER -i $I1M > $o1m ;" >> $cmd || rm -f $o1m ;
  [ "$I21" != "$NA" ] && echo "$ALIENDISCOVER -i $I21 > $o21 ;" >> $cmd || rm -f $o21 ;
  [ "$I22" != "$NA" ] && echo "$ALIENDISCOVER -i $I22 > $o22 ;" >> $cmd || rm -f $o22 ;
  [ "$I2S" != "$NA" ] && echo "$ALIENDISCOVER -i $I2S > $o2s ;" >> $cmd || rm -f $o2s ;
  [ "$I2M" != "$NA" ] && echo "$ALIENDISCOVER -i $I2M > $o2m ;" >> $cmd || rm -f $o2m ;
  [ "$I31" != "$NA" ] && echo "$ALIENDISCOVER -i $I31 > $o31 ;" >> $cmd || rm -f $o31 ;
  [ "$I32" != "$NA" ] && echo "$ALIENDISCOVER -i $I32 > $o32 ;" >> $cmd || rm -f $o32 ;
  [ "$I3S" != "$NA" ] && echo "$ALIENDISCOVER -i $I3S > $o3s ;" >> $cmd || rm -f $o3s ;
  [ "$I3M" != "$NA" ] && echo "$ALIENDISCOVER -i $I3M > $o3m ;" >> $cmd || rm -f $o3m ;
  [ "$I4S" != "$NA" ] && echo "$ALIENDISCOVER -i $I4S > $o4s ;" >> $cmd || rm -f $o4s ;
  [ "$I5S" != "$NA" ] && echo "$ALIENDISCOVER -i $I5S > $o5s ;" >> $cmd || rm -f $o5s ;
  [ "$I6S" != "$NA" ] && echo "$ALIENDISCOVER -i $I6S > $o6s ;" >> $cmd || rm -f $o6s ;

  $XARGS -a $cmd -P $4 -n 1 -I CMD bash -c CMD ;
  rm -f $cmd ;

  display_aliens "POLY" | grep -v -F "##" | grep -v "^$"               > $2 ;
  if [ -s $o11 ]; then $BAWK '!/^>/{print">lib1.2."(++i);print}' $o11 >> $2 ; fi ; rm -f $o11 ;
  if [ -s $o12 ]; then $BAWK '!/^>/{print">lib1.2."(++i);print}' $o12 >> $2 ; fi ; rm -f $o12 ;
  if [ -s $o1s ]; then $BAWK '!/^>/{print">lib1.S."(++i);print}' $o1s >> $2 ; fi ; rm -f $o1s ;
  if [ -s $o1m ]; then $BAWK '!/^>/{print">lib1.M."(++i);print}' $o1m >> $2 ; fi ; rm -f $o1m ;
  if [ -s $o21 ]; then $BAWK '!/^>/{print">lib2.1."(++i);print}' $o21 >> $2 ; fi ; rm -f $o21 ;
  if [ -s $o22 ]; then $BAWK '!/^>/{print">lib2.2."(++i);print}' $o22 >> $2 ; fi ; rm -f $o22 ;
  if [ -s $o2s ]; then $BAWK '!/^>/{print">lib2.S."(++i);print}' $o2s >> $2 ; fi ; rm -f $o2s ;
  if [ -s $o2m ]; then $BAWK '!/^>/{print">lib2.M."(++i);print}' $o2m >> $2 ; fi ; rm -f $o2m ;
  if [ -s $o31 ]; then $BAWK '!/^>/{print">lib3.1."(++i);print}' $o31 >> $2 ; fi ; rm -f $o31 ;
  if [ -s $o32 ]; then $BAWK '!/^>/{print">lib3.2."(++i);print}' $o32 >> $2 ; fi ; rm -f $o32 ;
  if [ -s $o3s ]; then $BAWK '!/^>/{print">lib3.S."(++i);print}' $o3s >> $2 ; fi ; rm -f $o3s ;
  if [ -s $o3m ]; then $BAWK '!/^>/{print">lib3.M."(++i);print}' $o3m >> $2 ; fi ; rm -f $o3m ;
  if [ -s $o4s ]; then $BAWK '!/^>/{print">lib4.S."(++i);print}' $o4s >> $2 ; fi ; rm -f $o4s ;
  if [ -s $o5s ]; then $BAWK '!/^>/{print">lib5.S."(++i);print}' $o5s >> $2 ; fi ; rm -f $o5s ;
  if [ -s $o6s ]; then $BAWK '!/^>/{print">lib6.S."(++i);print}' $o6s >> $2 ; fi ; rm -f $o6s ;

  return 0 ;
}
#                                                                                                            #
# -- fqtrim -----------------------------------------------------------------------------------------------  #
# >> trims and clips the specified FASTQ infiles                                                             #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# >> NOTE: when input lib.S is not NA, lib.1 and lib.2 are processed as PE, and resulting singletons are     #
#          merged with processed lib.S                                                                       #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
# + $5  alien oligo file in FASTA format for clipping                                                        #
# + $6  Phred score quality threshold for trimming (integer between 0 and 40)                                #
# + $7  maximum allowed percentage of low-quality bases per read (integer between 0 and 100)                 #
# + $8  minimum allowed read length (integer higher than 0)                                                  #
#                                                                                                            #
fqtrim() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## outfiles
  local O11=$(get "$2"  1)  O12=$(get "$2"  2)  O1S=$(get "$2"  3)  O1M=$(get "$2"  4); 
  local O21=$(get "$2"  5)  O22=$(get "$2"  6)  O2S=$(get "$2"  7)  O2M=$(get "$2"  8); 
  local O31=$(get "$2"  9)  O32=$(get "$2" 10)  O3S=$(get "$2" 11)  O3M=$(get "$2" 12); 
  local O4S=$(get "$2" 13)  O5S=$(get "$2" 14)  O6S=$(get "$2" 15);
  ## tmp files
  local b1p=$(randfilesfx $3/fqtrim.1p "1.fastq,2.fastq,S.fastq");
  local b1s=$(randfilesfx $3/fqtrim.1s "fastq");
  local b1m=$(randfilesfx $3/fqtrim.1m "fastq");
  local b2p=$(randfilesfx $3/fqtrim.2p "1.fastq,2.fastq,S.fastq");
  local b2s=$(randfilesfx $3/fqtrim.2s "fastq");
  local b2m=$(randfilesfx $3/fqtrim.2m "fastq");
  local b3p=$(randfilesfx $3/fqtrim.3p "1.fastq,2.fastq,S.fastq");
  local b3s=$(randfilesfx $3/fqtrim.3s "fastq");
  local b3m=$(randfilesfx $3/fqtrim.3m "fastq");
  local b4s=$(randfilesfx $3/fqtrim.4s "fastq");
  local b5s=$(randfilesfx $3/fqtrim.5s "fastq");
  local b6s=$(randfilesfx $3/fqtrim.6s "fastq");
  ## AlienTrimmer options
  local ca=$(grep -v "^>" $5 | tr -d "[:cntrl:]" | wc -c);
  local k=11; [ $ca -lt 5000 ] && local k=10; [ $ca -lt 500 ] && local k=9; [ $ca -lt 50 ] && local k=8;
  local opt="-a $5 -k $k -m $k -q $6 -p $7 -l $8";

  local cmd=$(randfile $3/ftcmd);
  [ "$I11,$I12" != "$NA2" ] && echo "$ALIENTRIMMER $opt -1 $I11 -2 $I12 -o $b1p &>/dev/null ;" >> $cmd || rm -f $b1p.1.fastq $b1p.2.fastq $b1p.S.fastq ;
  [ "$I1S" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I1S         -o $b1s &>/dev/null ;" >> $cmd || rm -f $b1s.fastq ;
  [ "$I1M" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I1M         -o $b1m &>/dev/null ;" >> $cmd || rm -f $b1m.fastq ;
  [ "$I21,$I22" != "$NA2" ] && echo "$ALIENTRIMMER $opt -1 $I21 -2 $I22 -o $b2p &>/dev/null ;" >> $cmd || rm -f $b2p.1.fastq $b2p.2.fastq $b2p.S.fastq ;
  [ "$I2S" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I2S         -o $b2s &>/dev/null ;" >> $cmd || rm -f $b2s.fastq ;
  [ "$I2M" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I2M         -o $b2m &>/dev/null ;" >> $cmd || rm -f $b2m.fastq ;
  [ "$I31,$I32" != "$NA2" ] && echo "$ALIENTRIMMER $opt -1 $I31 -2 $I32 -o $b3p &>/dev/null ;" >> $cmd || rm -f $b3p.1.fastq $b3p.2.fastq $b3p.S.fastq ;
  [ "$I3S" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I3S         -o $b3s &>/dev/null ;" >> $cmd || rm -f $b3s.fastq ;
  [ "$I3M" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I3M         -o $b3m &>/dev/null ;" >> $cmd || rm -f $b3m.fastq ;
  [ "$I4S" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I4S         -o $b4s &>/dev/null ;" >> $cmd || rm -f $b4s.fastq ;
  [ "$I5S" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I5S         -o $b5s &>/dev/null ;" >> $cmd || rm -f $b5s.fastq ;
  [ "$I6S" != "$NA" ]       && echo "$ALIENTRIMMER $opt -i $I6S         -o $b6s &>/dev/null ;" >> $cmd || rm -f $b6s.fastq ;
  
  $XARGS -a $cmd -P $4 -I CMD bash -c CMD ;
  rm -f $cmd ;

  if [ "$I11,$I12" != "$NA2" ]
  then
    mv $b1p.1.fastq $O11 ; mv $b1p.2.fastq $O12 ; mv $b1p.S.fastq $O1S ; 
    [ "$I1S" != "$NA" ] && { cat $b1s.fastq >> $O1S ; rm -f $b1s.fastq ; } 
    [ "$I1M" != "$NA" ] && mv $b1m.fastq $O1M ;
  fi
  if [ "$I21,$I22" != "$NA2" ]
  then
    mv $b2p.1.fastq $O21 ; mv $b2p.2.fastq $O22 ; mv $b2p.S.fastq $O2S ; 
    [ "$I2S" != "$NA" ] && { cat $b2s.fastq >> $O2S ; rm -f $b2s.fastq ; }
    [ "$I2M" != "$NA" ] && mv $b2m.fastq $O2M ;
  fi
  if [ "$I31,$I32" != "$NA2" ]
  then
    mv $b3p.1.fastq $O31 ; mv $b3p.2.fastq $O32 ; mv $b3p.S.fastq $O3S ; 
    [ "$I3S" != "$NA" ] && { cat $b3s.fastq >> $O3S ; rm -f $b3s.fastq ; }
    [ "$I3M" != "$NA" ] && mv $b3m.fastq $O3M ;
  fi
  [ "$I4S" != "$NA" ] && mv $b4s.fastq $O4S ;
  [ "$I5S" != "$NA" ] && mv $b5s.fastq $O5S ;
  [ "$I6S" != "$NA" ] && mv $b6s.fastq $O6S ;

  return 0 ;
}
#                                                                                                            #
# -- fqconta ----------------------------------------------------------------------------------------------  #
# >> discards alien contaminating reads                                                                      #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
# + $5  alien sequence/k-mer files separated by commas                                                       #
#                                                                                                            #
fqconta() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## outfiles
  local O11=$(get "$2"  1)  O12=$(get "$2"  2)  O1S=$(get "$2"  3)  O1M=$(get "$2"  4); 
  local O21=$(get "$2"  5)  O22=$(get "$2"  6)  O2S=$(get "$2"  7)  O2M=$(get "$2"  8); 
  local O31=$(get "$2"  9)  O32=$(get "$2" 10)  O3S=$(get "$2" 11)  O3M=$(get "$2" 12); 
  local O4S=$(get "$2" 13)  O5S=$(get "$2" 14)  O6S=$(get "$2" 15);
  ## tmp files
  local tf11=$I11 tf12=$I12 tf1s=$I1S tf1m=$I1M;
  local tf21=$I21 tf22=$I22 tf2s=$I2S tf2m=$I2M;
  local tf31=$I31 tf32=$I32 tf3s=$I3S tf3m=$I3M;
  local tf4s=$I4S tf5s=$I5S tf6s=$I6S;

  for afile in $(tr ',' ' ' <<<"$5")
  do
    local cmd=$(randfile $3/fccmd);
    if [ "$I11,$I12" != "$NA2" ]; then local b1p=$(randfilesfx $3/fqconta.1p "1.fastq,2.fastq"); echo "$ALIENREMOVER -a $afile -1 $tf11 -2 $tf12 -o $b1p &>/dev/null ;" >> $cmd ; fi
    if [ "$I1S" != "$NA" ];       then local b1s=$(randfilesfx $3/fqconta.1s "fastq");           echo "$ALIENREMOVER -a $afile -i $tf1s          -o $b1s &>/dev/null ;" >> $cmd ; fi
    if [ "$I1M" != "$NA" ];       then local b1m=$(randfilesfx $3/fqconta.1m "fastq");           echo "$ALIENREMOVER -a $afile -i $tf1m          -o $b1m &>/dev/null ;" >> $cmd ; fi
    if [ "$I21,$I22" != "$NA2" ]; then local b2p=$(randfilesfx $3/fqconta.2p "1.fastq,2.fastq"); echo "$ALIENREMOVER -a $afile -1 $tf21 -2 $tf22 -o $b2p &>/dev/null ;" >> $cmd ; fi
    if [ "$I2S" != "$NA" ];       then local b2s=$(randfilesfx $3/fqconta.2s "fastq");           echo "$ALIENREMOVER -a $afile -i $tf2s          -o $b2s &>/dev/null ;" >> $cmd ; fi
    if [ "$I2M" != "$NA" ];       then local b2m=$(randfilesfx $3/fqconta.2m "fastq");           echo "$ALIENREMOVER -a $afile -i $tf2m          -o $b2m &>/dev/null ;" >> $cmd ; fi
    if [ "$I31,$I32" != "$NA2" ]; then local b3p=$(randfilesfx $3/fqconta.3p "1.fastq,2.fastq"); echo "$ALIENREMOVER -a $afile -1 $tf31 -2 $tf32 -o $b3p &>/dev/null ;" >> $cmd ; fi
    if [ "$I3S" != "$NA" ];       then local b3s=$(randfilesfx $3/fqconta.3s "fastq");           echo "$ALIENREMOVER -a $afile -i $tf3s          -o $b3s &>/dev/null ;" >> $cmd ; fi
    if [ "$I3M" != "$NA" ];       then local b3m=$(randfilesfx $3/fqconta.3m "fastq");           echo "$ALIENREMOVER -a $afile -i $tf3m          -o $b3m &>/dev/null ;" >> $cmd ; fi
    if [ "$I4S" != "$NA" ];       then local b4s=$(randfilesfx $3/fqconta.4s "fastq");           echo "$ALIENREMOVER -a $afile -i $tf4s          -o $b4s &>/dev/null ;" >> $cmd ; fi
    if [ "$I5S" != "$NA" ];       then local b5s=$(randfilesfx $3/fqconta.5s "fastq");           echo "$ALIENREMOVER -a $afile -i $tf5s          -o $b5s &>/dev/null ;" >> $cmd ; fi
    if [ "$I6S" != "$NA" ];       then local b6s=$(randfilesfx $3/fqconta.6s "fastq");           echo "$ALIENREMOVER -a $afile -i $tf6s          -o $b6s &>/dev/null ;" >> $cmd ; fi
  
    $XARGS -a $cmd -P $4 -I CMD bash -c CMD ;
    rm -f $cmd ;

    if [ "$I11,$I12" != "$NA2" ]; then local tf11=$(randfile $3/fc11) tf12=$(randfile $3/fc12); mv $b1p.1.fastq $tf11 ; mv $b1p.2.fastq $tf12 ; fi
    if [ "$I1S" != "$NA" ];       then local tf1s=$(randfile $3/fc1s);                          mv $b1s.fastq   $tf1s ;                         fi
    if [ "$I1M" != "$NA" ];       then local tf1m=$(randfile $3/fc1m);                          mv $b1m.fastq   $tf1m ;                         fi
    if [ "$I21,$I22" != "$NA2" ]; then local tf21=$(randfile $3/fc21) tf22=$(randfile $3/fc22); mv $b2p.1.fastq $tf21 ; mv $b2p.2.fastq $tf22 ; fi
    if [ "$I2S" != "$NA" ];       then local tf2s=$(randfile $3/fc2s);                          mv $b2s.fastq   $tf2s ;                         fi
    if [ "$I2M" != "$NA" ];       then local tf2m=$(randfile $3/fc2m);                          mv $b2m.fastq   $tf2m ;                         fi
    if [ "$I31,$I32" != "$NA2" ]; then local tf31=$(randfile $3/fc31) tf32=$(randfile $3/fc32); mv $b3p.1.fastq $tf31 ; mv $b3p.2.fastq $tf32 ; fi
    if [ "$I3S" != "$NA" ];       then local tf3s=$(randfile $3/fc3s);                          mv $b3s.fastq   $tf3s ;                         fi
    if [ "$I3M" != "$NA" ];       then local tf3m=$(randfile $3/fc3m);                          mv $b3m.fastq   $tf3m ;                         fi
    if [ "$I4S" != "$NA" ];       then local tf4s=$(randfile $3/fc4s);                          mv $b4s.fastq   $tf4s ;                         fi
    if [ "$I5S" != "$NA" ];       then local tf5s=$(randfile $3/fc5s);                          mv $b5s.fastq   $tf5s ;                         fi
    if [ "$I6S" != "$NA" ];       then local tf6s=$(randfile $3/fc6s);                          mv $b6s.fastq   $tf6s ;                         fi
  done
  
  [ "$I11,$I12" != "$NA2" ] && { mv $tf11 $O11 ; mv $tf12 $O12 ; }
  [ "$I1S" != "$NA" ]       &&   mv $tf1s $O1S ;
  [ "$I1M" != "$NA" ]       &&   mv $tf1m $O1M ;
  [ "$I21,$I22" != "$NA2" ] && { mv $tf21 $O21 ; mv $tf22 $O22 ; }
  [ "$I2S" != "$NA" ]       &&   mv $tf2s $O2S ;
  [ "$I2M" != "$NA" ]       &&   mv $tf2m $O2M ;
  [ "$I31,$I32" != "$NA2" ] && { mv $tf31 $O31 ; mv $tf32 $O32 ; }
  [ "$I3S" != "$NA" ]       &&   mv $tf3s $O3S ;
  [ "$I3M" != "$NA" ]       &&   mv $tf3m $O3M ;
  [ "$I4S" != "$NA" ]       &&   mv $tf4s $O4S ;
  [ "$I5S" != "$NA" ]       &&   mv $tf5s $O5S ;
  [ "$I6S" != "$NA" ]       &&   mv $tf6s $O6S ;

  return 0 ;
}
#                                                                                                            #
# -- fqnorm -----------------------------------------------------------------------------------------------  #
# >> digital normalization of the specified FASTQ infiles                                                    #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
# + $5  coverage min (min covr)                                                                              #
# + $6  coverage max (max covr)                                                                              #
#                                                                                                            #
fqnorm() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## outfiles
  local O11=$(get "$2"  1)  O12=$(get "$2"  2)  O1S=$(get "$2"  3)  O1M=$(get "$2"  4); 
  local O21=$(get "$2"  5)  O22=$(get "$2"  6)  O2S=$(get "$2"  7)  O2M=$(get "$2"  8); 
  local O31=$(get "$2"  9)  O32=$(get "$2" 10)  O3S=$(get "$2" 11)  O3M=$(get "$2" 12); 
  local O4S=$(get "$2" 13)  O5S=$(get "$2" 14)  O6S=$(get "$2" 15);
  ## ROCK infile/outfile
  local inrock=$(randfile $3/fqnrmi);
  local outrock=$(randfile $3/fqnrmo);
  [ "$I11,$I12" != "$NA2" ] && { echo "$I11,$I12" >> $inrock ; echo "$O11,$O12" >> $outrock ; }
  [ "$I1S" != "$NA" ]       && { echo "$I1S"      >> $inrock ; echo "$O1S"      >> $outrock ; }
  [ "$I1M" != "$NA" ]       && { echo "$I1M"      >> $inrock ; echo "$O1M"      >> $outrock ; }
  [ "$I21,$I22" != "$NA2" ] && { echo "$I21,$I22" >> $inrock ; echo "$O21,$O22" >> $outrock ; }
  [ "$I2S" != "$NA" ]       && { echo "$I2S"      >> $inrock ; echo "$O2S"      >> $outrock ; }
  [ "$I2M" != "$NA" ]       && { echo "$I2M"      >> $inrock ; echo "$O2M"      >> $outrock ; }
  [ "$I31,$I32" != "$NA2" ] && { echo "$I31,$I32" >> $inrock ; echo "$O31,$O32" >> $outrock ; }
  [ "$I3S" != "$NA" ]       && { echo "$I3S"      >> $inrock ; echo "$O3S"      >> $outrock ; }
  [ "$I3M" != "$NA" ]       && { echo "$I3M"      >> $inrock ; echo "$O3M"      >> $outrock ; }
  [ "$I4S" != "$NA" ]       && { echo "$I4S"      >> $inrock ; echo "$O4S"      >> $outrock ; }
  [ "$I5S" != "$NA" ]       && { echo "$I5S"      >> $inrock ; echo "$O5S"      >> $outrock ; }
  [ "$I6S" != "$NA" ]       && { echo "$I6S"      >> $inrock ; echo "$O6S"      >> $outrock ; }

  local ftmp=$(randfile $3/fqnrmt);
  { fqsize $(tr ',\n' ' ' < $inrock) > $ftmp  & } 2>/dev/null
  local F0=$(get "$(fqkmers $1 $3 $4 $K_ROCK)" 10);

  wait 2>/dev/null

  ## ROCK k-mer coverage options
  # nr = no. reads; nb = no. bases; lr = avg read length
  local nr=$(get "$(cat $ftmp)" 1)  nb=$(get "$(cat $ftmp)" 2);
  local lr=$(( $nb / $nr ));
  rm $ftmp ;
  #    $5 = min expected covr;    $6 = max expected covr
  # ckmin = min expected covk; ckmax = max expected covk
  # of note, covk and covk have a simple linear relationship (Formula 2 in arxiv.org/pdf/1308.2012.pdf)
  local ckmin=$(( $5 * ( $lr - $K_ROCK + 1 ) / $lr ));
  local ckmax=$(( $6 * ( $lr - $K_ROCK + 1 ) / $lr ));
  
  $ROCK  -i $inrock  -o $outrock  -k $K_ROCK  -c $ckmin  -C $ckmax  -n $F0  &> /dev/null ;
  rm -f $inrock $outrock ;  
  
  ## PE lib 1
  if [ "$I11,$I12" != "$NA2" ]
  then
    [ -e ${I11%.*}.undefined.${I11##*.} ]                        && rm -f ${I11%.*}.undefined.${I11##*.} ; 
    [ -e ${I12%.*}.undefined.${I12##*.} ]                        && rm -f ${I12%.*}.undefined.${I12##*.} ; 
    [ "$I1S" != "$NA" ] && [ -e ${I1S%.*}.undefined.${I1S##*.} ] && rm -f ${I1S%.*}.undefined.${I1S##*.} ; 
    [ "$I1M" != "$NA" ] && [ -e ${I1M%.*}.undefined.${I1M##*.} ] && rm -f ${I1M%.*}.undefined.${I1M##*.} ; 
  fi
  ## PE lib 2
  if [ "$I21,$I22" != "$NA2" ]
  then
    [ -e ${I21%.*}.undefined.${I21##*.} ]                        && rm -f ${I21%.*}.undefined.${I21##*.} ; 
    [ -e ${I22%.*}.undefined.${I22##*.} ]                        && rm -f ${I22%.*}.undefined.${I22##*.} ; 
    [ "$I1S" != "$NA" ] && [ -e ${I2S%.*}.undefined.${I2S##*.} ] && rm -f ${I2S%.*}.undefined.${I2S##*.} ; 
    [ "$I1M" != "$NA" ] && [ -e ${I2M%.*}.undefined.${I2M##*.} ] && rm -f ${I2M%.*}.undefined.${I2M##*.} ; 
  fi
  ## PE lib 3
  if [ "$I31,$I32" != "$NA2" ]
  then
    [ -e ${I31%.*}.undefined.${I31##*.} ]                        && rm -f ${I31%.*}.undefined.${I31##*.} ; 
    [ -e ${I32%.*}.undefined.${I32##*.} ]                        && rm -f ${I32%.*}.undefined.${I32##*.} ; 
    [ "$I1S" != "$NA" ] && [ -e ${I3S%.*}.undefined.${I3S##*.} ] && rm -f ${I3S%.*}.undefined.${I3S##*.} ; 
    [ "$I1M" != "$NA" ] && [ -e ${I3M%.*}.undefined.${I3M##*.} ] && rm -f ${I3M%.*}.undefined.${I3M##*.} ; 
  fi
  ## SE lib 4
  [ "$I4S" != "$NA" ] && [ -e ${I4S%.*}.undefined.${I4S##*.} ]   && rm -f ${I4S%.*}.undefined.${I4S##*.} ; 
  ## SE lib 5
  [ "$I5S" != "$NA" ] && [ -e ${I5S%.*}.undefined.${I5S##*.} ]   && rm -f ${I5S%.*}.undefined.${I5S##*.} ; 
  ## SE lib 6
  [ "$I6S" != "$NA" ] && [ -e ${I6S%.*}.undefined.${I6S##*.} ]   && rm -f ${I6S%.*}.undefined.${I6S##*.} ; 

  return 0 ;
}
#                                                                                                            #
# -- fqcorrect --------------------------------------------------------------------------------------------  #
# >> sequencing error correction                                                                             #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
#                                                                                                            #
fqcorrect() {
  ## MUSKET options
  local allfqs="";
  for i in {1..15}
  do
    local infile=$(get "$1"  $i);
    [ "$infile" != "$NA" ] && allfqs="$allfqs $infile";
  done
  local bf=$(randfilesfx $3/fqcor "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14");
  local F0=$(get "$(fqkmers $1 $3 $4 $K_MUSKET)" 10);
  [ $F0 -lt 67108864 ] && local F0=67108864; ## NOTE: musket sometimes crashes when using too many threads on few k-mers

  $MUSKET -k $K_MUSKET $F0  -omulti $bf  -p $4  $allfqs 2>/dev/null ;
  
  local c=0;
  for i in {1..15}
  do
    local infile=$(get "$1"  $i);
    if [ "$infile" != "$NA" ]
    then
      local outfile=$(get "$2"  $i);
      mv $bf.$c $outfile ;
      let c++;
    fi
  done
  for i in {0..14}; do rm -f $bf.$i ; done

  return 0 ;
}
#                                                                                                            #
# -- fqmerge ----------------------------------------------------------------------------------------------  #
# >> merging paired read with short insert size                                                              #
# >> 9 file pattern: 1.1,1.2,1.M,2.1,2.2,2.M,3.1,3.2,3.M                                                     #
# >> NOTE: when input lib.M is not NA, lib.1 and lib.2 are processed and resulting singletons are appended   #
#          to the input lib.M                                                                                #
# + $1  (up to) 9 input file names separated by commas (use $NA for no file)                                 #
# + $2  (up to) 9 output file names separated by commas (use $NA for no file)                                #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
#                                                                                                            #
fqmerge() {
  ## infiles
  local I11=$(get "$1" 1)  I12=$(get "$1" 2)  I1M=$(get "$1" 3);
  local I21=$(get "$1" 4)  I22=$(get "$1" 5)  I2M=$(get "$1" 6);
  local I31=$(get "$1" 7)  I32=$(get "$1" 8)  I3M=$(get "$1" 9);
  ## outfiles
  local O11=$(get "$2" 1)  O12=$(get "$2" 2)  O1M=$(get "$2" 3);
  local O21=$(get "$2" 4)  O22=$(get "$2" 5)  O2M=$(get "$2" 6);
  local O31=$(get "$2" 7)  O32=$(get "$2" 8)  O3M=$(get "$2" 9);
  ## tmp files
  local b1p=$(randfilesfx $3/fqm1 "notCombined_1.fastq,notCombined_2.fastq,extendedFrags.fastq");
  local b2p=$(randfilesfx $3/fqm2 "notCombined_1.fastq,notCombined_2.fastq,extendedFrags.fastq");
  local b3p=$(randfilesfx $3/fqm3 "notCombined_1.fastq,notCombined_2.fastq,extendedFrags.fastq");

  ## PE lib 1
  if [ "$I11,$I12" != "$NA2" ]
  then
    [ "$I1M" != "$NA" ] && cp $I1M $O1M || touch $O1M ;
    local rb="$(fqsize $I11 $I12)"; local arl=$(( $(get "$rb" 2) / $(get "$rb" 1) )); ## arl = avg read lgt
    $FLASH  -M $(bc -l <<<"scale=0;($arl*$M_SCALE_FLASH+0.5)/1")  -t $4  -d $(dirname $b1p)  -o $(basename $b1p)  $I11  $I12  &>/dev/null ;
    mv $b1p.notCombined_1.fastq $O11 ;
    mv $b1p.notCombined_2.fastq $O12 ;
    sed 's/\(^@.*\)\(\/1$\)/\1/' $b1p.extendedFrags.fastq >> $O1M ;
  fi
  rm -f $b1p.notCombined_1.fastq $b1p.notCombined_2.fastq $b1p.extendedFrags.fastq ;
  ## PE lib 2
  if [ "$I21,$I22" != "$NA2" ]
  then
    [ "$I2M" != "$NA" ] && cp $I2M $O2M || touch $O2M ;
    local rb="$(fqsize $I21 $I22)"; local arl=$(( $(get "$rb" 2) / $(get "$rb" 1) )); ## rl = avg read lgt
    $FLASH  -M $(bc -l <<<"scale=0;($arl*$M_SCALE_FLASH+0.5)/1")  -t $4  -d $(dirname $b2p)  -o $(basename $b2p)  $I21  $I22  &>/dev/null ; 
    mv $b2p.notCombined_1.fastq $O21 ;
    mv $b2p.notCombined_2.fastq $O22 ;
    sed 's/\(^@.*\)\(\/1$\)/\1/' $b2p.extendedFrags.fastq >> $O2M ;
  fi
  rm -f $b2p.notCombined_1.fastq $b2p.notCombined_2.fastq $b2p.extendedFrags.fastq ;
  ## PE lib 3
  if [ "$I31,$I32" != "$NA2" ]
  then
    [ "$I3M" != "$Na" ] && cp $I3M $O3M || touch $O3M ;
    local rb="$(fqsize $I31 $I32)"; local arl=$(( $(get "$rb" 2) / $(get "$rb" 1) )); ## rl = avg read lgt
    $FLASH  -M $(bc -l <<<"scale=0;($arl*$M_SCALE_FLASH+0.5)/1")  -t $4  -d $(dirname $b3p)  -o $(basename $b3p)  $I31  $I32  &>/dev/null ; 
    mv $b3p.notCombined_1.fastq $O31 ;
    mv $b3p.notCombined_2.fastq $O32 ;
    sed 's/\(^@.*\)\(\/1$\)/\1/' $b3p.extendedFrags.fastq >> $O3M ;
  fi
  rm -f $b3p.notCombined_1.fastq $b3p.notCombined_2.fastq $b3p.extendedFrags.fastq ;

  return 0 ;
}
#                                                                                                            #
# -- fquniq -----------------------------------------------------------------------------------------------  #
# >> discards duplicated PE/SE reads                                                                         #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
# + $3  tmp directory                                                                                        #
# + $4  no. threads                                                                                          #
#                                                                                                            #
fquniq() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## outfiles
  local O11=$(get "$2"  1)  O12=$(get "$2"  2)  O1S=$(get "$2"  3)  O1M=$(get "$2"  4); 
  local O21=$(get "$2"  5)  O22=$(get "$2"  6)  O2S=$(get "$2"  7)  O2M=$(get "$2"  8); 
  local O31=$(get "$2"  9)  O32=$(get "$2" 10)  O3S=$(get "$2" 11)  O3M=$(get "$2" 12); 
  local O4S=$(get "$2" 13)  O5S=$(get "$2" 14)  O6S=$(get "$2" 15);
  ## tmp files
  local t1p=$(randfile $3/fu1p) t1s=$(randfile $3/fu1s) t1m=$(randfile $3/fu1m);
  local t2p=$(randfile $3/fu2p) t2s=$(randfile $3/fu2s) t2m=$(randfile $3/fu2m);
  local t3p=$(randfile $3/fu3p) t3s=$(randfile $3/fu3s) t3m=$(randfile $3/fu3m);
  local t4s=$(randfile $3/fu4s) t5s=$(randfile $3/fu5s) t6s=$(randfile $3/fu6s);

  local cmd=$(randfile $3/fucmd);
  [ "$I11,$I12" != "$NA2" ] && echo "$FQDUPLICATE -o $t1p $I11 $I12 &>/dev/null ;" >> $cmd || rm -r $t1p ;
  [ "$I1S" != "$NA" ]       && echo "$FQDUPLICATE -o $t1s $I1S      &>/dev/null ;" >> $cmd || rm -r $t1s ;
  [ "$I1M" != "$NA" ]       && echo "$FQDUPLICATE -o $t1m $I1M      &>/dev/null ;" >> $cmd || rm -r $t1m ;
  [ "$I21,$I22" != "$NA2" ] && echo "$FQDUPLICATE -o $t2p $I21 $I22 &>/dev/null ;" >> $cmd || rm -r $t2p ;
  [ "$I2S" != "$NA" ]       && echo "$FQDUPLICATE -o $t2s $I2S      &>/dev/null ;" >> $cmd || rm -r $t2s ;
  [ "$I2M" != "$NA" ]       && echo "$FQDUPLICATE -o $t2m $I2M      &>/dev/null ;" >> $cmd || rm -r $t2m ;
  [ "$I31,$I32" != "$NA2" ] && echo "$FQDUPLICATE -o $t3p $I31 $I32 &>/dev/null ;" >> $cmd || rm -r $t3p ;
  [ "$I3S" != "$NA" ]       && echo "$FQDUPLICATE -o $t3s $I3S      &>/dev/null ;" >> $cmd || rm -r $t3s ;
  [ "$I3M" != "$NA" ]       && echo "$FQDUPLICATE -o $t3m $I3M      &>/dev/null ;" >> $cmd || rm -r $t3m ;
  [ "$I4S" != "$NA" ]       && echo "$FQDUPLICATE -o $t4s $I4S      &>/dev/null ;" >> $cmd || rm -r $t4s ;
  [ "$I5S" != "$NA" ]       && echo "$FQDUPLICATE -o $t5s $I5S      &>/dev/null ;" >> $cmd || rm -r $t5s ;
  [ "$I6S" != "$NA" ]       && echo "$FQDUPLICATE -o $t6s $I6S      &>/dev/null ;" >> $cmd || rm -r $t6s ;

  $XARGS -a $cmd -P $4 -I CMD bash -c CMD ;
  rm -f $cmd ;

  touch $cmd ;
  [ "$I11" != "$NA" ] && echo "$FQEXTRACT -o $O11 -px -l $t1p $I11 &>/dev/null ;" >> $cmd ;
  [ "$I12" != "$NA" ] && echo "$FQEXTRACT -o $O12 -px -l $t1p $I12 &>/dev/null ;" >> $cmd ;
  [ "$I1S" != "$NA" ] && echo "$FQEXTRACT -o $O1S -px -l $t1s $I1S &>/dev/null ;" >> $cmd ;
  [ "$I1M" != "$NA" ] && echo "$FQEXTRACT -o $O1M -px -l $t1m $I1M &>/dev/null ;" >> $cmd ;
  [ "$I21" != "$NA" ] && echo "$FQEXTRACT -o $O21 -px -l $t2p $I21 &>/dev/null ;" >> $cmd ;
  [ "$I22" != "$NA" ] && echo "$FQEXTRACT -o $O22 -px -l $t2p $I22 &>/dev/null ;" >> $cmd ;
  [ "$I2S" != "$NA" ] && echo "$FQEXTRACT -o $O2S -px -l $t2s $I2S &>/dev/null ;" >> $cmd ;
  [ "$I2M" != "$NA" ] && echo "$FQEXTRACT -o $O2M -px -l $t2m $I2M &>/dev/null ;" >> $cmd ;
  [ "$I31" != "$NA" ] && echo "$FQEXTRACT -o $O31 -px -l $t3p $I31 &>/dev/null ;" >> $cmd ;
  [ "$I32" != "$NA" ] && echo "$FQEXTRACT -o $O32 -px -l $t3p $I32 &>/dev/null ;" >> $cmd ;
  [ "$I3S" != "$NA" ] && echo "$FQEXTRACT -o $O3S -px -l $t3s $I3S &>/dev/null ;" >> $cmd ;
  [ "$I3M" != "$NA" ] && echo "$FQEXTRACT -o $O3M -px -l $t3m $I3M &>/dev/null ;" >> $cmd ;
  [ "$I4S" != "$NA" ] && echo "$FQEXTRACT -o $O4S -px -l $t4s $I4S &>/dev/null ;" >> $cmd ;
  [ "$I5S" != "$NA" ] && echo "$FQEXTRACT -o $O5S -px -l $t5s $I5S &>/dev/null ;" >> $cmd ;
  [ "$I6S" != "$NA" ] && echo "$FQEXTRACT -o $O6S -px -l $t6s $I6S &>/dev/null ;" >> $cmd ;
  
  $XARGS -a $cmd -P $4 -n 1 -I CMD bash -c CMD ;
  rm -f $cmd ;
 
  rm -f $t1p $t1s $t1m $t2p $t2s $t2m $t3p $t3s $t3m $t4s $t5s $t6s ;

  return 0 ;
}
#                                                                                                            #
# -- mvf --------------------------------------------------------------------------------------------------  #
# >> mv comma-separated specified files $1 (when they are not $NA) into corresponding (comma-separated)      #
#    specified files $2, and next return the new comma-separated file names                                  #
# >> example (of size 5 file names):  mvf fa1,$NA,fa3,fa4,$NA fb1,fb2,fb3,fb4,fb5                            #
#    + this will perform:             mv fa1 fb1 ; mv fa3 fb3 ; mv fa4 fb4 ;                                 #
#    + and next returns:              fb1,$NA,fb3,fb4,$NA                                                    # 
# >> no file pattern                                                                                         #
# + $1  (up to) 15 input file names separated by commas (use $NA for no file)                                #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
mvf() {
  local out="#";
  for i in {1..15}
  do
    local infile=$(get "$1" $i); local outfile=$(get "$2" $i);
    if [ "$infile" != "$NA" ]
    then
      if [ ! -s $infile ] ## infile is empty => no outfile (v23.07)
      then
        out="$out,$NA";
      else
        [ "$infile" != "$outfile" ] && mv $infile $outfile ;
        out="$out,$outfile";
      fi
    else
      out="$out,$NA";
    fi
  done
  sed 's/#,//' <<<"$out" ;
}
#                                                                                                            #
# -- cpfz -------------------------------------------------------------------------------------------------  #
# >> cp comma-separated specified files $1 (when they are not $NA) into corresponding (comma-separated)      #
#    specified files $2, compresses files (if any), and next returns the new comma-separated file names      #
# >> example (of size 5 file names):  cpfz fa1,$NA,fa3,fa4,$NA fb1,fb2,fb3,fb4,fb5 12 gz                     #
#    + this will perform:             gzip -c fa1 > fb1 ; gzip -c fa3 > fb3 ; gzip -c fa4 > fb4 ;            #
#    + and next returns:              fb1,$NA,fb3,fb4,$NA                                                    # 
# >> no file pattern                                                                                         #
# + $1  (up to) 15 input file names separated by commas (use $NA for no file)                                #
# + $2  (up to) 15 output file names separated by commas (use $NA for no file)                               #
# + $3  no. threads                                                                                          #
# + $4  compression format                                                                                   #
cpfz() {
  local out="#";
  for i in {1..15}
  do
    local infile=$(get "$1" $i); local outfile=$(get "$2" $i);
    if [ "$infile" != "$NA" ]
    then
      case $4 in
       fastq|fq)   { cp             $infile   $outfile & } 2>/dev/null ;;
       gz)         { $GZIP  -c      $infile > $outfile & } 2>/dev/null ;;
       bz|bz2)     { $BZIP2 -c      $infile > $outfile & } 2>/dev/null ;;
       dsrc|dsrc2)   $DSRC2  c -t$3 $infile   $outfile     2>/dev/null ;;
      esac
      while [ $(jobs -r | wc -l) -ge $3 ]; do sleep $WAITIME ; done
      out="$out,$outfile";
    else
      out="$out,$NA";
    fi
  done
  wait 2>/dev/null ;
  sed 's/#,//' <<<"$out" ;
}
#                                                                                                            #
# -- display_report ---------------------------------------------------------------------------------------  #
# >> returns the size of each file (when not $NA) from each library                                          #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input  file names separated by commas (use $NA for no file)                               #
# + $2  tmp directory                                                                                        #
# + $3  no. threads                                                                                          #
#                                                                                                            #
display_report() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## tmp files
  local t11=$(randfile $2/fs11) t12=$(randfile $2/fs12) t1s=$(randfile $2/fs1s) t1m=$(randfile $2/fs1m);
  local t21=$(randfile $2/fs21) t22=$(randfile $2/fs22) t2s=$(randfile $2/fs2s) t2m=$(randfile $2/fs2m);
  local t31=$(randfile $2/fs31) t32=$(randfile $2/fs32) t3s=$(randfile $2/fs3s) t3m=$(randfile $2/fs3m);
  local t4s=$(randfile $2/fs4s) t5s=$(randfile $2/fs5s) t6s=$(randfile $2/fs6s);

  local cmd=$(randfile $2/drcmd);
  [ "$I11" != "$NA" ] && echo "$FQSTATS $I11 > $t11 ;" >> $cmd ;
  [ "$I12" != "$NA" ] && echo "$FQSTATS $I12 > $t12 ;" >> $cmd ;
  [ "$I1S" != "$NA" ] && echo "$FQSTATS $I1S > $t1s ;" >> $cmd ;
  [ "$I1M" != "$NA" ] && echo "$FQSTATS $I1M > $t1m ;" >> $cmd ;
  [ "$I21" != "$NA" ] && echo "$FQSTATS $I21 > $t21 ;" >> $cmd ;
  [ "$I22" != "$NA" ] && echo "$FQSTATS $I22 > $t22 ;" >> $cmd ;
  [ "$I2S" != "$NA" ] && echo "$FQSTATS $I2S > $t2s ;" >> $cmd ;
  [ "$I2M" != "$NA" ] && echo "$FQSTATS $I2M > $t2m ;" >> $cmd ;
  [ "$I31" != "$NA" ] && echo "$FQSTATS $I31 > $t31 ;" >> $cmd ;
  [ "$I32" != "$NA" ] && echo "$FQSTATS $I32 > $t32 ;" >> $cmd ;
  [ "$I3S" != "$NA" ] && echo "$FQSTATS $I3S > $t3s ;" >> $cmd ;
  [ "$I3M" != "$NA" ] && echo "$FQSTATS $I3M > $t3m ;" >> $cmd ;
  [ "$I4S" != "$NA" ] && echo "$FQSTATS $I4S > $t4s ;" >> $cmd ;
  [ "$I5S" != "$NA" ] && echo "$FQSTATS $I5S > $t5s ;" >> $cmd ;
  [ "$I6S" != "$NA" ] && echo "$FQSTATS $I6S > $t6s ;" >> $cmd ;

  $XARGS -a $cmd -P $3 -I CMD bash -c CMD ;
  rm -f $cmd ;

  [ "$I11,$I12" != "$NA2" ] && echo "> PE lib 1" ;
  [ "$I11" != "$NA" ] && $BAWK '{print"+ FQ11: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t11 ;
  [ "$I12" != "$NA" ] && $BAWK '{print"+ FQ12: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t12 ;
  [ "$I1S" != "$NA" ] && $BAWK '{print"+ FQ1S: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t1s ;
  [ "$I1M" != "$NA" ] && $BAWK '{print"+ FQ1M: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t1m ;
  [ "$I21,$I22" != "$NA2" ] && echo "> PE lib 2" ;
  [ "$I21" != "$NA" ] && $BAWK '{print"+ FQ21: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t21 ;
  [ "$I22" != "$NA" ] && $BAWK '{print"+ FQ22: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t22 ;
  [ "$I2S" != "$NA" ] && $BAWK '{print"+ FQ2S: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t2s ;
  [ "$I2M" != "$NA" ] && $BAWK '{print"+ FQ2M: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t2m ;
  [ "$I31,$I32" != "$NA2" ] && echo "> PE lib 3" ;
  [ "$I31" != "$NA" ] && $BAWK '{print"+ FQ31: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t31 ;
  [ "$I32" != "$NA" ] && $BAWK '{print"+ FQ32: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t32 ;
  [ "$I3S" != "$NA" ] && $BAWK '{print"+ FQ3S: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t3s ;
  [ "$I3M" != "$NA" ] && $BAWK '{print"+ FQ3M: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t3m ;
  [ "$I4S" != "$NA" ] && echo "> SE lib 4" ;
  [ "$I4S" != "$NA" ] && $BAWK '{print"+ FQ4S: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t4s ;
  [ "$I5S" != "$NA" ] && echo "> SE lib 5" ;
  [ "$I5S" != "$NA" ] && $BAWK '{print"+ FQ5S: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t5s ;
  [ "$I6S" != "$NA" ] && echo "> SE lib 5" ;
  [ "$I6S" != "$NA" ] && $BAWK '{print"+ FQ6S: "$2" reads\t"$4" bases\t(avg lgt = "int(0.5 + $4/$2)" bps)"}' $t6s ;

  rm -f $t11 $t12 $t1s $t1m $t21 $t22 $t2s $t2m $t31 $t32 $t3s $t3m $t4s $t5s $t6s ;

  return 0 ;
}
#                                                                                                            #
# -- min_avg_lgt ------------------------------------------------------------------------------------------  #
# >> returns the minimum average read length size of all files (when not $NA) from all libraries             #
# >> 15 file pattern: 1.1,1.2,1.S,1.M,2.1,2.2,2.S,2.M,3.1,3.2,3.S,3.M,4.S,5.S,6.S                            #
# + $1  (up to) 15 input file names separated by commas (use $NA for no file)                                #
# + $2  tmp directory                                                                                        #
# + $3  no. threads                                                                                          #
#                                                                                                            #
min_avg_lgt() {
  ## infiles
  local I11=$(get "$1"  1)  I12=$(get "$1"  2)  I1S=$(get "$1"  3)  I1M=$(get "$1"  4); 
  local I21=$(get "$1"  5)  I22=$(get "$1"  6)  I2S=$(get "$1"  7)  I2M=$(get "$1"  8); 
  local I31=$(get "$1"  9)  I32=$(get "$1" 10)  I3S=$(get "$1" 11)  I3M=$(get "$1" 12); 
  local I4S=$(get "$1" 13)  I5S=$(get "$1" 14)  I6S=$(get "$1" 15);
  ## tmp files
  local t11=$(randfile $2/fs11) t12=$(randfile $2/fs12) t1s=$(randfile $2/fs1s) t1m=$(randfile $2/fs1m);
  local t21=$(randfile $2/fs21) t22=$(randfile $2/fs22) t2s=$(randfile $2/fs2s) t2m=$(randfile $2/fs2m);
  local t31=$(randfile $2/fs31) t32=$(randfile $2/fs32) t3s=$(randfile $2/fs3s) t3m=$(randfile $2/fs3m);
  local t4s=$(randfile $2/fs4s) t5s=$(randfile $2/fs5s) t6s=$(randfile $2/fs6s);

  local cmd=$(randfile $2/drcmd);
  [ "$I11" != "$NA" ] && echo "$FQSTATS $I11 > $t11 ;" >> $cmd ;
  [ "$I12" != "$NA" ] && echo "$FQSTATS $I12 > $t12 ;" >> $cmd ;
  [ "$I1S" != "$NA" ] && echo "$FQSTATS $I1S > $t1s ;" >> $cmd ;
  [ "$I1M" != "$NA" ] && echo "$FQSTATS $I1M > $t1m ;" >> $cmd ;
  [ "$I21" != "$NA" ] && echo "$FQSTATS $I21 > $t21 ;" >> $cmd ;
  [ "$I22" != "$NA" ] && echo "$FQSTATS $I22 > $t22 ;" >> $cmd ;
  [ "$I2S" != "$NA" ] && echo "$FQSTATS $I2S > $t2s ;" >> $cmd ;
  [ "$I2M" != "$NA" ] && echo "$FQSTATS $I2M > $t2m ;" >> $cmd ;
  [ "$I31" != "$NA" ] && echo "$FQSTATS $I31 > $t31 ;" >> $cmd ;
  [ "$I32" != "$NA" ] && echo "$FQSTATS $I32 > $t32 ;" >> $cmd ;
  [ "$I3S" != "$NA" ] && echo "$FQSTATS $I3S > $t3s ;" >> $cmd ;
  [ "$I3M" != "$NA" ] && echo "$FQSTATS $I3M > $t3m ;" >> $cmd ;
  [ "$I4S" != "$NA" ] && echo "$FQSTATS $I4S > $t4s ;" >> $cmd ;
  [ "$I5S" != "$NA" ] && echo "$FQSTATS $I5S > $t5s ;" >> $cmd ;
  [ "$I6S" != "$NA" ] && echo "$FQSTATS $I6S > $t6s ;" >> $cmd ;

  $XARGS -a $cmd -P $3 -I CMD bash -c CMD ;
  rm -f $cmd ;

  local mal=987654321;
  local avg=987654321;
  if [ "$I11" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t11); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I12" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t12); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I1S" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t1s); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I1M" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t1m); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I21" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t21); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I22" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t22); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I2S" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t2s); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I2M" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t2m); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I31" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t31); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I32" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t32); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I3S" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t3s); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I3M" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t3m); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I4S" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t4s); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I5S" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t5s); [ $avg -lt $mal ] && mal=$avg; fi
  if [ "$I6S" != "$NA" ]; then avg=$($BAWK '{print int(0.5 + $4/$2)}' $t6s); [ $avg -lt $mal ] && mal=$avg; fi

  rm -f $t11 $t12 $t1s $t1m $t21 $t22 $t2s $t2m $t31 $t32 $t3s $t3m $t4s $t5s $t6s ;

  echo $mal ;
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# READING OPTIONS                                                                                            #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

LIB1=false; LIB1S=false; LIB1M=false;
LIB2=false; LIB2S=false; LIB2M=false;
LIB3=false; LIB3S=false; LIB3M=false;
LIB4=false; LIB5=false;  LIB6=false; 
FQ11="$NA"; FQ12="$NA";  # lib1 PE files                      -1 -2
FQ21="$NA"; FQ22="$NA";  # lib2 PE files                      -3 -4
FQ31="$NA"; FQ32="$NA";  # lib3 PE files                      -5 -6
FQ4S="$NA";              # lib4 SE file                          -7
FQ5S="$NA";              # lib5 SE file                          -8
FQ6S="$NA";              # lib6 SE file                          -9
OUTDIR="$NA";            # outdir                                -o
BASENAME="$NA";          # basename                              -b
TMPDIR=${TMPDIR:-/tmp};  # tmp directory                         -w
ALIENS="$NA";            # alien oligo                           -a
CONTA="$NA";             # contaminant file                      -A
STEPS="$NA";             # processing step(s)                    -s
Q=15;                    # Phred score threshold                 -q
P=50;                    # max allowed percent low-Phreded bases -p
L=0;                     # min allowed lgt                       -l
MINCOV=4;                # min allowed read coverage depth       -c
MAXCOV=90;               # max allowed read coverage depth       -C
STEPS="T"                # processing steps                      -s
NTHREADS=12;             # no. threads                           -t
ZIP="fq";                # output file compression               -z

while getopts :d:1:2:3:4:5:6:7:8:9:o:b:a:A:s:q:p:l:c:C:t:w:z:h option
do
  case $option in
   d) display_aliens "$OPTARG" ; exit ;;
   1) FQ11="$OPTARG"     ;;
   2) FQ12="$OPTARG"     ;;
   3) FQ21="$OPTARG"     ;;
   4) FQ22="$OPTARG"     ;;
   5) FQ31="$OPTARG"     ;;
   6) FQ32="$OPTARG"     ;;
   7) FQ4S="$OPTARG"     ;;
   8) FQ5S="$OPTARG"     ;;
   9) FQ6S="$OPTARG"     ;;
   o) OUTDIR="$OPTARG"   ;;
   b) BASENAME="$OPTARG" ;;
   a) ALIENS="$OPTARG"   ;;
   A) CONTA="$OPTARG"    ;;
   s) STEPS="$OPTARG"    ;;
   q) Q=$OPTARG          ;;
   p) P=$OPTARG          ;;
   l) L=$OPTARG          ;;
   c) MINCOV=$OPTARG     ;;
   C) MAXCOV=$OPTARG     ;;
   t) NTHREADS=$OPTARG   ;;
   w) TMPDIR="$OPTARG"   ;;
   z) ZIP="$OPTARG"      ;;
   h)  mandoc ;  exit 0  ;;
   \?) mandoc ;  exit 1  ;;
  esac
done

echo "# OS:   $OSTYPE";
echo "# Bash: $BASH_VERSION" ;
echo "# fqCleanER: $VERSION" ;

##############################################################################################################
#                                                                                                            #
# CHECKING OPTIONS                                                                                           #
#                                                                                                            #
##############################################################################################################
## PE lib 1  #################################################################################################
echo "# input file(s):"
if [ "$FQ11" != "$NA" ]
then
  if [ ! -e $FQ11 ];                  then echo "lib1 R1 file does not exist (option -1): $FQ11" >&2 ; exit 1 ; fi
  if [ ! -f $FQ11 ];                  then echo "lib1 R1 file is not regular (option -1): $FQ11" >&2 ; exit 1 ; fi
  if [ ! -r $FQ11 ];                  then echo "no read permission (option -1): $FQ11"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ11)" != "valid" ]; then echo "invalid extension (option -1): $FQ11"           >&2 ; exit 1 ; fi
  if [ "$FQ12" == "$NA" ];            then echo "no lib1 R2 file (option -2)"                    >&2 ; exit 1 ; fi
  if [ ! -e $FQ12 ];                  then echo "lib1 R2 file does not exist (option -2): $FQ12" >&2 ; exit 1 ; fi
  if [ ! -f $FQ12 ];                  then echo "lib1 R2 file is not regular (option -2): $FQ12" >&2 ; exit 1 ; fi
  if [ ! -r $FQ12 ];                  then echo "no read permission (option -2): $FQ12"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ12)" != "valid" ]; then echo "invalid extension (option -2): $FQ12"           >&2 ; exit 1 ; fi
  LIB1=true; 
  echo "> PE lib 1" ;
  echo -e "+ FQ11: $(disp $FQ11)";
  echo -e "+ FQ12: $(disp $FQ12)"; 
fi
## PE lib 2  #################################################################################################
if [ "$FQ21" != "$NA" ]
then
  if [ ! -e $FQ21 ];                  then echo "lib2 R1 file does not exist (option -3): $FQ21" >&2 ; exit 1 ; fi
  if [ ! -f $FQ21 ];                  then echo "lib2 R1 file is not regular (option -3): $FQ21" >&2 ; exit 1 ; fi
  if [ ! -r $FQ21 ];                  then echo "no read permission (option -3): $FQ21"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ21)" != "valid" ]; then echo "invalid extension (option -3): $FQ21"           >&2 ; exit 1 ; fi
  if [ "$FQ22" == "$NA" ];            then echo "no lib2 R2 file (option -4)"                    >&2 ; exit 1 ; fi
  if [ ! -e $FQ22 ];                  then echo "lib2 R2 file does not exist (option -4): $FQ22" >&2 ; exit 1 ; fi
  if [ ! -f $FQ22 ];                  then echo "lib2 R2 file is not regular (option -4): $FQ22" >&2 ; exit 1 ; fi
  if [ ! -r $FQ22 ];                  then echo "no read permission (option -4): $FQ22"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ22)" != "valid" ]; then echo "invalid extension (option -4): $FQ22"           >&2 ; exit 1 ; fi
  LIB2=true;
  echo "> PE lib 2" ;
  echo -e "+ FQ21: $(disp $FQ21)";
  echo -e "+ FQ22: $(disp $FQ22)"; 
fi
## PE lib 3  #################################################################################################
if [ "$FQ31" != "$NA" ]
then
  if [ ! -e $FQ31 ];                  then echo "lib3 R1 file does not exist (option -5): $FQ31" >&2 ; exit 1 ; fi
  if [ ! -f $FQ31 ];                  then echo "lib3 R1 file is not regular (option -5): $FQ31" >&2 ; exit 1 ; fi
  if [ ! -r $FQ31 ];                  then echo "no read permission (option -5): $FQ31"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ31)" != "valid" ]; then echo "invalid extension (option -5): $FQ31"           >&2 ; exit 1 ; fi
  if [ "$FQ32" == "$NA" ];            then echo "no lib3 R2 file (option -6)"                    >&2 ; exit 1 ; fi
  if [ ! -e $FQ32 ];                  then echo "lib3 R2 file does not exist (option -6): $FQ32" >&2 ; exit 1 ; fi
  if [ ! -f $FQ32 ];                  then echo "lib3 R2 file is not regular (option -6): $FQ32" >&2 ; exit 1 ; fi
  if [ ! -r $FQ32 ];                  then echo "no read permission (option -6): $FQ32"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ32)" != "valid" ]; then echo "invalid extension (option -6): $FQ32"           >&2 ; exit 1 ; fi
  LIB3=true;
  echo "> PE lib 3" ;
  echo -e "+ FQ31: $(disp $FQ31)";
  echo -e "+ FQ32: $(disp $FQ31)"; 
fi
## SE lib 4  #################################################################################################
if [ "$FQ4S" != "$NA" ]
then
  if [ ! -e $FQ4S ];                  then echo "lib4 SE file does not exist (option -7): $FQ4S" >&2 ; exit 1 ; fi
  if [ ! -f $FQ4S ];                  then echo "lib4 SE file is not regular (option -7): $FQ4S" >&2 ; exit 1 ; fi
  if [ ! -r $FQ4S ];                  then echo "no read permission (option -7): $FQ4S"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ4S)" != "valid" ]; then echo "invalid extension (option -7): $FQ4S"           >&2 ; exit 1 ; fi
  LIB4=true;
  echo "> SE lib 4" ;
  echo -e "+ FQ4S: $(disp $FQ4S)";
fi
## SE lib 5  #################################################################################################
if [ "$FQ5S" != "$NA" ]
then
  if [ ! -e $FQ5S ];                  then echo "lib5 SE file does not exist (option -8): $FQ5S" >&2 ; exit 1 ; fi
  if [ ! -f $FQ5S ];                  then echo "lib5 SE file is not regular (option -8): $FQ5S" >&2 ; exit 1 ; fi
  if [ ! -r $FQ5S ];                  then echo "no read permission (option -8): $FQ5S"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ5S)" != "valid" ]; then echo "invalid extension (option -8): $FQ5S"           >&2 ; exit 1 ; fi
  LIB5=true;
  echo "> SE lib 5" ;
  echo -e "+ FQ5S: $(disp $FQ5S)";
fi
## SE lib 6  #################################################################################################
if [ "$FQ6S" != "$NA" ]
then
  if [ ! -e $FQ6S ];                  then echo "lib6 SE file does not exist (option -9): $FQ6S" >&2 ; exit 1 ; fi
  if [ ! -f $FQ6S ];                  then echo "lib6 SE file is not regular (option -9): $FQ6S" >&2 ; exit 1 ; fi
  if [ ! -r $FQ6S ];                  then echo "no read permission (option -9): $FQ6S"          >&2 ; exit 1 ; fi
  if [ "$(chext $FQ6S)" != "valid" ]; then echo "invalid extension (option -9): $FQ6S"           >&2 ; exit 1 ; fi
  LIB6=true;
  echo "> SE lib 6" ;
  echo -e "+ FQ6S: $(disp $FQ6S)";
fi
if ! $LIB1 && ! $LIB2 && ! $LIB3 && ! $LIB4 && ! $LIB5 && ! $LIB6 ; then echo "no input file(s)"    >&2 ; exit 1 ; fi
## Q  ########################################################################################################
if ! [[ $Q =~ ^[0-9]+$ ]];           then echo "incorrect value (option -q): $Q"                    >&2 ; exit 1 ; fi
## L  ########################################################################################################
if ! [[ $L =~ ^[0-9]+$ ]];           then echo "incorrect value (option -l): $L"                    >&2 ; exit 1 ; fi
## P  ########################################################################################################
if ! [[ $P =~ ^[0-9]+$ ]];           then echo "incorrect value (option -p): $P"                    >&2 ; exit 1 ; fi
if [ $P -gt 100 ];                   then echo "incorrect percentage (option -p): $P"               >&2 ; exit 1 ; fi
## MINCOV/MAXCOV  ############################################################################################
if ! [[ $MINCOV =~ ^[0-9]+$ ]];      then echo "incorrect value (option -c): $MINCOV"               >&2 ; exit 1 ; fi
if ! [[ $MAXCOV =~ ^[0-9]+$ ]];      then echo "incorrect value (option -C): $MAXCOV"               >&2 ; exit 1 ; fi
if [ $MINCOV -ge $MAXCOV ];          then echo "min too large (options -c/-C): $MINCOV >= $MAXCOV"  >&2 ; exit 1 ; fi
## NTHREADS  #################################################################################################
if ! [[ $NTHREADS =~ ^[0-9]+$ ]];    then echo "incorrect value: $NTHREADS (option -t)"             >&2 ; exit 1 ; fi
## TMPDIR  ###################################################################################################
[ "${TMPDIR:0:1}" != "/" ] && TMPDIR="$(pwd)/$TMPDIR";
if [ ! -e $TMPDIR ];                 then echo "tmp directory does not exist (option -w): $TMPDIR"  >&2 ; exit 1 ; fi
if [ ! -d $TMPDIR ];                 then echo "not a directory (option -w): $TMPDIR"               >&2 ; exit 1 ; fi
if [ ! -w $TMPDIR ];                 then echo "no write permission (option -w): $TMPDIR"           >&2 ; exit 1 ; fi
## BASENAME  #################################################################################################
if [ "$BASENAME" == "$NA" ];         then echo "basename not specified (option -b)"                 >&2 ; exit 1 ; fi
## OUTDIR  ###################################################################################################
if [ "$OUTDIR" == "$NA" ];           then echo "outdir not specified (option -o)"                   >&2 ; exit 1 ; fi
if [ -e $OUTDIR ]
then
  if [ ! -d $OUTDIR ];               then echo "not a directory (option -o): $OUTDIR"               >&2 ; exit 1 ; fi
  if [ ! -w $OUTDIR ];               then echo "no write permission (option -o): $OUTDIR"           >&2 ; exit 1 ; fi
fi
## ZIP  ######################################################################################################
if [ "$(chext x.$ZIP)" != "valid" ]; then echo "invalid compression format (option -z): $ZIP"       >&2 ; exit 1 ; fi


##############################################################################################################
#                                                                                                            #
# INIT                                                                                                       #
#                                                                                                            #
##############################################################################################################

## OUTDIR  ###################################################################################################
if [ ! -e $OUTDIR ]
then
  echo "$(chrono $STIME) creating output directory" ;
  mkdir $OUTDIR ;
  if [ ! -e $OUTDIR ];            then echo "unable to create directory (option -o): $OUTDIR"    >&2 ; exit 1 ; fi
fi
echo "# output directory" ; echo "+ OUTDIR=$OUTDIR" ;

## TMPDIR  ###################################################################################################
TMP_DIR=$(mktemp -d -p $TMPDIR -t fqCleanER.$BASENAME.XXXXXXXXX);
echo "# tmp directory" ; echo "+ TMP_DIR=$TMP_DIR" ;

## SYSTEM SPEC  ##############################################################################################
export LC_ALL=C ;
trap "echo -n interrupting ; echo -n . ; kill -9 $(jobs -pr) &> /dev/null ; echo -n . ; wait ; echo -n . ; rm -rf $TMP_DIR/* &>/dev/null ; wait ; echo -n . ; rm -rf $TMP_DIR &>/dev/null ; while [ -e $TMP_DIR ]; do sleep 1 ; echo -n . ; rm -rf $TMP_DIR &>/dev/null ; done ; echo ; exit " SIGINT

## TMP FILES  ################################################################################################
# PE lib 1
TI11=$(randfile $TMP_DIR/ifq11); TI12=$(randfile $TMP_DIR/ifq12);
TI1S=$(randfile $TMP_DIR/ifq1s); TI1M=$(randfile $TMP_DIR/ifq1m);
TO11=$(randfile $TMP_DIR/ofq11); TO12=$(randfile $TMP_DIR/ofq12);
TO1S=$(randfile $TMP_DIR/ofq1s); TO1M=$(randfile $TMP_DIR/ofq1m);
! $LIB1 && rm -f $TI11 $TI12 $TI1S $TI1M $TO11 $TO12 $TO1S $TO1M ;
# PE lib 2
TI21=$(randfile $TMP_DIR/ifq21); TI22=$(randfile $TMP_DIR/ifq22);
TI2S=$(randfile $TMP_DIR/ifq2s); TI2M=$(randfile $TMP_DIR/ifq2m);
TO21=$(randfile $TMP_DIR/ofq21); TO22=$(randfile $TMP_DIR/ofq22);
TO2S=$(randfile $TMP_DIR/ofq2s); TO2M=$(randfile $TMP_DIR/ofq2m);
! $LIB2 && rm -f $TI21 $TI22 $TI2S $TI2M $TO21 $TO22 $TO2S $TO2M ;
# PE lib 3
TI31=$(randfile $TMP_DIR/ifq31); TI32=$(randfile $TMP_DIR/ifq32);
TI3S=$(randfile $TMP_DIR/ifq3s); TI3M=$(randfile $TMP_DIR/ifq3m);
TO31=$(randfile $TMP_DIR/ofq31); TO32=$(randfile $TMP_DIR/ofq32);
TO3S=$(randfile $TMP_DIR/ofq3s); TO3M=$(randfile $TMP_DIR/ofq3m);
! $LIB3 && rm -f $TI31 $TI32 $TI3S $TI3M $TO31 $TO32 $TO3S $TO3M ;
# SE lib 4
TI4S=$(randfile $TMP_DIR/ifq4s); TO4S=$(randfile $TMP_DIR/ofq4s);
! $LIB4 && rm -f $TI4S $TO4S ;
# SE lib 5
TI5S=$(randfile $TMP_DIR/ifq5s); TO5S=$(randfile $TMP_DIR/ofq5s);
! $LIB5 && rm -f $TI5S $TO5S ;
# SE lib 6
TI6S=$(randfile $TMP_DIR/ifq6s); TO6S=$(randfile $TMP_DIR/ofq6s);
! $LIB6 && rm -f $TI6S $TO6S ;
# ALL INFQ
ALLINFQ="$TI11,$TI12,$TI1S,$TI1M,$TI21,$TI22,$TI2S,$TI2M,$TI31,$TI32,$TI3S,$TI3M,$TI4S,$TI5S,$TI6S";

## STEPS  ####################################################################################################
[ "$STEPS" = "$NA" ] && STEPS="T" || STEP=${STEP^^};
STEPS=$(sed 's/RL/N/g;s/LR/N/g' <<<"$STEPS");
[ -n "$(grep M <<<$STEPS)" ] && ! $LIB1 && ! $LIB2 && ! $LIB3 && STEPS=$(tr -d M <<<"$STEPS");

## PROCESSING INFILE(S)  #####################################################################################
if $LIB1 ; then INFQ="$FQ11,$FQ12";       OUTFQ="$TI11,$TI12";        else INFQ="$NA2";       OUTFQ="$NA2";        fi
if $LIB2 ; then INFQ="$INFQ,$FQ21,$FQ22"; OUTFQ="$OUTFQ,$TI21,$TI22"; else INFQ="$INFQ,$NA2"; OUTFQ="$OUTFQ,$NA2"; fi
if $LIB3 ; then INFQ="$INFQ,$FQ31,$FQ32"; OUTFQ="$OUTFQ,$TI31,$TI32"; else INFQ="$INFQ,$NA2"; OUTFQ="$OUTFQ,$NA2"; fi
if $LIB4 ; then INFQ="$INFQ,$FQ4S";       OUTFQ="$OUTFQ,$TI4S";       else INFQ="$INFQ,$NA";  OUTFQ="$OUTFQ,$NA";  fi
if $LIB5 ; then INFQ="$INFQ,$FQ5S";       OUTFQ="$OUTFQ,$TI5S";       else INFQ="$INFQ,$NA";  OUTFQ="$OUTFQ,$NA";  fi
if $LIB6 ; then INFQ="$INFQ,$FQ6S";       OUTFQ="$OUTFQ,$TI6S";       else INFQ="$INFQ,$NA";  OUTFQ="$OUTFQ,$NA";  fi
echo -n "$(chrono) reading input files ... ";
fqinit $INFQ $OUTFQ $TMP_DIR $NTHREADS ;
echo "[ok]" ;
$LIB1 && INFQ="$TI11,$TI12,$NA2"       || INFQ="$NA4" ;
$LIB2 && INFQ="$INFQ,$TI21,$TI22,$NA2" || INFQ="$INFQ,$NA4" ;
$LIB3 && INFQ="$INFQ,$TI31,$TI32,$NA2" || INFQ="$INFQ,$NA4" ;
$LIB4 && INFQ="$INFQ,$TI4S"            || INFQ="$INFQ,$NA" ;
$LIB5 && INFQ="$INFQ,$TI5S"            || INFQ="$INFQ,$NA" ;
$LIB6 && INFQ="$INFQ,$TI6S"            || INFQ="$INFQ,$NA" ;
display_report $INFQ $TMP_DIR $NTHREADS ;

## DEFAULT L  ################################################################################################
if [ $L -eq 0 ]
then
  echo -n "$(chrono) estimating read length threshold (option -l) ... ";
  L=$(min_avg_lgt $INFQ $TMP_DIR $NTHREADS);
  L=$(( $L / 2 ));
  echo "[ok]" ;
  echo "+ L=$L" ;
fi  

## ALIEN OLIGOS  #############################################################################################
ALIENFILE=$OUTDIR/$BASENAME.aliens.fasta;
if [ -n "$(grep T <<<$STEPS)" ]
then
  echo "# alien oligonucleotides" ;
  if   [ "$ALIENS" == "$NA" ]
  then
    display_aliens "POLY" | grep -v -F "##" > $ALIENFILE ;
    echo "+ ALIENS=POLY" ;
  elif [ "$ALIENS" == "AUTO" ]
  then  
    echo "+ ALIENS=AUTO" ;
    echo -n "$(chrono) inferring alien oligos ... " ;
    fq2oligo $INFQ $ALIENFILE $TMP_DIR $NTHREADS ;
    echo "[ok]" ;
    cat $ALIENFILE ;
  elif [ -e $ALIENS ]
  then
    cat $ALIENS > $ALIENFILE ;
    echo "+ ALIENS=$ALIENS" ;
  else
    display_aliens "$ALIENS" | grep -v -F "##" > $ALIENFILE ;
    echo "+ ALIENS=$ALIENS" ;
  fi 
  if [ ! -s $ALIENFILE ]; then echo "no alien oligo (option -a)" >&2 ; rm -rf $TMP_DIR ; exit 1 ; fi
fi

## CONTA SEQUENCE(S)  ########################################################################################
if [ -n "$(grep C <<<$STEPS)" ]
then
  echo "# contaminating sequence model" ;
  if   [ "$CONTA" == "$NA" ]
  then
    CONTAFILE=$(randfile $TMP_DIR/conta);
    echo "+ CONTA=PHIX174" ;
    echo "$PHIX174" > $CONTAFILE ;
  elif [ -e $CONTA ]
  then
    CONTAFILE=$CONTA;
    echo "+ CONTAFILE=$CONTAFILE" ;
    cfe=${CONTA##*.}
    if [ "$cfe" != "fasta" ] && [ "$cfe" != "fa" ] && [ "$cfe" != "fna" ] && [ "$cfe" != "kmr" ] && [ "$cfe" != "kmz" ]
    then echo "incorrect contaminant file (option -A)" >&2 ; rm -rf $TMP_DIR ; exit 1 ; fi
  elif [ $(grep -c -F "," <<<"$CONTA") -ne 0 ]
  then
    for afile in $(tr ',' ' ' <<<"$CONTA")
    do
      if [ ! -e $afile ]; then echo "not found (option -A): $afile" >&2 ; rm -rf $TMP_DIR ; exit 1 ; fi
      cfe=${afile##*.}
      if [ "$cfe" != "fasta" ] && [ "$cfe" != "fa" ] && [ "$cfe" != "fna" ] && [ "$cfe" != "kmr" ] && [ "$cfe" != "kmz" ]
      then echo "incorrect contaminant file (option -A)" >&2 ; rm -rf $TMP_DIR ; exit 1 ; fi
      echo "+ CONTAFILE=$afile" ;
    done
    CONTAFILE=$CONTA;
  else      
    echo "problem with option -A" >&2 ; rm -rf $TMP_DIR ; exit 1 ;
  fi  
fi

##############################################################################################################
#                                                                                                            #
# PROCESSING STEPS                                                                                           #
#                                                                                                            #
##############################################################################################################

echo "$(chrono) processing files ..." ;
echo "+ STEPS=\"$STEPS\"" ;
echo "+ NTHREADS=$NTHREADS" ;

sid=0;
while [ $sid -lt ${#STEPS} ]
do
  step=${STEPS:$sid:1};

  case $step in
   C)
    MESSAGE="discarding [C]ontaminating reads";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12"        || OUTFQ="$NA2" ;
    $LIB1S && OUTFQ="$OUTFQ,$TO1S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB2S && OUTFQ="$OUTFQ,$TO2S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"       || OUTFQ="$OUTFQ,$NA" ; 
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB3S && OUTFQ="$OUTFQ,$TO3S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"       || OUTFQ="$OUTFQ,$NA" ;
    fqconta $INFQ $OUTFQ $TMP_DIR $NTHREADS $CONTAFILE ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   E)
    MESSAGE="correcting sequencing [E]rrors";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12"        || OUTFQ="$NA2" ;
    $LIB1S && OUTFQ="$OUTFQ,$TO1S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB2S && OUTFQ="$OUTFQ,$TO2S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"       || OUTFQ="$OUTFQ,$NA" ; 
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB3S && OUTFQ="$OUTFQ,$TO3S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"       || OUTFQ="$OUTFQ,$NA" ;
    fqcorrect $INFQ $OUTFQ $TMP_DIR $NTHREADS ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    # display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   D)
    MESSAGE="[D]eduplicating";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12"        || OUTFQ="$NA2" ;
    $LIB1S && OUTFQ="$OUTFQ,$TO1S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB2S && OUTFQ="$OUTFQ,$TO2S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"       || OUTFQ="$OUTFQ,$NA" ; 
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB3S && OUTFQ="$OUTFQ,$TO3S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"       || OUTFQ="$OUTFQ,$NA" ;
    fquniq $INFQ $OUTFQ $TMP_DIR $NTHREADS ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   L)
    MESSAGE="discarding [L]ow-coverage reads";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12"        || OUTFQ="$NA2" ;
    $LIB1S && OUTFQ="$OUTFQ,$TO1S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB2S && OUTFQ="$OUTFQ,$TO2S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"       || OUTFQ="$OUTFQ,$NA" ; 
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB3S && OUTFQ="$OUTFQ,$TO3S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"       || OUTFQ="$OUTFQ,$NA" ;
    fqnorm $INFQ $OUTFQ $TMP_DIR $NTHREADS $MINCOV $ROCK_MAXCOV ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   N)
    MESSAGE="[N]ormalizing coverage depth";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12"        || OUTFQ="$NA2" ;
    $LIB1S && OUTFQ="$OUTFQ,$TO1S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB2S && OUTFQ="$OUTFQ,$TO2S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"       || OUTFQ="$OUTFQ,$NA" ; 
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB3S && OUTFQ="$OUTFQ,$TO3S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"       || OUTFQ="$OUTFQ,$NA" ;
    fqnorm $INFQ $OUTFQ $TMP_DIR $NTHREADS $MINCOV $MAXCOV ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   M)
    MESSAGE="[M]erging overlapping PE reads";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && iflib1="$TI11,$TI12"       || iflib1="$NA2" ;
    $LIB1M && iflib1="$iflib1,$TI1M"     || iflib1="$iflib1,$NA" ;
    $LIB2  && iflib2="$TI21,$TI22"       || iflib2="$NA2" ;
    $LIB2M && iflib2="$iflib2,$TI2M"     || iflib2="$iflib2,$NA" ;
    $LIB3  && iflib3="$TI31,$TI32"       || iflib3="$NA2" ;
    $LIB3M && iflib3="$iflib3,$TI3M"     || iflib3="$iflib3,$NA" ;
    $LIB1  && oflib1="$TO11,$TO12,$TO1M" || oflib1="$NA3" ;
    $LIB2  && oflib2="$TO21,$TO22,$TO2M" || oflib2="$NA3" ;
    $LIB3  && oflib3="$TO31,$TO32,$TO3M" || oflib3="$NA3" ;
    fqmerge "$iflib1,$iflib2,$iflib3" "$oflib1,$oflib2,$oflib3" $TMP_DIR $NTHREADS ;
    OUTFQ="$(get $oflib1 1),$(get $oflib1 2),$(get $INFQ 3),$(get $oflib1 3)";
    OUTFQ="$OUTFQ,$(get $oflib2 1),$(get $oflib2 2),$(get $INFQ 7),$(get $oflib2 3)";
    OUTFQ="$OUTFQ,$(get $oflib3 1),$(get $oflib3 2),$(get $INFQ 11),$(get $oflib3 3)";
    OUTFQ="$OUTFQ,$(get $INFQ 13),$(get $INFQ 14),$(get $INFQ 15)";
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    LIB1M=$LIB1; LIB2M=$LIB2; LIB3M=$LIB3;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   R)
    MESSAGE="[R]educing redundancy";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12"        || OUTFQ="$NA2" ;
    $LIB1S && OUTFQ="$OUTFQ,$TO1S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB2S && OUTFQ="$OUTFQ,$TO2S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"       || OUTFQ="$OUTFQ,$NA" ; 
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32" || OUTFQ="$OUTFQ,$NA2" ;
    $LIB3S && OUTFQ="$OUTFQ,$TO3S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"       || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"       || OUTFQ="$OUTFQ,$NA" ;
    fqnorm $INFQ $OUTFQ $TMP_DIR $NTHREADS 0 $MAXCOV ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
   T)
    MESSAGE="[T]rimming and clipping";
    echo -n "$(chrono) $MESSAGE ... " ;
    $LIB1  && OUTFQ="$TO11,$TO12,$TO1S"        || OUTFQ="$NA3" ;
    $LIB1M && OUTFQ="$OUTFQ,$TO1M"             || OUTFQ="$OUTFQ,$NA" ;
    $LIB2  && OUTFQ="$OUTFQ,$TO21,$TO22,$TO2S" || OUTFQ="$OUTFQ,$NA3" ;
    $LIB2M && OUTFQ="$OUTFQ,$TO2M"             || OUTFQ="$OUTFQ,$NA" ;
    $LIB3  && OUTFQ="$OUTFQ,$TO31,$TO32,$TO3S" || OUTFQ="$OUTFQ,$NA3" ;
    $LIB3M && OUTFQ="$OUTFQ,$TO3M"             || OUTFQ="$OUTFQ,$NA" ;
    $LIB4  && OUTFQ="$OUTFQ,$TO4S"             || OUTFQ="$OUTFQ,$NA" ;
    $LIB5  && OUTFQ="$OUTFQ,$TO5S"             || OUTFQ="$OUTFQ,$NA" ;
    $LIB6  && OUTFQ="$OUTFQ,$TO6S"             || OUTFQ="$OUTFQ,$NA" ;
    fqtrim $INFQ $OUTFQ $TMP_DIR $NTHREADS $ALIENFILE $Q $P $L ;
    INFQ=$(mvf $OUTFQ $ALLINFQ);
    echo "[ok]" ;
    LIB1S=$LIB1; LIB2S=$LIB2; LIB3S=$LIB3;
    if [ "$INFQ" == "$NA15" ]; then break; fi
    display_report $INFQ $TMP_DIR $NTHREADS ;
   ;;
  esac
  let sid++;
done

echo -n "$(chrono) finalizing ... " ;
case $ZIP in
 fastq|fq)   fext="fastq"      ;;
 gz)         fext="fastq.gz"   ;;
 bz|bz2)     fext="fastq.bz2"  ;;
 dsrc|dsrc2) fext="fastq.dsrc" ;;
esac
nlib=0;
$LIB1 && let nlib++ ; $LIB2 && let nlib++ ; $LIB3 && let nlib++ ;
$LIB4 && let nlib++ ; $LIB5 && let nlib++ ; $LIB6 && let nlib++ ;
if [ $nlib -eq 1 ]
then
  oflib1="$OUTDIR/$BASENAME.1.$fext,$OUTDIR/$BASENAME.2.$fext,$OUTDIR/$BASENAME.S.$fext,$OUTDIR/$BASENAME.M.$fext";
  oflib2="$OUTDIR/$BASENAME.1.$fext,$OUTDIR/$BASENAME.2.$fext,$OUTDIR/$BASENAME.S.$fext,$OUTDIR/$BASENAME.M.$fext";
  oflib3="$OUTDIR/$BASENAME.1.$fext,$OUTDIR/$BASENAME.2.$fext,$OUTDIR/$BASENAME.S.$fext,$OUTDIR/$BASENAME.M.$fext";
  oflib4="$OUTDIR/$BASENAME.$fext";
  oflib5="$OUTDIR/$BASENAME.$fext";
  oflib6="$OUTDIR/$BASENAME.$fext";
else
  oflib1="$OUTDIR/$BASENAME.1.1.$fext,$OUTDIR/$BASENAME.1.2.$fext,$OUTDIR/$BASENAME.1.S.$fext,$OUTDIR/$BASENAME.1.M.$fext";
  oflib2="$OUTDIR/$BASENAME.2.1.$fext,$OUTDIR/$BASENAME.2.2.$fext,$OUTDIR/$BASENAME.2.S.$fext,$OUTDIR/$BASENAME.2.M.$fext";
  oflib3="$OUTDIR/$BASENAME.3.1.$fext,$OUTDIR/$BASENAME.3.2.$fext,$OUTDIR/$BASENAME.3.S.$fext,$OUTDIR/$BASENAME.3.M.$fext";
  oflib4="$OUTDIR/$BASENAME.4.$fext";
  oflib5="$OUTDIR/$BASENAME.5.$fext";
  oflib6="$OUTDIR/$BASENAME.6.$fext";
fi
OUTFQ=$(cpfz $INFQ "$oflib1,$oflib2,$oflib3,$oflib4,$oflib5,$oflib6" $NTHREADS $ZIP);
rm -rf $TMP_DIR ;
echo "[ok]" ;

if [ "$INFQ" == "$NA15" ]
then
  echo "# output files: none" ;
else
  echo "# output files:"
  F11=$(get "$OUTFQ"  1);  F12=$(get "$OUTFQ"  2);  F1S=$(get "$OUTFQ"  3);  F1M=$(get "$OUTFQ"  4); 
  F21=$(get "$OUTFQ"  5);  F22=$(get "$OUTFQ"  6);  F2S=$(get "$OUTFQ"  7);  F2M=$(get "$OUTFQ"  8); 
  F31=$(get "$OUTFQ"  9);  F32=$(get "$OUTFQ" 10);  F3S=$(get "$OUTFQ" 11);  F3M=$(get "$OUTFQ" 12); 
  F4S=$(get "$OUTFQ" 13);  F5S=$(get "$OUTFQ" 14);  F6S=$(get "$OUTFQ" 15);
  [ $nlib -ne 1 ] && [ "$F11,$F12" != "$NA2" ] && echo "> PE lib 1" ;
  [ "$F11" != "$NA" ] && echo -e "+ R1:  $(disp $F11)" ;
  [ "$F12" != "$NA" ] && echo -e "+ R2:  $(disp $F12)" ;
  [ "$F1S" != "$NA" ] && echo -e "+ Sgl: $(disp $F1S)" ;
  [ "$F1M" != "$NA" ] && echo -e "+ Mrg: $(disp $F1M)" ;
  [ $nlib -ne 1 ] && [ "$F21,$F22" != "$NA2" ] && echo "> PE lib 2" ;
  [ "$F21" != "$NA" ] && echo -e "+ R1:  $(disp $F21)" ;
  [ "$F22" != "$NA" ] && echo -e "+ R2:  $(disp $F22)" ;
  [ "$F2S" != "$NA" ] && echo -e "+ Sgl: $(disp $F2S)" ;
  [ "$F2M" != "$NA" ] && echo -e "+ Mrg: $(disp $F2M)" ;
  [ $nlib -ne 1 ] && [ "$F31,$F32" != "$NA2" ] && echo "> PE lib 3" ;
  [ "$F31" != "$NA" ] && echo -e "+ R1:  $(disp $F31)" ;
  [ "$F32" != "$NA" ] && echo -e "+ R2:  $(disp $F32)" ;
  [ "$F3S" != "$NA" ] && echo -e "+ Sgl: $(disp $F3S)" ;
  [ "$F3M" != "$NA" ] && echo -e "+ Mrg: $(disp $F3M)" ;
  [ $nlib -ne 1 ] && [ "$F4S" != "$NA" ] && echo "> SE lib 4" ;
  [ "$F4S" != "$NA" ] && echo -e "+ $(disp $F4S)" ;
  [ $nlib -ne 1 ] && [ "$F5S" != "$NA" ] && echo "> SE lib 5" ;
  [ "$F5S" != "$NA" ] && echo -e "+ $(disp $F5S)" ;
  [ $nlib -ne 1 ] && [ "$F6S" != "$NA" ] && echo "> SE lib 6" ;
  [ "$F6S" != "$NA" ] && echo -e "+ $(disp $F6S)" ;
fi

echo "$(chrono) exit" ;

exit ;
